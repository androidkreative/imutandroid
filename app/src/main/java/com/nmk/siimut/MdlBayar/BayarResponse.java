package com.nmk.siimut.MdlBayar;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;


/**
 * Created by Warsono on 01/07/2019.
 */

public class BayarResponse extends BasicResponse {

    @SerializedName("DATA")
    MstBayar Data;

    public MstBayar getData() {
        return Data;
    }
}
