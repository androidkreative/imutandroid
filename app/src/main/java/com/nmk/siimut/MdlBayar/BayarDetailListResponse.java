package com.nmk.siimut.MdlBayar;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class BayarDetailListResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstBayarDetail> DataList    = new ArrayList<MstBayarDetail>();

    public List<MstBayarDetail> getDataList() {
        return DataList;
    }
}
