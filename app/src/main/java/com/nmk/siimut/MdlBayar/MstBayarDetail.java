package com.nmk.siimut.MdlBayar;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstBayarDetail {
    @SerializedName("ID")
    String Id;

    @SerializedName("NIM")
    String Nim;

    @SerializedName("SEMESTER")
    String Semester;

    @SerializedName("PAY_DATE")
    String PaymentDate;

    @SerializedName("DESCRIPTION")
    String Description;

    @SerializedName("PLAN")
    String Plan;

    @SerializedName("ACTUAL")
    String Actual;

    public MstBayarDetail(String id, String nim, String semester, String paymentDate, String description, String plan, String actual) {
        Id = id;
        Nim = nim;
        Semester = semester;
        PaymentDate = paymentDate;
        Description = description;
        Plan = plan;
        Actual = actual;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getNim() {
        return Nim;
    }

    public void setNim(String nim) {
        Nim = nim;
    }

    public String getSemester() {
        return Semester;
    }

    public void setSemester(String semester) {
        Semester = semester;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        PaymentDate = paymentDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPlan() {
        return Plan;
    }

    public void setPlan(String plan) {
        Plan = plan;
    }

    public String getActual() {
        return Actual;
    }

    public void setActual(String actual) {
        Actual = actual;
    }
}