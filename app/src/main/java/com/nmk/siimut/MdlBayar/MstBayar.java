package com.nmk.siimut.MdlBayar;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstBayar {
    @SerializedName("ID")
    String Id;

    @SerializedName("NIM")
    String Nim;

    @SerializedName("NAMA_MAHASISWA")
    String NamaMahasiswa;

    @SerializedName("SPB_TOTAL")
    String SpbTotal;

    @SerializedName("SPB_WAJIB")
    String SpbWajib;

    @SerializedName("SPB_BAYAR")
    String SpbBayar;

    @SerializedName("SPB_TUNGGAKAN")
    String SpbTunggakan;

    @SerializedName("SPP_TOTAL")
    String SppTotal;

    @SerializedName("SPP_WAJIB")
    String SppWajib;

    @SerializedName("SPP_BAYAR")
    String SppBayar;

    @SerializedName("SPP_TUNGGAKAN")
    String SppTunggakan;

    public MstBayar(String id, String nim, String namaMahasiswa, String spbTotal, String spbWajib, String spbBayar, String spbTunggakan, String sppTotal, String sppWajib, String sppBayar, String sppTunggakan) {
        Id = id;
        Nim = nim;
        NamaMahasiswa = namaMahasiswa;
        SpbTotal = spbTotal;
        SpbWajib = spbWajib;
        SpbBayar = spbBayar;
        SpbTunggakan = spbTunggakan;
        SppTotal = sppTotal;
        SppWajib = sppWajib;
        SppBayar = sppBayar;
        SppTunggakan = sppTunggakan;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getNim() {
        return Nim;
    }

    public void setNim(String nim) {
        Nim = nim;
    }

    public String getNamaMahasiswa() {
        return NamaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        NamaMahasiswa = namaMahasiswa;
    }

    public String getSpbWajib() {
        return SpbWajib;
    }

    public void setSpbWajib(String spbWajib) {
        SpbWajib = spbWajib;
    }

    public String getSpbBayar() {
        return SpbBayar;
    }

    public void setSpbBayar(String spbBayar) {
        SpbBayar = spbBayar;
    }

    public String getSpbTunggakan() {
        return SpbTunggakan;
    }

    public void setSpbTunggakan(String spbTunggakan) {
        SpbTunggakan = spbTunggakan;
    }

    public String getSppWajib() {
        return SppWajib;
    }

    public void setSppWajib(String sppWajib) {
        SppWajib = sppWajib;
    }

    public String getSppBayar() {
        return SppBayar;
    }

    public void setSppBayar(String sppBayar) {
        SppBayar = sppBayar;
    }

    public String getSppTunggakan() {
        return SppTunggakan;
    }

    public void setSppTunggakan(String sppTunggakan) {
        SppTunggakan = sppTunggakan;
    }

    public String getSpbTotal() {
        return SpbTotal;
    }

    public void setSpbTotal(String spbTotal) {
        SpbTotal = spbTotal;
    }

    public String getSppTotal() {
        return SppTotal;
    }

    public void setSppTotal(String sppTotal) {
        SppTotal = sppTotal;
    }
}