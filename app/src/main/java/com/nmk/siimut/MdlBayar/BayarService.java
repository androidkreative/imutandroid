package com.nmk.siimut.MdlBayar;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface BayarService {

    @GET(Constants.API_GET_PAYMENT)
    Call<BayarResponse> getPayment(
            @Query("id") String UserId,
            @Query("auth") String UserAuth,
            @Query("nim") String Nim);

    @GET(Constants.API_GET_SPB)
    Call<BayarDetailListResponse> getDataSpb(
            @Query("id") String UserId,
            @Query("auth") String UserAuth,
            @Query("nim") String Nim,
            @Query("limit") String Limit,
            @Query("page") String Page);

    @GET(Constants.API_GET_SPP)
    Call<BayarDetailListResponse> getDataSpp(
            @Query("id") String UserId,
            @Query("auth") String UserAuth,
            @Query("nim") String Nim,
            @Query("semester") String Semester,
            @Query("limit") String Limit,
            @Query("page") String Page);
}
