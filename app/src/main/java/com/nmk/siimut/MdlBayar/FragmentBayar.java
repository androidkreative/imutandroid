package com.nmk.siimut.MdlBayar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jpardogo.android.googleprogressbar.library.GoogleProgressBar;
import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.MstUserProfile;
import com.nmk.siimut.MdlAccount.User.SignInActivity;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlPendaftaran.ObjGeneral;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Response;

import static java.lang.Double.valueOf;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class FragmentBayar extends Fragment implements AdapterView.OnItemSelectedListener{

    private List<MstBayarDetail> bayarList      = new ArrayList<MstBayarDetail>();
    private List<ObjGeneral> itemSemesterList   = new ArrayList<ObjGeneral>();
    private MstUser objUser;
    private MstUserProfile obProfile;
    private MstBayar objBayar = null;
    private ViewGroup RootView;
    private RecyclerView viewBayar;
    private LinearLayout viewLayoutEmpty;
    private LinearLayout loadingInfo;
    private ShimmerLayout shimmerLayout;
    private LinearLayoutManager layoutManager;
    private bayarAdapter bayarAdapter;
    private boolean isLoading   = false;
    private int page            = 0;
    private ImageView viewImageEmpty;
    private TextView viewTitleEmpty, viewDescEmpty, buttonLogin;
    private LinearLayout viewTitle, viewButton;
    private LinearLayout viewSemester;
    private Spinner inputSemester;
    private LinearLayout viewLayoutTotal, viewData;
    private TextView labelTotal, labelWajib, labelPembayaran, labelTunggakan;
    private TextView viewTotalAll, viewTotal, viewWajib, viewPembayaran, viewTunggakan;
    private TextView buttonSpb, buttonSpp;
    private static String SPB       = "VIEW_SPB";
    private static String SPP       = "VIEW_SPP";
    private String strView          = SPB;
    private int MIN_LIST_LOADING    = 25;
    private String strSemester      = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RootView        = (ViewGroup) inflater.inflate(R.layout.fragment_bayar, null);
        viewLayoutEmpty = RootView.findViewById(R.id.viewEmpty);
        viewImageEmpty  = RootView.findViewById(R.id.viewEmptyImage);
        viewTitleEmpty  = RootView.findViewById(R.id.viewEmptyTitle);
        viewDescEmpty   = RootView.findViewById(R.id.viewEmptyDescription);
        viewTitle       = RootView.findViewById(R.id.viewTitle);
        viewButton      = RootView.findViewById(R.id.viewButton);

        viewTotalAll    = RootView.findViewById(R.id.viewTotalAll);
        viewTotal       = RootView.findViewById(R.id.viewTotalTunggakan);
        viewLayoutTotal = RootView.findViewById(R.id.viewTotal);
        viewTotal       = RootView.findViewById(R.id.viewTotalTunggakan);
        viewSemester    = RootView.findViewById(R.id.viewSemester);
        viewSemester.setVisibility(View.GONE);
        inputSemester   = RootView.findViewById(R.id.inputSemester);
        inputSemester.setOnItemSelectedListener(this);
        labelTotal      = RootView.findViewById(R.id.labelTotal);
        labelWajib      = RootView.findViewById(R.id.labelWajib);
        labelPembayaran = RootView.findViewById(R.id.labelSudah);
        labelTunggakan  = RootView.findViewById(R.id.labelTunggakan);
        viewWajib       = RootView.findViewById(R.id.viewWajib);
        viewPembayaran  = RootView.findViewById(R.id.viewSudah);
        viewTunggakan   = RootView.findViewById(R.id.viewTunggakan);
        buttonSpb       = RootView.findViewById(R.id.btnSpb);
        buttonSpp       = RootView.findViewById(R.id.btnSpp);
        viewData        = RootView.findViewById(R.id.viewData);
        viewBayar       = RootView.findViewById(R.id.viewBayar);
        buttonLogin     = RootView.findViewById(R.id.btnLogin);
        buttonLogin.setVisibility(View.GONE);
        loadingInfo     = RootView.findViewById(R.id.loadingInformation);
        layoutManager   = new LinearLayoutManager(getActivity());
        viewBayar.setLayoutManager(layoutManager);
//        viewBayar.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                int lastvisibleitemposition = layoutManager.findLastVisibleItemPosition();
//                if (lastvisibleitemposition == bayarAdapter.getItemCount() - 1) {
//                    if (!isLoading && strSemester.contentEquals("")) {
//                        isLoading = true;
//                        new GetMoreDataTask().execute(0);
//                    }
//                }
//            }
//        });
        buttonLogin.setOnClickListener(v -> {
            Intent intentlogin = new Intent(getActivity(), SignInActivity.class);
            intentlogin.putExtra(Constants.VIEW_KEY, Constants.VIEW_BAYAR);
            startActivity(intentlogin);
        });
        buttonSpb.setOnClickListener(v -> {
            onClickSpb();
        });
        buttonSpp.setOnClickListener(v -> {
            onClickSpp();
        });
        onSetSpinnerSemester();
        onGetDataBayar();
        return RootView;
    }
    private void onClickSpb(){
        buttonSpb.setBackground(getResources().getDrawable(R.drawable.bg_button_left_selected));
        buttonSpb.setTextColor(Color.WHITE);
        buttonSpp.setBackground(getResources().getDrawable(R.drawable.bg_button_right_default));
        buttonSpp.setTextColor(getResources().getColor(R.color.colorAccent));
        labelTotal.setText("SPb/DSP Total");
        labelWajib.setText("SPb/DSP Yang Harus dibayar");
        labelPembayaran.setText("SPb/DSP Yang Sudah dibayar");
        labelTunggakan.setText("Tunggakan SPb/DSP");
        viewSemester.setVisibility(View.GONE);

        if (objBayar.getSpbTotal() != null)
            viewTotalAll.setText(Utility.currencyFormat(valueOf(objBayar.getSpbTotal())));
        else
            viewTotalAll.setText("0");
        if (objBayar.getSpbWajib() != null)
            viewWajib.setText(Utility.currencyFormat(valueOf(objBayar.getSpbWajib())));
        else
            viewWajib.setText("0");
        if (objBayar.getSpbBayar() != null)
            viewPembayaran.setText(Utility.currencyFormat(valueOf(objBayar.getSpbBayar())));
        else
            viewPembayaran.setText("0");
        if (objBayar.getSpbTunggakan() != null)
            viewTunggakan.setText(Utility.currencyFormat(valueOf(objBayar.getSpbTunggakan())));
        else
            viewTunggakan.setText("0");

        if (Utility.isNetworkConnected()) {
            onShowLoading();
            strView     = SPB;
            page        = 0;
            strSemester = "";
            new GetBayarDetailTask().execute(0);
        }
    }
    private void onClickSpp(){
        buttonSpb.setBackground(getResources().getDrawable(R.drawable.bg_button_left_default));
        buttonSpb.setTextColor(getResources().getColor(R.color.colorAccent));
        buttonSpp.setBackground(getResources().getDrawable(R.drawable.bg_button_right_selected));
        buttonSpp.setTextColor(Color.WHITE);
        labelTotal.setText("SPP Total (8 Semester)");
        labelWajib.setText("SPP Yang Harus dibayar");
        labelPembayaran.setText("SPP Yang Sudah dibayar");
        labelTunggakan.setText("Tunggakan SPP");
        viewSemester.setVisibility(View.GONE);
//        viewSemester.setVisibility(View.VISIBLE);

        if (objBayar.getSppTotal() != null)
            viewTotalAll.setText(Utility.currencyFormat(valueOf(objBayar.getSppTotal())));
        else
            viewTotalAll.setText("0");
        if (objBayar.getSppWajib() != null)
            viewWajib.setText(Utility.currencyFormat(valueOf(objBayar.getSppWajib())));
        else
            viewWajib.setText("0");
        if (objBayar.getSppBayar() != null)
            viewPembayaran.setText(Utility.currencyFormat(valueOf(objBayar.getSppBayar())));
        else
            viewPembayaran.setText("0");
        if (objBayar.getSppTunggakan() != null)
            viewTunggakan.setText(Utility.currencyFormat(valueOf(objBayar.getSppTunggakan())));
        else
            viewTunggakan.setText("0");
        if (Utility.isNetworkConnected()) {
            onShowLoading();
            strView         = SPP;
            page            = 0;
            strSemester     = "";
            onSetSpinnerSemester();
            new GetBayarDetailTask().execute(0);
        }
    }
    private void onSetSpinnerSemester(){
        if (itemSemesterList.size() > 0)
            itemSemesterList.clear();
        itemSemesterList.add(new ObjGeneral("", getResources().getString(R.string.label_pilih_semester)));
        itemSemesterList.add(new ObjGeneral("1", "Semester - 1"));
        itemSemesterList.add(new ObjGeneral("2", "Semester - 2"));
        itemSemesterList.add(new ObjGeneral("3", "Semester - 3"));
        itemSemesterList.add(new ObjGeneral("4", "Semester - 4"));
        itemSemesterList.add(new ObjGeneral("5", "Semester - 5"));
        itemSemesterList.add(new ObjGeneral("6", "Semester - 6"));
        itemSemesterList.add(new ObjGeneral("7", "Semester - 7"));
        itemSemesterList.add(new ObjGeneral("8", "Semester - 8"));
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemSemesterList.size();
            }

            @Override
            public String get(int location) {
                return itemSemesterList.get(location).getName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputSemester.setAdapter(dataAdapter);
        int mPos = 0;
        if (strSemester != null && !strSemester.contentEquals("")) {
            int countData = itemSemesterList.size();
            for (int i=0; i<countData; i++) {
                ObjGeneral objData = itemSemesterList.get(i);
                if (objData.getId().contentEquals(strSemester))
                    mPos = i;
            }
        }
        inputSemester.setSelection(mPos);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()) {
            case R.id.inputSemester:
                strSemester  = itemSemesterList.get(position).getId();
                new GetBayarDetailTask().execute(0);
                break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    private void onGetDataBayar(){
        if (Utility.isNetworkConnected()) {
            List<MstUser> dataList = SqlUser.getAllData();
            if (dataList.size() > 0) {
                objUser = dataList.get(0);
                onShowLoading();
                List<MstUserProfile> dataProfile = SqlUser.getAllDataProfile();
                if (dataProfile.size() > 0)
                {
                    obProfile = dataProfile.get(0);
                    if (bayarList.size() > 0)
                        bayarList.clear();
                    if (objUser.getUserType().contentEquals("2"))
                        new GetBayarTask().execute(0);
                    else
                        setBayarNotPremit();
                }else
                    setBayarNotPremit();
            }else{
                viewTitle.setVisibility(View.VISIBLE);
                viewButton.setVisibility(View.GONE);
                viewData.setVisibility(View.GONE);
                viewLayoutEmpty.setVisibility(View.VISIBLE);
                viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_schedule));
                viewTitleEmpty.setText(R.string.no_account_title);
                viewDescEmpty.setText(R.string.no_account_desc);
                buttonLogin.setVisibility(View.VISIBLE);
                viewTotal.setText("Belum Tersedia");
            }
        }else{
            viewTitle.setVisibility(View.VISIBLE);
            viewButton.setVisibility(View.GONE);
            viewData.setVisibility(View.GONE);
            viewLayoutEmpty.setVisibility(View.VISIBLE);
            viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_requirement));
            viewTitleEmpty.setText(R.string.no_conection_title);
            viewDescEmpty.setText(R.string.no_conection_desc);
            viewTotal.setText("Belum Tersedia");
        }
    }
    private void setBayarNotPremit(){
        viewTitle.setVisibility(View.VISIBLE);
        viewButton.setVisibility(View.GONE);
        viewLayoutTotal.setVisibility(View.GONE);
        viewData.setVisibility(View.GONE);
        viewTotal.setText("Tidak Tersedia");
        viewBayar.setVisibility(View.GONE);
        buttonSpb.setVisibility(View.GONE);
        buttonSpp.setVisibility(View.GONE);
        viewLayoutEmpty.setVisibility(View.VISIBLE);
        viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_schedule));
        viewTitleEmpty.setText(R.string.admin_bayar_title);
        viewDescEmpty.setText(R.string.admin_bayar_desc);
        onHideloading();
    }
    class GetBayarTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            onShowLoading();
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                BayarService apiService                 = ApiClient.getClient().create(BayarService.class);
                Call<BayarResponse> mRequest            = apiService.getPayment(objUser.getUserId(), objUser.getUserAuth(), obProfile.getId());
                Response<BayarResponse> Response        = mRequest.execute();
                BayarResponse responseResult     = Response.body();
                if (responseResult != null){
                    switch (responseResult.getResultState()) {
                        case 200:
                            objBayar = responseResult.getData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            if (objBayar != null){
                //SET HEADER PAYMENT
                viewTitle.setVisibility(View.GONE);
                viewButton.setVisibility(View.VISIBLE);
                viewLayoutTotal.setVisibility(View.VISIBLE);
                buttonSpb.setVisibility(View.VISIBLE);
                buttonSpp.setVisibility(View.VISIBLE);

                double total = valueOf(objBayar.getSpbTunggakan()) + valueOf(objBayar.getSppTunggakan());
                if (total < 0) total   = 0;
                
                viewTotal.setText("Rp."+Utility.currencyFormat(total));

                if (objBayar.getSpbTotal() != null)
                    viewTotalAll.setText(Utility.currencyFormat(valueOf(objBayar.getSpbTotal())));
                else
                    viewTotalAll.setText("0");
                if (objBayar.getSpbWajib() != null)
                    viewWajib.setText(Utility.currencyFormat(valueOf(objBayar.getSpbWajib())));
                else
                    viewWajib.setText("0");
                if (objBayar.getSpbBayar() != null)
                    viewPembayaran.setText(Utility.currencyFormat(valueOf(objBayar.getSpbBayar())));
                else
                    viewPembayaran.setText("0");
                if (objBayar.getSpbTunggakan() != null)
                    viewTunggakan.setText(Utility.currencyFormat(valueOf(objBayar.getSpbTunggakan())));
                else
                    viewTunggakan.setText("0");
                new GetBayarDetailTask().execute(0);
            }else{
                viewTitle.setVisibility(View.VISIBLE);
                viewButton.setVisibility(View.GONE);
                viewLayoutTotal.setVisibility(View.GONE);
                viewData.setVisibility(View.GONE);
                viewTotal.setText("Belum Tersedia");
                viewBayar.setVisibility(View.GONE);
                buttonSpb.setVisibility(View.GONE);
                buttonSpp.setVisibility(View.GONE);
                viewLayoutEmpty.setVisibility(View.VISIBLE);
                viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_schedule));
                viewTitleEmpty.setText(R.string.no_bayar_title);
                viewDescEmpty.setText(R.string.no_bayar_desc);
                onHideloading();
            }
        }
    }
    class GetBayarDetailTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            if (bayarList.size() >0)
                bayarList.clear();
//            if (strView.contentEquals(SPP) && !strSemester.contentEquals("")){
//                viewWajib.setText("");
//                viewPembayaran.setText("");
//                viewTunggakan.setText("");
//            }
        }

        @Override
        protected String doInBackground(Integer... integers) {
            try {
                BayarService apiService                 = ApiClient.getClient().create(BayarService.class);
                Call<BayarDetailListResponse> mRequest;
                if (strView.contentEquals(SPB))
                    mRequest    = apiService.getDataSpb(objUser.getUserId(), objUser.getUserAuth(), obProfile.getId(), String.valueOf(MIN_LIST_LOADING), String.valueOf(page));
                else
                    mRequest    = apiService.getDataSpp(objUser.getUserId(), objUser.getUserAuth(), obProfile.getId(), strSemester, String.valueOf(MIN_LIST_LOADING), String.valueOf(page));
                Response<BayarDetailListResponse> Response      = mRequest.execute();
                BayarDetailListResponse responseResult          = Response.body();
                if (responseResult != null){
                    switch (responseResult.getResultState()) {
                        case 200:
                            bayarList = responseResult.getDataList();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            if (bayarList.size() > 0){
                bayarAdapter = new bayarAdapter(getActivity(), bayarList);
                viewBayar.setAdapter(bayarAdapter);
                viewBayar.setVisibility(View.VISIBLE);
                viewLayoutEmpty.setVisibility(View.GONE);
                if (strView.contentEquals(SPP)){
                    double sppWajib     = 0;
                    if (objBayar.getSppWajib() != null)
                        sppWajib = valueOf(objBayar.getSppWajib());
                    double sppBayar     = 0;
                    if (objBayar.getSppBayar() != null)
                        sppBayar     = valueOf(objBayar.getSppBayar());

//                    if (!strSemester.contentEquals("")){
//                        sppWajib     = valueOf(objBayar.getSppWajib());
//                        sppBayar     = valueOf(bayarAdapter.getTotal());
//                    }
                    double sppTunggakan = sppWajib - sppBayar;
                    if (sppTunggakan < 0) sppTunggakan   = 0;
                    viewWajib.setText(Utility.currencyFormat(sppWajib));
                    viewPembayaran.setText(Utility.currencyFormat(sppBayar));
                    viewTunggakan.setText(Utility.currencyFormat(sppTunggakan));
                }
            }else{
                viewBayar.setVisibility(View.GONE);
                viewLayoutEmpty.setVisibility(View.VISIBLE);
                viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_requirement));
                viewDescEmpty.setText(R.string.no_bayar_desc);
                if (strView.contentEquals(SPB))
                    viewTitleEmpty.setText(R.string.no_spb_title);
                else
                    viewTitleEmpty.setText(R.string.no_spp_title);
            }
            onHideloading();
        }
    }

    private void onShowLoading(){
        viewBayar.setVisibility(View.GONE);
        viewLayoutEmpty.setVisibility(View.GONE);
        loadingInfo.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        loadingInfo.removeAllViews();
        for (int i = 0; i < 10; i++) {
            View child  = getLayoutInflater().inflate(R.layout.item_informasi_shimmer, null);
            child.setLayoutParams(params);
            shimmerLayout   = child.findViewById(R.id.shimmer_layout);
            shimmerLayout.startShimmerAnimation();
            loadingInfo.setOrientation(LinearLayout.VERTICAL);
            loadingInfo.addView(child);
        }
    }
    private void onHideloading(){
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            shimmerLayout.stopShimmerAnimation();
            loadingInfo.setVisibility(View.GONE);
        }, 300);
    }

    class GetMoreDataTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            page        = page + 1;
        }
        @Override
        protected String doInBackground(Integer... integers) {
            String strResult = "";
            try {
                BayarService apiService                     = ApiClient.getClient().create(BayarService.class);
                Call<BayarDetailListResponse> mRequest;
                if (strView.contentEquals(SPB))
                    mRequest    = apiService.getDataSpb(objUser.getUserId(), objUser.getUserAuth(), obProfile.getId(), String.valueOf(MIN_LIST_LOADING), String.valueOf(page));
                else
                    mRequest    = apiService.getDataSpp(objUser.getUserId(), objUser.getUserAuth(), obProfile.getId(), strSemester, String.valueOf(MIN_LIST_LOADING), String.valueOf(page));
                Response<BayarDetailListResponse> Response  = mRequest.execute();
                BayarDetailListResponse responseResult      = Response.body();
                if (responseResult != null){
                    strResult       = String.valueOf(responseResult.getResultState());
                    switch (responseResult.getResultState()) {
                        case 200:
                            List<MstBayarDetail> responseData     = responseResult.getDataList();
                            int count                           = responseData.size();
                            if (count > 0){
                                int index = bayarList.size();
                                for (int i= 0; i < count; i++){
                                    MstBayarDetail objBayarDetail = responseData.get(i);
                                    bayarList.add( index, objBayarDetail);
                                    index = index+1;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) {}
            return strResult;
        }
        @Override
        protected void onProgressUpdate(Integer... values) { }
        @Override
        protected void onPostExecute(String result) {
            if (result.contentEquals("200"))
                bayarAdapter.notifyDataSetChanged();
            isLoading = false;
        }
    }

    public class bayarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private final int VIEW_TYPE_HEADER  = 0;
        private final int VIEW_TYPE_ITEM    = 1;
        private final int VIEW_TYPE_FOOTER  = 2;
        private List<MstBayarDetail> mItems;
        private Activity mContext;

        public bayarAdapter(Activity context, List<MstBayarDetail> itemData) {
            super();
            mItems      = itemData;
            mContext    = context;
        }
        @Override
        public int getItemViewType(int position) {
            if (isPositionFooter(position))
                return VIEW_TYPE_FOOTER;
            else
                return VIEW_TYPE_ITEM;
        }
        private boolean isPositionHeader(int position) {
            return position == 0;
        }
        private boolean isPositionFooter(int position) {
            return position >= mItems.size();
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            if (strSemester.contentEquals("")){
                if (viewType == VIEW_TYPE_FOOTER) {
                    View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading, viewGroup, false);
                    return new FooterViewHolder(view);
                }else{
                    View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bayar, viewGroup, false);
                    return new ItemViewHolder(view);
                }
            }else{
                View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bayar, viewGroup, false);
                return new ItemViewHolder(view);
            }
        }
        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
            if (strSemester.contentEquals("")){
                if (holder instanceof ItemViewHolder) {
                    setItemHolder((ItemViewHolder) holder, mItems.get(position));
                }else if (holder instanceof FooterViewHolder) {
                    FooterViewHolder FooterViewHolder = (FooterViewHolder) holder;
                    FooterViewHolder.progressLoading.setVisibility(View.VISIBLE);
                    FooterViewHolder.labelLoading.setVisibility(View.VISIBLE);
                }
            }else
                setItemHolder((ItemViewHolder) holder, mItems.get(position));
        }
        @Override
        public int getItemCount() {
            if(mItems.size() % MIN_LIST_LOADING == 0)
                return mItems.size() + 1;
            else
                return mItems.size();
        }
        public int getTotal() {
            int total = 0;
            int countItem = mItems.size();
            for (int i = 0; i < countItem; i++) {
                MstBayarDetail ObjItem   = mItems.get(i);
                total = total + Integer.valueOf(ObjItem.getActual());
            }
            return total;
        }
        private void setItemHolder(final ItemViewHolder viewHolder, final MstBayarDetail objBayarDetail){
            if (objBayarDetail != null && objBayarDetail.getId() != null){
                if (objBayarDetail.getPaymentDate() != null)
                    viewHolder.viewDate.setText(Utility.simpleDateView(objBayarDetail.getPaymentDate()));
                else
                    viewHolder.viewDate.setText("-");
                if (objBayarDetail.getDescription() != null)
                    viewHolder.viewDesc.setText(objBayarDetail.getDescription());
                else
                    viewHolder.viewDesc.setText("-");
                if (objBayarDetail.getPlan() != null)
                    viewHolder.viewPlan.setText(Utility.currencyFormat(valueOf(objBayarDetail.getPlan())));
                else
                    viewHolder.viewPlan.setText("-");
                if (objBayarDetail.getActual() != null)
                    viewHolder.viewActual.setText(Utility.currencyFormat(valueOf(objBayarDetail.getActual())));
                else
                    viewHolder.viewActual.setText("-");
            }
        }
        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView viewDate, viewDesc, viewPlan, viewActual;
            public LinearLayout itemLayout;

            public ItemViewHolder(View itemView) {
                super(itemView);
                itemLayout      = itemView.findViewById(R.id.layoutItem);
                viewDate        = itemView.findViewById(R.id.dateView);
                viewDesc        = itemView.findViewById(R.id.descriptionView);
                viewPlan        = itemView.findViewById(R.id.planView);
                viewActual      = itemView.findViewById(R.id.actualView);
            }
        }
        class FooterViewHolder extends RecyclerView.ViewHolder {
            public GoogleProgressBar progressLoading;
            public TextView labelLoading;
            public FooterViewHolder(View view) {
                super(view);
                progressLoading     = view.findViewById(R.id.loadingProgress);
                labelLoading        = view.findViewById(R.id.loadingView);
            }
        }
    }
}
