package com.nmk.siimut.MdlFakultas;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlFakultas {
    private static String TABLE_FAKULTAS        = "MST_FAKULTAS";
    private static String FIELD_FAKULTAS_ID     = "FAKULTAS_ID";
    private static String FIELD_FAKULTAS_NAME   = "FAKULTAS_NAME";
    private static String FIELD_IMAGE           = "IMAGE";

    private static String TABLE_JURUSAN         = "MST_JURUSAN";
    private static String FIELD_JURUSAN_ID      = "JURUSAN_ID";
    private static String FIELD_JURUSAN_NAME    = "JURUSAN_NAME";
    private static String FIELD_JENJANG         = "JENJANG";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_FAKULTAS + "("
                + FIELD_FAKULTAS_ID + " TEXT PRIMARY KEY, "
                + FIELD_FAKULTAS_NAME + " TEXT, "
                + FIELD_IMAGE + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);

        String CREATE_TABLE_JURUSAN = "CREATE TABLE " + TABLE_JURUSAN + "("
                + FIELD_JURUSAN_ID + " TEXT PRIMARY KEY, "
                + FIELD_JURUSAN_NAME + " TEXT, "
                + FIELD_JENJANG + " TEXT, "
                + FIELD_FAKULTAS_ID + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_JURUSAN);
    }
    public static void addDataFakultas(MstFakultas Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_FAKULTAS_ID, Data.getFakultasId());
        values.put(FIELD_FAKULTAS_NAME, Data.getFakultasName());
        values.put(FIELD_IMAGE, Data.getImage());
        db.insert(TABLE_FAKULTAS, null, values);
    }
    public static List<MstFakultas> getAllFakultas() {
        List<MstFakultas> DataList   = new ArrayList<MstFakultas>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_FAKULTAS;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstFakultas Data = new MstFakultas();
                Data.setFakultasId(cursor.getString(0));
                Data.setFakultasName(cursor.getString(1));
                Data.setImage(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteFakultas() {
        if (getAllFakultas().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_FAKULTAS);
        }
    }

    public static void addDataJurusan(MstJurusan Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_JURUSAN_ID, Data.getJurusanId());
        values.put(FIELD_JURUSAN_NAME, Data.getJurusanName());
        values.put(FIELD_JENJANG, Data.getJenjang());
        values.put(FIELD_FAKULTAS_ID, Data.getFakultasId());
        db.insert(TABLE_JURUSAN, null, values);
    }
    public static List<MstJurusan> getAllJurusan() {
        List<MstJurusan> DataList   = new ArrayList<MstJurusan>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_JURUSAN;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstJurusan Data = new MstJurusan();
                Data.setJurusanId(cursor.getString(0));
                Data.setJurusanName(cursor.getString(1));
                Data.setJenjang(cursor.getString(2));
                Data.setFakultasId(cursor.getString(3));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static MstJurusan getJurusanById(String strData) {
        String selectQuery 			= "SELECT  * FROM " + TABLE_JURUSAN+ " WHERE " + FIELD_JURUSAN_ID + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        MstJurusan Data             = new MstJurusan();
        if (cursor.moveToFirst()) {
            do {
                Data.setJurusanId(cursor.getString(0));
                Data.setJurusanName(cursor.getString(1));
                Data.setJenjang(cursor.getString(2));
                Data.setFakultasId(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return Data;
    }
    public static List<MstJurusan> getJurusanByFakultas(String strData) {
        List<MstJurusan> DataList   = new ArrayList<MstJurusan>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_JURUSAN+ " WHERE " + FIELD_FAKULTAS_ID + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstJurusan Data = new MstJurusan();
                Data.setJurusanId(cursor.getString(0));
                Data.setJurusanName(cursor.getString(1));
                Data.setJenjang(cursor.getString(2));
                Data.setFakultasId(cursor.getString(3));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteJurusan() {
        if (getAllJurusan().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_JURUSAN);
        }
    }
}