package com.nmk.siimut.MdlFakultas;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface FakultasService {

    @GET(Constants.API_GET_STUDI)
    Call<FakultasResponse> getDataList(@Query("id") String UserID);
}
