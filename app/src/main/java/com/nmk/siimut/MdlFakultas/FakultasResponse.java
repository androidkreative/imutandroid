package com.nmk.siimut.MdlFakultas;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class FakultasResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstFakultas> DataList    = new ArrayList<MstFakultas>();

    public List<MstFakultas> getDataList() {
        return DataList;
    }

}
