package com.nmk.siimut.MdlFakultas;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstJurusan {
    @SerializedName("JURUSAN_ID")
    String JurusanId;

    @SerializedName("JURUSAN_NAME")
    String JurusanName;

    @SerializedName("JENJANG")
    String Jenjang;

    String FakultasId;

    public MstJurusan() {
    }

    public MstJurusan(String jurusanId, String jurusanName, String jenjang, String fakultasId) {
        JurusanId   = jurusanId;
        JurusanName = jurusanName;
        Jenjang     = jenjang;
        FakultasId  = fakultasId;
    }

    public String getJurusanId() {
        return JurusanId;
    }

    public void setJurusanId(String jurusanId) {
        JurusanId = jurusanId;
    }

    public String getJurusanName() {
        return JurusanName;
    }

    public void setJurusanName(String jurusanName) {
        JurusanName = jurusanName;
    }

    public String getJenjang() {
        return Jenjang;
    }

    public void setJenjang(String jenjang) {
        Jenjang = jenjang;
    }

    public String getFakultasId() {
        return FakultasId;
    }

    public void setFakultasId(String fakultasId) {
        FakultasId = fakultasId;
    }
}