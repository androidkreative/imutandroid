package com.nmk.siimut.MdlFakultas;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlBiaya.BiayaActivity;
import com.nmk.siimut.MdlBiaya.MstBiaya;
import com.nmk.siimut.MdlBiaya.MstBiayaDetail;
import com.nmk.siimut.MdlBiaya.SqlBiaya;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class FakultasActivity extends AppCompatActivity implements View.OnClickListener{

    private List<MstFakultas> dataList   = new ArrayList<MstFakultas>();
    private RecyclerView dataView;
    private RelativeLayout noInternetView;
//    private BottomSheetBehavior behaviorAddress;
//    private BottomSheetDialog biayaDialog;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(FakultasActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_HOME);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fakultas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        ImageView buttonBack    = findViewById(R.id.btnBack);
        buttonBack.setOnClickListener(this);
        dataView                = findViewById(R.id.viewFakultas);
        dataView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
        noInternetView          = findViewById(R.id.viewUnavailable);
        noInternetView.setVisibility(View.GONE);
        onSetFakultasData();

        //SET BOTTOM SHEET
//        View bottomBiaya        = findViewById(R.id.bottomSheetBiaya);
//        behaviorAddress         = BottomSheetBehavior.from(bottomBiaya);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }
    private void onSetFakultasData(){
        if (dataList.size() > 0)
            dataList.clear();
        dataList = SqlFakultas.getAllFakultas();
        if (dataList.size() > 0){
            dataAdapter newAdapter   = new dataAdapter(dataList);
            dataView.setAdapter(newAdapter);
            dataView.setNestedScrollingEnabled(false);
            dataView.setVisibility(View.VISIBLE);
            noInternetView.setVisibility(View.GONE);
        }else{
            dataView.setVisibility(View.GONE);
            noInternetView.setVisibility(View.VISIBLE);
        }
    }
    class dataAdapter extends RecyclerView.Adapter<dataAdapter.ViewHolder> {
        private List<MstFakultas> mItems;

        private dataAdapter(List<MstFakultas> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fakultas, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstFakultas objectData   = mItems.get(position);
            viewHolder.fakultasView.setText(objectData.getFakultasName());
            Picasso.get()
                    .load(Uri.parse(objectData.getImage()))
                    .into(viewHolder.fakultasImage);

            List<MstJurusan> jurusanList = SqlFakultas.getJurusanByFakultas(objectData.getFakultasId());
            if (jurusanList.size() > 0){
                jurusanAdapter newAdapter   = new jurusanAdapter(jurusanList);
                viewHolder.jurusanView.setAdapter(newAdapter);
                viewHolder.jurusanView.setNestedScrollingEnabled(false);
            }
            viewHolder.layoutItem.setOnClickListener(v -> {
                Intent intent   = new Intent(FakultasActivity.this, BiayaActivity.class);
                intent.putExtra(Constants.PARAMS_KEY, objectData.FakultasId);
                startActivity(intent);
//                onShowDialogBiaya(objectData.FakultasId);
            });
            viewHolder.buttonBiaya.setOnClickListener(v -> {
                Intent intent   = new Intent(FakultasActivity.this, BiayaActivity.class);
                intent.putExtra(Constants.PARAMS_KEY, objectData.FakultasId);
                startActivity(intent);
//                onShowDialogBiaya(objectData.FakultasId);
            });
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            RelativeLayout layoutItem;
            TextView fakultasView, buttonBiaya;
            RecyclerView jurusanView;
            ImageView fakultasImage;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                fakultasView    = itemView.findViewById(R.id.viewFakultas);
                fakultasImage   = itemView.findViewById(R.id.imgFakultas);
                buttonBiaya     = itemView.findViewById(R.id.btnBiaya);
                jurusanView     = itemView.findViewById(R.id.viewJurusan);
                jurusanView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
            }
        }
    }
    class jurusanAdapter extends RecyclerView.Adapter<jurusanAdapter.ViewHolder> {
        private List<MstJurusan> mItems;

        private jurusanAdapter(List<MstJurusan> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_jurusan, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstJurusan objectData   = mItems.get(position);
            viewHolder.jurusanView.setText(objectData.getJurusanName());
            viewHolder.jenjangView.setText(objectData.getJenjang());
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView jurusanView, jenjangView;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                jurusanView     = itemView.findViewById(R.id.viewJurusan);
                jenjangView     = itemView.findViewById(R.id.viewJenjang);
            }
        }
    }

//    public void onShowDialogBiaya(String strData) {
//        if (behaviorAddress.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//            behaviorAddress.setState(BottomSheetBehavior.STATE_COLLAPSED);
//        }
//        biayaDialog                 = new BottomSheetDialog(this);
//        final View view             = getLayoutInflater().inflate(R.layout.item_biaya_main, null);
//        ImageView buttonClose       = view.findViewById(R.id.btnClose);
//        RecyclerView biayaMain      = view.findViewById(R.id.viewBiayaMain);
//        biayaMain.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
//        List<MstBiaya> dataBiaya    = SqlBiaya.getBiayaByFakultas(strData);
//        if (dataBiaya.size() > 0){
//            biayaMainAdapter newAdapter = new biayaMainAdapter(dataBiaya);
//            biayaMain.setAdapter(newAdapter);
//            biayaMain.setNestedScrollingEnabled(false);
//        }
//        buttonClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (biayaDialog != null) biayaDialog.dismiss();
//            }
//        });
//        biayaDialog.setContentView(view);
//        biayaDialog.setCanceledOnTouchOutside(false);
//        biayaDialog.show();
//        biayaDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                biayaDialog = null;
//            }
//        });
//    }
//    class biayaMainAdapter extends RecyclerView.Adapter<biayaMainAdapter.ViewHolder> {
//        private List<MstBiaya> mItems;
//
//        private biayaMainAdapter(List<MstBiaya> itemData) {
//            super();
//            mItems          = itemData;
//        }
//
//        @NonNull
//        @Override
//        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_biaya_child, viewGroup, false);
//            return new ViewHolder(v);
//        }
//        @Override
//        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
//            final MstBiaya objectData   = mItems.get(position);
//            viewHolder.titleView.setText(objectData.getDescription());
//            List<MstBiayaDetail> dataDetail = SqlBiaya.getBiayaByID(objectData.getBiayaId());
//            if (dataDetail.size() > 0){
//                biayaChildAdapter newAdapter         = new biayaChildAdapter(dataDetail, objectData.getFirst());
//                viewHolder.biayaView.setAdapter(newAdapter);
//                viewHolder.biayaView.setNestedScrollingEnabled(false);
//                viewHolder.totalView.setText("Rp. "+ Utility.currencyFormat(newAdapter.getTotal()));
//            }
//            if (objectData.getFirst().contentEquals("1"))
//                viewHolder.layoutItem.setBackground(getResources().getDrawable(R.drawable.bg_card_first));
//        }
//        @Override
//        public int getItemCount() {
//            return mItems.size();
//        }
//        private class ViewHolder extends RecyclerView.ViewHolder {
//            LinearLayout layoutItem;
//            TextView titleView, totalView;
//            RecyclerView biayaView;
//
//            private ViewHolder(View itemView) {
//                super(itemView);
//                layoutItem      = itemView.findViewById(R.id.itemLayout);
//                titleView       = itemView.findViewById(R.id.viewTitle);
//                totalView       = itemView.findViewById(R.id.viewTotal);
//                biayaView       = itemView.findViewById(R.id.viewBiaya);
//                biayaView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
//            }
//        }
//    }
//    class biayaChildAdapter extends RecyclerView.Adapter<biayaChildAdapter.ViewHolder> {
//        private List<MstBiayaDetail> mItems;
//        private String strFirst;
//
//        private biayaChildAdapter(List<MstBiayaDetail> itemData, String isFirst) {
//            super();
//            mItems      = itemData;
//            strFirst    = isFirst;
//        }
//
//        @NonNull
//        @Override
//        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_biaya, viewGroup, false);
//            return new ViewHolder(v);
//        }
//        @Override
//        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
//            final MstBiayaDetail objectData   = mItems.get(position);
//            viewHolder.viewDesc.setText(objectData.getDescription());
//            viewHolder.viewBiaya.setText(Utility.currencyFormat(Double.valueOf(objectData.getBiaya())));
//            if (strFirst.contentEquals("1")){
//                viewHolder.viewDesc.setTextColor(getResources().getColor(R.color.colorTitle));
//                viewHolder.viewBiaya.setTextColor(getResources().getColor(R.color.colorTitle));
//            }
//        }
//        @Override
//        public int getItemCount() {
//            return mItems.size();
//        }
//        public int getTotal() {
//            int total = 0;
//            int countItem = mItems.size();
//            for (int i = 0; i < countItem; i++) {
//                MstBiayaDetail ObjItem   = mItems.get(i);
//                total = total + Integer.valueOf(ObjItem.getBiaya());
//            }
//            return total;
//        }
//        private class ViewHolder extends RecyclerView.ViewHolder {
//            LinearLayout layoutItem;
//            TextView viewDesc, viewBiaya;
//
//            private ViewHolder(View itemView) {
//                super(itemView);
//                layoutItem      = itemView.findViewById(R.id.itemLayout);
//                viewDesc        = itemView.findViewById(R.id.descView);
//                viewBiaya       = itemView.findViewById(R.id.biayaView);
//            }
//        }
//    }
}
