package com.nmk.siimut.MdlFakultas;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstFakultas {
    @SerializedName("FAKULTAS_ID")
    String FakultasId;

    @SerializedName("FAKULTAS_NAME")
    String FakultasName;

    @SerializedName("IMAGE")
    String Image;

    @SerializedName("JURUSAN")
    List<MstJurusan> JurusanList = new ArrayList<MstJurusan>();

    public MstFakultas() {
    }

    public MstFakultas(String fakultasId, String fakultasName, String image) {
        FakultasId      = fakultasId;
        FakultasName    = fakultasName;
        Image           = image;
    }

    public String getFakultasId() {
        return FakultasId;
    }

    public void setFakultasId(String fakultasId) {
        FakultasId = fakultasId;
    }

    public String getFakultasName() {
        return FakultasName;
    }

    public void setFakultasName(String fakultasName) {
        FakultasName = fakultasName;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public List<MstJurusan> getJurusanList() {
        return JurusanList;
    }

    public void setJurusanList(List<MstJurusan> jurusanList) {
        JurusanList = jurusanList;
    }
}