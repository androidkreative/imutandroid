package com.nmk.siimut.Fcm;

public class Config {
    public static final int NOTIFICATION_ID             = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE   = 101;
    public static final String FCM_TOKEN                = "FCM_TOKEN";
    public static final String SHARED_PREF              = "ah_firebase";
    public static final String UPDATE_CONTENT           = "update_content";
}
