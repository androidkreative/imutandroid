package com.nmk.siimut.Fcm;


import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.MdlNotifikasi.NotifikasiActivity;
import com.nmk.siimut.MdlNotifikasi.NotifikasiDetailActivity;
import com.nmk.siimut.Mdlinformasi.InformasiDetailActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;

import java.util.List;
import java.util.Map;

public class FcmService extends FirebaseMessagingService {

    private NotificationUtils notificationUtils;
    private String infoId, notifId;

    @Override
    public void onNewToken(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Config.FCM_TOKEN, token);
        editor.commit();
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null)
            return;

        //CHECK IF MESSAGE CONTAINS A DATA PAYLOAD
        if (remoteMessage.getNotification() != null && remoteMessage.getData().size() > 0) {
            try {
                handlePayloadData(remoteMessage);
            } catch (Exception e) {}
        }else
            handleNotification(remoteMessage, "");
    }
    private void handlePayloadData(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        try {
            String targetModule = data.get("targetModule");
            if (targetModule.contains(","))
            {
                //PARSING DELIMETER ( , )  TARGETMODULE
                String[] separated = targetModule.split(",");
                for (String strSeparated : separated)
                {
                    targetModuleHandling(remoteMessage, strSeparated);
                }
            }
            else
            {
                targetModuleHandling(remoteMessage, targetModule);
            }

        } catch (Exception ignored) { }
    }
    private void targetModuleHandling(RemoteMessage remoteMessage, String targetModule){
        Map<String, String> data    = remoteMessage.getData();
        String action               = data.get("notificationAction");
        switch (targetModule) {
            case "informasi":
                infoId = data.get("infoId");
                handleNotification(remoteMessage, targetModule);
                break;
            case "notif":
                notifId = data.get("notifId");
                handleNotification(remoteMessage, targetModule);
                break;
            default:
                handleNotification(remoteMessage, targetModule);
                break;
        }
    }
    private void handleNotification(RemoteMessage remoteMessage, String strModule){
        if (remoteMessage.getNotification() != null) {
            Intent targetIntent;
            switch (strModule) {
                case "informasi":
                    targetIntent    = new Intent(this, InformasiDetailActivity.class);
                    targetIntent.putExtra(Constants.VIEW_KEY, Constants.VIEW_INFO);
                    targetIntent.putExtra(Constants.INFO_ID, infoId);
                    break;
                case "notif":
                    targetIntent   = new Intent(this, NotifikasiDetailActivity.class);
                    targetIntent.putExtra(Constants.NOTIF_ID, notifId);
                    break;
                default:
                    targetIntent = new Intent(this, NotifikasiActivity.class);
                    break;
            }
            RemoteMessage.Notification NotificationContent = remoteMessage.getNotification();
            if (!isAppIsInBackground(getApplicationContext()))
            {
                notificationRunningApps(remoteMessage.getNotification(), targetIntent);
            }
            else
            {
                notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.showNotificationMessage(NotificationContent.getTitle(), NotificationContent.getBody(), String.valueOf(remoteMessage.getSentTime()), targetIntent);
            }
        }
    }
    private void notificationRunningApps(RemoteMessage.Notification Message, Intent targetIntent){
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId    = getString(R.string.default_notification_channel_id);
        Uri soundUri        = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder  = new NotificationCompat.Builder(this, channelId);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(Message.getTitle())
                .setContentText(Message.getBody())
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setLights(Color.BLUE,1,1)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setContentIntent(contentIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            notificationBuilder.setPriority(NotificationManager.IMPORTANCE_MAX);
        }else{
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(0, notificationBuilder.build());
    }
    private Runnable onRefreshUI(final String strUpdateFilter) {
        Runnable mRun = new Runnable(){
            public void run(){
                //FOR REFRESH UI
                Intent i = new Intent(strUpdateFilter);
                i.putExtra(strUpdateFilter, Config.UPDATE_CONTENT);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
            }
        };
        return mRun;
    }
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }
}
