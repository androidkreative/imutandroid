package com.nmk.siimut.MdlMain;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.os.Bundle;

import android.text.Html;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class AboutActivity extends AppCompatActivity{

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(AboutActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_ACCOUNT);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        TextView viewContent = findViewById(R.id.aboutContent);
        viewContent.setText(Html.fromHtml(getString(R.string.label_about_desc)));
    }



    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
