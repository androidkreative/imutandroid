package com.nmk.siimut.MdlMain;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.Fcm.Config;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlAccount.User.UserTask;
import com.nmk.siimut.MdlAddress.AddressResponse;
import com.nmk.siimut.MdlAddress.AddressService;
import com.nmk.siimut.MdlAddress.MstAddress;
import com.nmk.siimut.MdlAddress.SqlAddress;
import com.nmk.siimut.MdlBiaya.BiayaResponse;
import com.nmk.siimut.MdlBiaya.BiayaService;
import com.nmk.siimut.MdlBiaya.MstBiaya;
import com.nmk.siimut.MdlBiaya.MstBiayaDetail;
import com.nmk.siimut.MdlBiaya.SqlBiaya;
import com.nmk.siimut.MdlFakultas.FakultasResponse;
import com.nmk.siimut.MdlFakultas.FakultasService;
import com.nmk.siimut.MdlFakultas.MstFakultas;
import com.nmk.siimut.MdlFakultas.MstJurusan;
import com.nmk.siimut.MdlFakultas.SqlFakultas;
import com.nmk.siimut.MdlFasilitas.FasilitasResponse;
import com.nmk.siimut.MdlFasilitas.FasilitasService;
import com.nmk.siimut.MdlFasilitas.MstFasilitas;
import com.nmk.siimut.MdlFasilitas.SqlFasilitas;
import com.nmk.siimut.MdlHome.MstProfile;
import com.nmk.siimut.MdlHome.ProfileResponse;
import com.nmk.siimut.MdlHome.ProfileService;
import com.nmk.siimut.MdlHome.SqlProfile;
import com.nmk.siimut.MdlOther.AgamaResponse;
import com.nmk.siimut.MdlOther.AjaranResponse;
import com.nmk.siimut.MdlOther.MstAgama;
import com.nmk.siimut.MdlOther.MstAjaran;
import com.nmk.siimut.MdlOther.MstStatus;
import com.nmk.siimut.MdlOther.OtherService;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.MdlOther.StatusResponse;
import com.nmk.siimut.MdlPendaftaran.JadwalResponse;
import com.nmk.siimut.MdlPendaftaran.MarketResponse;
import com.nmk.siimut.MdlPendaftaran.MstJadwal;
import com.nmk.siimut.MdlPendaftaran.MstMarket;
import com.nmk.siimut.MdlPendaftaran.MstSyarat;
import com.nmk.siimut.MdlPendaftaran.PendaftaranService;
import com.nmk.siimut.MdlPendaftaran.SqlDaftar;
import com.nmk.siimut.MdlPendaftaran.SyaratResponse;
import com.nmk.siimut.MdlRelasi.MstRelasi;
import com.nmk.siimut.MdlRelasi.RelasiResponse;
import com.nmk.siimut.MdlRelasi.RelasiService;
import com.nmk.siimut.MdlRelasi.SqlRelasi;
import com.nmk.siimut.R;
import com.nmk.siimut.MdlBanner.BannerResponse;
import com.nmk.siimut.MdlBanner.BannerService;
import com.nmk.siimut.MdlBanner.MstBanner;
import com.nmk.siimut.MdlBanner.SqlBanner;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.List;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class SplashActivity extends AppCompatActivity {

    private ShimmerLayout shimmerLayout;
    private RelativeLayout layoutProccessing;
    private ProgressBar progressBar;
    private TextView proccessView;
    private int dataCount;

    @Override
    public void onBackPressed(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        Window w            = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.FIRST_OPEN, "1");
        editor.apply();

        shimmerLayout       = findViewById(R.id.shimmer_layout);
        shimmerLayout.startShimmerAnimation();
        layoutProccessing   = findViewById(R.id.LayoutProcentProgressBar);
        layoutProccessing.setVisibility(View.GONE);
        progressBar         = findViewById(R.id.procentProgressBar);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        proccessView        = findViewById(R.id.viewPercent);

        if (Utility.isNetworkConnected()) {
            List<MstUser> dataList = SqlUser.getAllData();
            if (dataList.size() >0)
                onGetAccount.run();
            else
                onGetBanner.run();
        }else
            gotoMain.run();
    }
    private Runnable gotoMain = new Runnable() {
        @Override
        public void run() {
            Intent intent   = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
        }
    };
    private Runnable onGetAccount = new Runnable() {
        @Override
        public void run() {
            UserTask.onGetAccount(SplashActivity.this, onGetUserProfile, onGetUserProfile);
        }
    };
    private Runnable onGetUserProfile = new Runnable() {
        @Override
        public void run() {
            UserTask.onGetProfile(SplashActivity.this, onGetBanner, onGetBanner);
        }
    };
    private Runnable onGetBanner = new Runnable() {
        @Override
        public void run() {
            new onGetListBanner().execute(0);
        }
    };
    class onGetListBanner extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                BannerService service               = ApiClient.getClient().create(BannerService.class);
                Call<BannerResponse> mRequest       = service.getBannerList("");
                Response<BannerResponse> Response   = mRequest.execute();
                BannerResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlBanner.DeleteData();
                            List<MstBanner> ListData    = Result.getDataList();
                            dataCount                   = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstBanner objData  = ListData.get(i);
                                    SqlBanner.addData(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlBanner.DeleteData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetProfile().execute(0);
        }
    }
    class onGetProfile extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                ProfileService service              = ApiClient.getClient().create(ProfileService.class);
                Call<ProfileResponse> mRequest      = service.getProfile("");
                Response<ProfileResponse> Response  = mRequest.execute();
                ProfileResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlProfile.DeleteData();
                            List<MstProfile> ListData    = Result.getDataList();
                            dataCount                   = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstProfile objData  = ListData.get(i);
                                    SqlProfile.addData(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlProfile.DeleteData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetFakultas().execute(0);
        }
    }
    class onGetFakultas extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                FakultasService service             = ApiClient.getClient().create(FakultasService.class);
                Call<FakultasResponse> mRequest     = service.getDataList("");
                Response<FakultasResponse> Response = mRequest.execute();
                FakultasResponse Result             = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlFakultas.DeleteFakultas();
                            SqlFakultas.DeleteJurusan();
                            List<MstFakultas> ListData      = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstFakultas objData  = ListData.get(i);
                                    SqlFakultas.addDataFakultas(new MstFakultas(objData.getFakultasId(), objData.getFakultasName(), objData.getImage()));

                                    List<MstJurusan> JurusanList = objData.getJurusanList();
                                    if (JurusanList.size() > 0){
                                        for (int j=0; j < JurusanList.size(); j++){
                                            MstJurusan objJurusan  = JurusanList.get(j);
                                            SqlFakultas.addDataJurusan(new MstJurusan(
                                                    objJurusan.getJurusanId(),
                                                    objJurusan.getJurusanName(),
                                                    objJurusan.getJenjang(),
                                                    objData.getFakultasId()
                                            ));
                                        }
                                    }
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlFakultas.DeleteFakultas();
                            SqlFakultas.DeleteJurusan();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            if (SqlFasilitas.getAllFasilitas().size() > 0)
                new onGetRelasi().execute(0);
            else
                new onGetFasilitas().execute(0);
        }
    }
    class onGetFasilitas extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                FasilitasService service             = ApiClient.getClient().create(FasilitasService.class);
                Call<FasilitasResponse> mRequest     = service.getDataList("");
                Response<FasilitasResponse> Response = mRequest.execute();
                FasilitasResponse Result             = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlFasilitas.DeleteFasilitas();
                            List<MstFasilitas> ListData     = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstFasilitas objData  = ListData.get(i);
                                    SqlFasilitas.addDataFasilitas(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlFasilitas.DeleteFasilitas();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            if (SqlRelasi.getAllRelasi().size() > 0)
                new onGetBiaya().execute(0);
            else
                new onGetRelasi().execute(0);
        }
    }
    class onGetRelasi extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                RelasiService service               = ApiClient.getClient().create(RelasiService.class);
                Call<RelasiResponse> mRequest       = service.getDataList("");
                Response<RelasiResponse> Response   = mRequest.execute();
                RelasiResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlRelasi.DeleteRelasi();
                            List<MstRelasi> ListData    = Result.getDataList();
                            dataCount                   = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstRelasi objData  = ListData.get(i);
                                    SqlRelasi.addDataRelasi(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlRelasi.DeleteRelasi();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetBiaya().execute(0);
        }
    }
    class onGetBiaya extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                BiayaService service                = ApiClient.getClient().create(BiayaService.class);
                Call<BiayaResponse> mRequest        = service.getDataList("");
                Response<BiayaResponse> Response    = mRequest.execute();
                BiayaResponse Result                = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlBiaya.DeleteBiaya();
                            SqlBiaya.DeleteBiayaDetail();
                            List<MstBiaya> ListData         = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstBiaya objData  = ListData.get(i);
                                    SqlBiaya.addDataBiaya(new MstBiaya(objData.getBiayaId(), objData.getDescription(), objData.getFakultas(), objData.getFirst()));

                                    List<MstBiayaDetail> DetailList = objData.getDetailList();
                                    if (DetailList.size() > 0){
                                        for (int j=0; j < DetailList.size(); j++){
                                            MstBiayaDetail objDetail  = DetailList.get(j);
                                            SqlBiaya.addDataBiayaDetail(new MstBiayaDetail(
                                                    objDetail.getDetailId(),
                                                    objDetail.getDescription(),
                                                    objDetail.getBiaya(),
                                                    objData.getBiayaId()
                                            ));
                                        }
                                    }
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlBiaya.DeleteBiaya();
                            SqlBiaya.DeleteBiayaDetail();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetAddress().execute(0);
        }
    }
    class onGetAddress extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                AddressService service              = ApiClient.getClient().create(AddressService.class);
                Call<AddressResponse> mRequest      = service.getAddress("");
                Response<AddressResponse> Response  = mRequest.execute();
                AddressResponse Result              = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlAddress.DeleteData();
                            List<MstAddress> ListData       = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstAddress objData  = ListData.get(i);
                                    SqlAddress.addData(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlAddress.DeleteData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetAgama().execute(0);
        }
    }
    class onGetAgama extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                OtherService service                = ApiClient.getClient().create(OtherService.class);
                Call<AgamaResponse> mRequest        = service.getAgama("");
                Response<AgamaResponse> Response    = mRequest.execute();
                AgamaResponse Result                = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlOther.DeleteAgama();
                            List<MstAgama> ListData         = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstAgama objData  = ListData.get(i);
                                    SqlOther.addDataAgama(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlOther.DeleteAgama();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            if (SqlOther.getAllStatus().size() > 0)
                new onGetSyarat().execute(0);
            else
                new onGetStatus().execute(0);
        }
    }
    class onGetStatus extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                OtherService service                = ApiClient.getClient().create(OtherService.class);
                Call<StatusResponse> mRequest       = service.getStatus("");
                Response<StatusResponse> Response   = mRequest.execute();
                StatusResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlOther.DeleteStatus();
                            List<MstStatus> ListData        = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstStatus objData  = ListData.get(i);
                                    SqlOther.addDataStatus(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlOther.DeleteStatus();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetSyarat().execute(0);
        }
    }
    class onGetSyarat extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                PendaftaranService service          = ApiClient.getClient().create(PendaftaranService.class);
                Call<SyaratResponse> mRequest       = service.getDataList("");
                Response<SyaratResponse> Response   = mRequest.execute();
                SyaratResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlDaftar.DeleteSyarat();
                            List<MstSyarat> ListData        = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstSyarat objData  = ListData.get(i);
                                    SqlDaftar.addDataSyarat(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlDaftar.DeleteSyarat();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetJadwal().execute(0);
        }
    }
    class onGetJadwal extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                PendaftaranService service          = ApiClient.getClient().create(PendaftaranService.class);
                Call<JadwalResponse> mRequest       = service.getJadwalList("");
                Response<JadwalResponse> Response   = mRequest.execute();
                JadwalResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlDaftar.DeleteJadwal();
                            List<MstJadwal> ListData        = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstJadwal objData  = ListData.get(i);
                                    SqlDaftar.addDataJadwal(objData);
                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlDaftar.DeleteJadwal();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetMarket().execute(0);
        }
    }
    class onGetMarket extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                PendaftaranService service          = ApiClient.getClient().create(PendaftaranService.class);
                Call<MarketResponse> mRequest       = service.getMarketList("");
                Response<MarketResponse> Response   = mRequest.execute();
                MarketResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            List<MstMarket> ListData        = Result.getDataList();
                            dataCount                       = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstMarket objData           = ListData.get(i);
                                    List<MstMarket> dataLocal   = SqlDaftar.getMarketById(objData.getId());
                                    if (dataLocal.size() > 0) {
                                        MstMarket objMarket = dataLocal.get(0);
                                        SqlDaftar.UpdateMarket(new MstMarket(objData.getId(), objData.getDescription(), objMarket.getChecked()));
                                    }else
                                        SqlDaftar.addDataMarket(new MstMarket(objData.getId(), objData.getDescription(), "0"));

                                    publishProgress(i + 1);
                                }
                            }
                            break;
                        case 202:
                            SqlDaftar.DeleteMarket();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            new onGetAjaran().execute(0);
        }
    }
    class onGetAjaran extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
//            layoutProccessing.setVisibility(View.VISIBLE);
            proccessView.setText(getResources().getString(R.string.loading));
            progressBar.setProgress(0);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                OtherService service                = ApiClient.getClient().create(OtherService.class);
                Call<AjaranResponse> mRequest       = service.getAjaran("");
                Response<AjaranResponse> Response   = mRequest.execute();
                AjaranResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlOther.DeleteAjaran();
                            MstAjaran objData   = Result.getObjData();
                            SqlOther.addDataAjaran(objData);
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dataCount > 0)
            {
                double procent = (values[0] * 100) / dataCount;
                proccessView.setText(getResources().getString(R.string.downloading)+" ("+ procent +"%)");
                progressBar.setProgress((int) procent);
            }
        }
        @Override
        protected void onPostExecute(String result) {
            progressBar.setProgress(0);
            proccessView.setText(result);
            gotoMain.run();
        }
    }





    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
