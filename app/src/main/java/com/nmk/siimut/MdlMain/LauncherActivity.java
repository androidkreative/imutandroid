package com.nmk.siimut.MdlMain;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlHome.MstProfile;
import com.nmk.siimut.MdlHome.ProfileResponse;
import com.nmk.siimut.MdlHome.ProfileService;
import com.nmk.siimut.MdlHome.SqlProfile;
import com.nmk.siimut.MdlNotifikasi.MstNotifikasi;
import com.nmk.siimut.MdlNotifikasi.NotifikasiActivity;
import com.nmk.siimut.MdlNotifikasi.NotifikasiDetailActivity;
import com.nmk.siimut.MdlNotifikasi.NotifikasiResponse;
import com.nmk.siimut.MdlNotifikasi.NotifikasiService;
import com.nmk.siimut.Mdlinformasi.InformasiDetailActivity;
import com.nmk.siimut.Mdlinformasi.InformasiResponse;
import com.nmk.siimut.Mdlinformasi.InformasiService;
import com.nmk.siimut.Mdlinformasi.MstInformasi;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class LauncherActivity extends AppCompatActivity {

    private String strUserId, strUserAuth, notifId, infoId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        Window w            = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        RelativeLayout layoutProccessing   = findViewById(R.id.LayoutProcentProgressBar);
        layoutProccessing.setVisibility(View.GONE);

        List<MstUser> dataList = SqlUser.getAllData();
        if (dataList.size() >0){
            if (Utility.isNetworkConnected()) {
                Intent bundle           = getIntent();
                if (bundle.getStringExtra("targetModule") != null)
                {
                    switch (bundle.getStringExtra("targetModule")) {
                        case "informasi":
                            infoId = bundle.getStringExtra("infoId");
                            new onGetInfoDetail().execute(0);
                            break;
                        case "notif":
                            notifId = bundle.getStringExtra("notifId");
                            new onGetNotifDetail().execute(0);
                            break;
                    }
                }else
                    gotoSplash.run();
            }else
                gotoSplash.run();
        }else
            gotoSplash.run();
    }
    private Runnable gotoSplash = new Runnable() {
        @Override
        public void run() {
            Intent intent   = new Intent(LauncherActivity.this, SplashActivity.class);
            startActivity(intent);
        }
    };

    @SuppressLint("StaticFieldLeak")
    class onGetNotifDetail extends AsyncTask<Integer, Integer, String> {
        MstNotifikasi objNotif = null;
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                List<MstUser> dataList = SqlUser.getAllData();
                if (dataList.size() > 0) {
                    MstUser data    = dataList.get(0);
                    strUserId       = data.getUserId();
                    strUserAuth     = data.getUserAuth();
                }

                NotifikasiService service               = ApiClient.getClient().create(NotifikasiService.class);
                Call<NotifikasiResponse> mRequest       = service.getNotifDetail(strUserId, strUserAuth, notifId);
                Response<NotifikasiResponse> Response   = mRequest.execute();
                NotifikasiResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            objNotif = Result.getData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            Intent intentNotif   = new Intent(LauncherActivity.this, NotifikasiDetailActivity.class);
            intentNotif.putExtra(Constants.NOTIF_ID, notifId);
            intentNotif.putExtra(Constants.PARAMS_KEY, objNotif);
            startActivity(intentNotif);
        }
    }
    @SuppressLint("StaticFieldLeak")
    class onGetInfoDetail extends AsyncTask<Integer, Integer, String> {
        MstInformasi objInfo = null;
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                List<MstUser> dataList = SqlUser.getAllData();
                if (dataList.size() > 0) {
                    MstUser data    = dataList.get(0);
                    strUserId       = data.getUserId();
                    strUserAuth     = data.getUserAuth();
                }

                InformasiService service                = ApiClient.getClient().create(InformasiService.class);
                Call<InformasiResponse> mRequest        = service.getInformasiDetail(infoId);
                Response<InformasiResponse> Response   = mRequest.execute();
                InformasiResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            objInfo = Result.getData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            Intent intentNotif   = new Intent(LauncherActivity.this, InformasiDetailActivity.class);
            intentNotif.putExtra(Constants.VIEW_KEY, Constants.VIEW_INFO);
            intentNotif.putExtra(Constants.INFO_ID, infoId);
            intentNotif.putExtra(Constants.PARAMS_KEY, objInfo);
            startActivity(intentNotif);
        }
    }

    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}