package com.nmk.siimut.MdlMain;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.nmk.siimut.Fcm.Config;
import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlAccount.FragmentAccount;
import com.nmk.siimut.MdlAccount.User.ChangePasswordActivity;
import com.nmk.siimut.MdlAccount.User.ProfileDetailActivity;
import com.nmk.siimut.MdlAccount.User.SignInActivity;
import com.nmk.siimut.MdlAccount.User.UserTask;
import com.nmk.siimut.MdlBayar.FragmentBayar;
import com.nmk.siimut.MdlHome.FragmentHome;
import com.nmk.siimut.MdlNotifikasi.NotifikasiActivity;
import com.nmk.siimut.MdlPendaftaran.FragmentPendaftaran;
import com.nmk.siimut.Mdlinformasi.FragmentInformasi;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.BottomNav.BottomNavigation;
import com.nmk.siimut.UiDesign.BottomNav.TabItem;
import com.nmk.siimut.UiDesign.BottomNav.events.OnSelectedItemChangeListener;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;

import org.jsoup.Jsoup;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FragmentTransaction transaction;
    private static BottomNavigation bottomNavigation;
    private static TabItem tabHome;
    private static TabItem tabAccount;
    private static TabItem tabPmb;
    private static TabItem tabInfo;
    private static TabItem tabBayar;
    private String strStateView;
    private boolean isClick = false;

    @Override
    public void onBackPressed(){
        CloseApps.run();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        bottomNavigation    = findViewById(R.id.bottom_navigation);
        bottomNavigation.setType(BottomNavigation.TYPE_FIXED);
        tabHome             = findViewById(R.id.tab_home);
        tabPmb              = findViewById(R.id.tab_pmb);
        tabInfo             = findViewById(R.id.tab_info);
        tabBayar            = findViewById(R.id.tab_bayar);
//        tabBayar.setVisibility(View.GONE);
        tabAccount          = findViewById(R.id.tab_account);

        onSetBottomNav(MainActivity.this);
        InitBottomNav();
        if (Utility.isNetworkConnected()) {
            SharedPreferences pref  = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
            String isFirstOpen      = pref.getString(Constants.FIRST_OPEN, "");
            assert isFirstOpen != null;
            if (isFirstOpen.contentEquals("1")) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(Constants.FIRST_OPEN, "0");
                editor.apply();
                new GetVersionCode().execute();
            }
        }
    }
    public void InitBottomNav(){
        Intent bundle       = getIntent();
        if (bundle.getStringExtra(Constants.VIEW_KEY) != null){
            strStateView    = bundle.getStringExtra(Constants.VIEW_KEY);
            switch (strStateView) {
                case Constants.VIEW_HOME:
                    bottomNavigation.setDefaultItem(0);
                    break;
                    case Constants.VIEW_PMB:
                    bottomNavigation.setDefaultItem(1);
                    break;
                case Constants.VIEW_INFO:
                    bottomNavigation.setDefaultItem(2);
                    break;
                case Constants.VIEW_BAYAR:
                    bottomNavigation.setDefaultItem(3);
                    break;
                case Constants.VIEW_ACCOUNT:
                    bottomNavigation.setDefaultItem(bottomNavigation.getChildCount()-1);
                    break;
                default:
                    bottomNavigation.setDefaultItem(0);
                    break;
            }
        }else {
            strStateView    = Constants.VIEW_HOME;
            bottomNavigation.setDefaultItem(0);
        }
        bottomNavigation.setOnSelectedItemChangeListener(new OnSelectedItemChangeListener() {
            @Override
            public void onSelectedItemChanged(int itemId) {
                switch (itemId){
                    case R.id.tab_home:
                        isClick         = false;
                        strStateView    = Constants.VIEW_HOME;
                        transaction     = getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.replace(R.id.frame_fragment_containers, new FragmentHome());
                        transaction.commit();
                        break;
                    case R.id.tab_pmb:
                        isClick         = false;
                        strStateView    = Constants.VIEW_PMB;
                        transaction     = getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.replace(R.id.frame_fragment_containers, new FragmentPendaftaran());
                        transaction.commit();
                        break;
                    case R.id.tab_info:
                        isClick         = false;
                        strStateView    = Constants.VIEW_INFO;
                        transaction     = getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.replace(R.id.frame_fragment_containers, new FragmentInformasi());
                        transaction.commit();
                        break;
                    case R.id.tab_bayar:
                        isClick         = false;
                        strStateView    = Constants.VIEW_BAYAR;
                        transaction     = getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.replace(R.id.frame_fragment_containers, new FragmentBayar());
                        transaction.commit();
                        break;
                    case R.id.tab_account:
                        isClick         = false;
                        strStateView    = Constants.VIEW_ACCOUNT;
                        transaction     = getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.replace(R.id.frame_fragment_containers, new FragmentAccount());
                        transaction.commit();
                        break;
                }
            }
        });
    }
    public static void onSetBottomNav(final Activity activity){
        Drawable homeDrawable       = activity.getResources().getDrawable(R.drawable.ic_home_default);
        Drawable homeSelected       = activity.getResources().getDrawable(R.drawable.ic_home_selected);
        Drawable pmbDrawable        = activity.getResources().getDrawable(R.drawable.ic_script_default);
        Drawable pmbSelected        = activity.getResources().getDrawable(R.drawable.ic_script_selected);
        Drawable infoDrawable       = activity.getResources().getDrawable(R.drawable.ic_papers_default);
        Drawable infoSelected       = activity.getResources().getDrawable(R.drawable.ic_papers_selected);
        Drawable payDrawable        = activity.getResources().getDrawable(R.drawable.ic_paypal_default);
        Drawable paySelected        = activity.getResources().getDrawable(R.drawable.ic_paypal_selected);
        Drawable profileDrawable    = activity.getResources().getDrawable(R.drawable.ic_user_default);
        Drawable profileSelected    = activity.getResources().getDrawable(R.drawable.ic_user_selected);

        tabHome.setIconImageView(homeDrawable, homeSelected);
        tabPmb.setIconImageView(pmbDrawable, pmbSelected);
        tabInfo.setIconImageView(infoDrawable, infoSelected);
        tabBayar.setIconImageView(payDrawable, paySelected);
        tabAccount.setIconImageView(profileDrawable, profileSelected);
        tabHome.setColortextView(activity.getResources().getColor(R.color.colorUnselect), activity.getResources().getColor(R.color.colorAccent));
        tabPmb.setColortextView(activity.getResources().getColor(R.color.colorUnselect), activity.getResources().getColor(R.color.colorAccent));
        tabInfo.setColortextView(activity.getResources().getColor(R.color.colorUnselect), activity.getResources().getColor(R.color.colorAccent));
        tabBayar.setColortextView(activity.getResources().getColor(R.color.colorUnselect), activity.getResources().getColor(R.color.colorAccent));
        tabAccount.setColortextView(activity.getResources().getColor(R.color.colorUnselect), activity.getResources().getColor(R.color.colorAccent));
    }
    public Runnable CloseApps = new Runnable() {
        @Override
        public void run() {
            if (isClick){
                isClick         = false;
                Intent intent   = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            } else {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_close_apps), Toast.LENGTH_LONG).show();
                isClick = true;
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                Intent intentlogin = new Intent(MainActivity.this, SignInActivity.class);
                intentlogin.putExtra(Constants.VIEW_KEY, Constants.VIEW_ACCOUNT);
                startActivity(intentlogin);
                break;
            case R.id.btnProfile:
                Intent intentProfile = new Intent(MainActivity.this, ProfileDetailActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.btnChangePassword:
                startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));
                break;
            case R.id.btnNotifications:
                Intent intentNotif = new Intent(MainActivity.this, NotifikasiActivity.class);
                startActivity(intentNotif);
                break;
            case R.id.btnRate:
                gotoPlayStore.run();
                break;
            case R.id.btnAbout:
                Intent intentAbout = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intentAbout);
                break;
            case R.id.btnSignOut:
                Dialog.ConfirmationDialog(MainActivity.this, getResources().getString(R.string.label_logout),
                        getResources().getString(R.string.label_msg_sign_out), logoutFunction, null);
                break;
            default:
                break;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = "";
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +getPackageName()+ "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                String[] onlineVersi    = onlineVersion.split("\\.");
                try {
                    PackageInfo pInfo       = GlobalApplication.getContext().getPackageManager().getPackageInfo(getPackageName(), 0);
                    String localVersion     = pInfo.versionName;
                    String[] localVersi     = localVersion.split("\\.");
                    for (int i=0; i < localVersi.length; i++){
                        if (Integer.valueOf(localVersi[i]) < Integer.valueOf(onlineVersi[i]) ) {
                            onShowUpdateVersion();
                            break;
                        }
                    }
                } catch (PackageManager.NameNotFoundException ignored) {}
            }
        }
    }
    private void onShowUpdateVersion(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.msg_new_version));
        dialogBuilder.setMessage(getResources().getString(R.string.msg_need_update));
        dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_now), (dialog, which) -> {
            gotoPlayStore.run();
            dialog.dismiss();
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.btn_later), (dialog, which) -> dialog.dismiss());
        AlertDialog dialogView = dialogBuilder.create();
        dialogView.setCanceledOnTouchOutside(false);
        dialogView.show();
    }

    public Runnable gotoPlayStore = new Runnable() {
        @Override
        public void run() {
            Intent downloadIntent = new Intent(Intent.ACTION_VIEW);
            downloadIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id="+ getPackageName()));
            startActivity(downloadIntent);
        }
    };
    private Runnable logoutFunction = new Runnable() {
        @Override
        public void run() {
            UserTask.postRemoveToken(null, null);
            Utility.onUserLogout(MainActivity.this);
        }
    };



    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}