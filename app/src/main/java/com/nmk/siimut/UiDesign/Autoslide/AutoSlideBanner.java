package com.nmk.siimut.UiDesign.Autoslide;

import android.app.Activity;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.nmk.siimut.MdlBanner.MstBanner;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

public class AutoSlideBanner {
    public static void onSetSlideBanner(Activity mAct, AutoScrollViewPager viewPager, List<MstBanner> sliderList, String StateView){
        double ImgWidth     = Utility.getScreenWidth();
        double ImgHeight    = (Constants.HEIGHT_BANNER * ImgWidth) / Constants.WIDTH_BANNER ;

        RelativeLayout.LayoutParams layoutParams    = new RelativeLayout.LayoutParams((int) ImgWidth, (int) ImgHeight);
        viewPager.setLayoutParams(layoutParams);
        viewPager.startAutoScroll();
        viewPager.setInterval(Constants.ADS_INTERVAL);
        viewPager.setCycle(true);
        viewPager.setStopScrollWhenTouch(true);
        viewPager.setAutoScrollDurationFactor(5);
        if (sliderList.size() > 0){
            PagerAdapter adapter = new CustomAdapter(mAct, sliderList, StateView, (int) ImgWidth, (int) ImgHeight);
            viewPager.setAdapter(adapter);
        }
    }
    public static void onSetSlideIndicator(final Activity mAct, final AutoScrollViewPager viewPager, final List<MstBanner> sliderList, final LinearLayout slideIndicator){
        if (sliderList.size() > 0){
            int currentPos          = 0;
            final int slideCount    = sliderList.size();
            if (slideIndicator.getChildCount() != slideCount){
                slideIndicator.removeViews(0, slideIndicator.getChildCount());
            }
            int padding = (int) (0.005 * Utility.getScreenWidth());
            for (int i = 0; i < slideCount; i++){
                ImageButton imgDot = new ImageButton(mAct);
                imgDot.setTag(i);
                imgDot.setImageResource(R.drawable.dot_selector);
                imgDot.setBackgroundResource(0);
                imgDot.setPadding(padding, padding, padding, padding);

                int slideIndicatorSize              = (int) (0.025 * Utility.getScreenWidth());
                LinearLayout.LayoutParams params    = new LinearLayout.LayoutParams( slideIndicatorSize, slideIndicatorSize );
                imgDot.setLayoutParams(params);
                if (slideIndicator.getChildCount() < slideCount){
                    slideIndicator.addView(imgDot);
                }
                if(i == currentPos){
                    imgDot.setSelected(true);
                    (slideIndicator.findViewWithTag(i)).setSelected(true);
                }else{
                    (slideIndicator.findViewWithTag(i)).setSelected(false);
                }
            }
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                int currentPos = 0;
                @Override
                public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
                }
                @Override
                public void onPageSelected(int pos) {

                    int position    = pos%slideCount;

                    for (int i = 0; i < slideCount; i++) {
                        if (i != position) {
                            if (currentPos != position) {
                                (slideIndicator.findViewWithTag(i)).setSelected(false);
                            }
                        } else
                            (slideIndicator.findViewWithTag(i)).setSelected(true);
                    }
                    currentPos = position;
                }
                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
    }
}
