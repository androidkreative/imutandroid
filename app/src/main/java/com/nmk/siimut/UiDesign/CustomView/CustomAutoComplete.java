package com.nmk.siimut.UiDesign.CustomView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import com.nmk.siimut.Utilities.Utility;

/**
 * Created by Warsono on 10/03/2019.
 */

public class CustomAutoComplete extends AppCompatAutoCompleteTextView {


    public CustomAutoComplete(Context context) {
        super(context);
        Typeface font = Utility.getFontType(context);
        this.setTypeface(font);
    }
    public CustomAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface font = Utility.getFontType(context);
        this.setTypeface(font);
    }
    public CustomAutoComplete(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface font = Utility.getFontType(context);
        this.setTypeface(font);
    }
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}