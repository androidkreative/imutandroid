package com.nmk.siimut.UiDesign.CustomView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.nmk.siimut.Utilities.Utility;

/**
 * Created by Warsono on 10/03/2019.
 */

public class CustomTextView extends AppCompatTextView {


    public CustomTextView(Context context) {
        super(context);
        Typeface font = Utility.getFontType(context);
        this.setTypeface(font);
    }
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface font = Utility.getFontType(context);
        this.setTypeface(font);
    }
    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface font = Utility.getFontType(context);
        this.setTypeface(font);
    }
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}