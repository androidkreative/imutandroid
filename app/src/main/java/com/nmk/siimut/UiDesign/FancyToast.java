package com.nmk.siimut.UiDesign;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nmk.siimut.R;


/**
 * Created by Warsono on 01/07/2019.
 */

public class FancyToast extends Toast {
    public static final int DEFAULT   = 0;
    public static final int INFO      = 1;
    public static final int SUCCESS   = 2;
    public static final int WARNING   = 3;
    public static final int ERROR     = 4;

    public FancyToast(Context context) {
        super(context);
    }

    public static Toast makeMessage(Context context, final int type, String strMessage, int duration) {
        Toast viewToast             = new Toast(context);
        View layout                 = LayoutInflater.from(context).inflate(R.layout.view_fancy_toast, (ViewGroup)null, false);
        TextView msgView            = layout.findViewById(R.id.toast_text);
        LinearLayout toastLayout    = layout.findViewById(R.id.toast_type);
        msgView.setText(strMessage);

        switch (type) {
            case DEFAULT:
                toastLayout.setBackgroundResource(R.drawable.bg_default);
                break;
            case INFO:
                toastLayout.setBackgroundResource(R.drawable.bg_info);
                break;
            case SUCCESS:
                toastLayout.setBackgroundResource(R.drawable.bg_success);
                break;
            case WARNING:
                toastLayout.setBackgroundResource(R.drawable.bg_warning);
                break;
            case ERROR:
                toastLayout.setBackgroundResource(R.drawable.bg_error);
                break;
            default:
                toastLayout.setBackgroundResource(R.drawable.bg_default);
                break;
        }
        viewToast.setDuration(duration);
        viewToast.setView(layout);
        viewToast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);

        return viewToast;
    }
}