package com.nmk.siimut.UiDesign.BottomNav.utils;

/**
 * @author S.Shahini
 * @since 11/14/16
 * this class hold dynamic bottom navigation dimens
 */

public class DynamicDimens extends Dimens {
    //Units are based on dp
    public static final int TAB_PADDING_RIGHT           = 6;
    public static final int TAB_PADDING_LEFT            = 6;
    public static final int TAB_PADDING_TOP_ACTIVE      = 6;
    public static final int TAB_PADDING_TOP_INACTIVE    = 12;
    public static final int TAB_PADDING_BOTTOM_ACTIVE   = 0;
}
