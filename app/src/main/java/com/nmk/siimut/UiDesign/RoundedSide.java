package com.nmk.siimut.UiDesign;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;

public class RoundedSide implements com.squareup.picasso.Transformation {
    private final int radius;
    private final int margin;
    private final int side;
    public static final int LEFT    = 1;
    public static final int RIGHT   = 2;
    public static final int TOP     = 3;
    public static final int BOTTOM  = 4;
    public static final int ALL     = 5;

    public RoundedSide(final int radius, final int margin, int side) {
        this.radius = radius;
        this.margin = margin;
        this.side   = side;
    }

    @Override
    public Bitmap transform(final Bitmap source) {
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        switch(side) {
            case LEFT:
                output = sideRoundBitmap(source, radius, 0, 0, radius);
                break;
            case RIGHT:
                output = sideRoundBitmap(source, 0, radius, radius, 0);
                break;
            case TOP:
                output = sideRoundBitmap(source, radius, radius, 0, 0);
                break;
            case BOTTOM:
                output = sideRoundBitmap(source, 0, 0, radius, radius);
                break;
            case ALL:
                output = sideRoundBitmap(source, radius, radius, radius, radius);
                break;
        }
        if (source != output) {
            source.recycle();
        }
        return output;
    }
    private static Bitmap sideRoundBitmap(Bitmap bitmap, float topLeftCorner, float topRightCorner,
                                                  float bottomRightCorner, float bottomLeftCorner) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color     = Color.WHITE;
        final Paint paint   = new Paint();
        final Rect rect     = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF   = new RectF(rect);
        Path path           = new Path();
        float[] radii       = new float[]{
                topLeftCorner, topLeftCorner,
                topRightCorner, topRightCorner,
                bottomRightCorner, bottomRightCorner,
                bottomLeftCorner, bottomLeftCorner
        };

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        path.addRoundRect(rectF, radii, Path.Direction.CW);
        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
    @Override
    public String key() {
        return "rounded(r=" + radius + ", m=" + margin + ")";
    }
}
