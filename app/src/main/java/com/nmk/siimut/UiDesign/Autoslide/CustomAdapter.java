package com.nmk.siimut.UiDesign.Autoslide;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.nmk.siimut.MdlBanner.MstBanner;
import com.nmk.siimut.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomAdapter extends PagerAdapter {

    private Activity activity;
    private List<MstBanner> slideArray;
    private String StateView;
    int ImgWidth, ImgHeight;

    public CustomAdapter(Activity activity, List<MstBanner> sliderList, String View, int imgWidth, int imgHeight){
        this.activity       = activity;
        this.slideArray     = sliderList;
        this.StateView      = View;
        ImgWidth            = (int) (0.95 * imgWidth);
        ImgHeight           = (int) (0.95 * imgHeight);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int index               = position%slideArray.size();

        LayoutInflater inflater     = activity.getLayoutInflater();
        View viewItem               = inflater.inflate(R.layout.view_banner_image, container, false);
        final ImageView imageView   = viewItem.findViewById(R.id.imageView);
        imageView.setBackgroundColor(Color.TRANSPARENT);
        final MstBanner objSlider   = slideArray.get(index);
        Picasso.get()
                .load(Uri.parse(objSlider.getBannerUrl()))
                .resize(ImgWidth, ImgHeight)
                .centerCrop()
                .transform(new com.nmk.siimut.UiDesign.RoundedTransformation(16, 0))
                .into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { }
        });
        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == ((View)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}
