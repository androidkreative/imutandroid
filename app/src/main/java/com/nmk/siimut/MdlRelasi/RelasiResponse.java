package com.nmk.siimut.MdlRelasi;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class RelasiResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstRelasi> DataList    = new ArrayList<MstRelasi>();

    public List<MstRelasi> getDataList() {
        return DataList;
    }
}
