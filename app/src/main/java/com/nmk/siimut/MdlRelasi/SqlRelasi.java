package com.nmk.siimut.MdlRelasi;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlRelasi {
    private static String TABLE_RELASI          = "MST_RELASI";
    private static String FIELD_RELASI_ID       = "RELASI_ID";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_RELASI + "("
                + FIELD_RELASI_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);
    }
    public static void addDataRelasi(MstRelasi Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_RELASI_ID, Data.getId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        db.insert(TABLE_RELASI, null, values);
    }
    public static List<MstRelasi> getAllRelasi() {
        List<MstRelasi> DataList   = new ArrayList<MstRelasi>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_RELASI;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstRelasi Data = new MstRelasi();
                Data.setId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteRelasi() {
        if (getAllRelasi().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_RELASI);
        }
    }
}