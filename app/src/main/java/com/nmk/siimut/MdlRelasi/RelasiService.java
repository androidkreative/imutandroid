package com.nmk.siimut.MdlRelasi;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface RelasiService {

    @GET(Constants.API_GET_KERJASAMA)
    Call<RelasiResponse> getDataList(@Query("id") String UserID);
}
