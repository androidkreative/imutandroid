package com.nmk.siimut.MdlRelasi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstRelasi {
    @SerializedName("ID")
    String Id;

    @SerializedName("DESCRIPTION")
    String Description;

    public MstRelasi() {
    }

    public MstRelasi(String id, String description) {
        Id          = id;
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}