package com.nmk.siimut.Utilities;

/**
 * Created by Warsono on 01/07/2019.
 */

public class Constants {

    public static final long CONNECTION_TIMEOUT = 60000;
    public static final long READ_TIMEOUT       = 60000;
    public static final long WRITE_TIMEOUT      = 60000;
    public static final long SPLASH_TIME        = 1000;
    public static final long SHIMMER_LOADING    = 500;
    public static final int WIDTH_BANNER        = 16;
    public static final int HEIGHT_BANNER       = 7;
    public static final int ADS_INTERVAL        = 3000;
    public static final String VIEW_KEY         = "VIEW_KEY";
    public static final String VIEW_HOME        = "VIEW_HOME";
    public static final String VIEW_ACCOUNT     = "VIEW_ACCOUNT";
    public static final String VIEW_PMB         = "VIEW_PMB";
    public static final String VIEW_INFO        = "VIEW_INFO";
    public static final String VIEW_BAYAR       = "VIEW_BAYAR";
    public static final String VIEW_NOTIF       = "VIEW_NOTIF";
    public static final String PARAMS_KEY       = "PARAMS_KEY";
    public static final String INFO_ID          = "INFO_ID";
    public static final String NOTIF_ID         = "NOTIF_ID";
    public static final String IMAGE_KEY        = "IMAGE_KEY";
    public static final String CALON_KEY        = "1";
    public static final String MAHASISWA_KEY    = "2";
    public static final int MIN_LIST_LOADING    = 10;
    public static final String VERSION_DEFAULT  = "v1.0.0";

    public static final String SOSMED_DEFAULT   = "O";
    public static final String SOSMED_FACEBOOK  = "F";
    public static final String SOSMED_GOOGLE    = "G";
    public static final String FIRST_OPEN       = "FIRST_OPEN";

    public static final String BANNER_1         = "1";
    public static final String BANNER_2         = "2";

    //HASH ID
    public static final String HASH_PENGGUNA    = "1";
    public static final String HASH_CUSTOMER    = "2";
    public static final String HASH_MACHINE     = "3";
    public static final String HASH_MATERIAL    = "4";
    public static final String HASH_ORDER       = "5";

    public static final String SORT_DATE_ASC    = "1";
    public static final String SORT_DATE_DESC   = "2";
    public static final String SORT_WO_ASC      = "3";
    public static final String SORT_WO_DESC     = "4";
    public static final String SORT_PO_ASC      = "5";
    public static final String SORT_PO_DESC     = "6";

    public static final String FILTER_MESIN     = "1";
    public static final String FILTER_CUSTOMER  = "2";

    public static final String API_HEADER       = "NMK/Android/2019";
    public static final String APP_KEY          = "3f30a837bb81372eced861509d55ca5d";
    public static final String API_SERVER       = "http://info-umt.com/";              //LIVE ONLINE
//    public static final String API_SERVER       = "http://192.168.1.4/";                    //LOKAL HOME
//    public static final String API_SERVER       = "http://192.168.12.18/";                  //LOKAL OFFICE

    private static final String API_MAIN            = "/p2k/api/";
    public static final String API_SIGN_IN          = API_MAIN+"user/sign_in";
    public static final String API_SIGN_UP          = API_MAIN+"user/sign_up";
    public static final String API_CHANGE_PASSWORD  = API_MAIN+"user/change_password";
    public static final String API_USER_ACCOUNT     = API_MAIN+"user/get_account";
    public static final String API_GET_NOTIFIKASI   = API_MAIN+"user/get_notifikasi";
    public static final String API_GET_NOTIF_DETAIL = API_MAIN+"user/get_notifikasi_detail";
    public static final String API_USER_PROFILE     = API_MAIN+"user/get_profile";
    public static final String API_UPDATE_PROFILE   = API_MAIN+"user/update_profile";
    public static final String API_UPLOAD_PROFILE   = API_MAIN+"user/upload_profile";
    public static final String API_INPUT_TOKEN      = API_MAIN+"user/input_token";
    public static final String API_REMOVE_TOKEN     = API_MAIN+"user/remove_token";

    public static final String API_GET_BANNER       = API_MAIN+"data/get_banner";
    public static final String API_GET_PROFILE      = API_MAIN+"data/get_profile";
    public static final String API_GET_ADDRESS      = API_MAIN+"data/get_address";
    public static final String API_GET_STUDI        = API_MAIN+"data/get_studi";
    public static final String API_GET_SARANA       = API_MAIN+"data/get_sarana";
    public static final String API_GET_KERJASAMA    = API_MAIN+"data/get_kerjasama";
    public static final String API_GET_SYARAT       = API_MAIN+"data/get_syarat_daftar";
    public static final String API_GET_JADWAL       = API_MAIN+"data/get_jadwal";
    public static final String API_GET_BIAYA        = API_MAIN+"data/get_biaya";
    public static final String API_GET_AGAMA        = API_MAIN+"data/get_agama";
    public static final String API_GET_STATUS       = API_MAIN+"data/get_status";
    public static final String API_GET_MARKET       = API_MAIN+"data/get_market";
    public static final String API_GET_AJARAN       = API_MAIN+"data/get_ajaran";
    public static final String API_GET_INFORMASI    = API_MAIN+"data/get_informasi";
    public static final String API_POST_INFORMASI   = API_MAIN+"data/post_informasi";
    public static final String API_GET_INFO_DETAIL  = API_MAIN+"data/get_informasi_detail";


    public static final String API_REGISTER         = API_MAIN+"mahasiswa/register";
    public static final String API_GET_PAYMENT      = API_MAIN+"mahasiswa/get_payment";
    public static final String API_GET_SPB          = API_MAIN+"mahasiswa/get_spb";
    public static final String API_GET_SPP          = API_MAIN+"mahasiswa/get_spp";
}
