package com.nmk.siimut.Utilities;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.nmk.siimut.R;


/**
 * Created by Warsono on 10/03/2019.
 */

public class Dialog {


    public static void InformationDialog(Activity mContext, String Title, String Message, String BtnText, final Runnable PositiveAction){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle(Title);
        dialogBuilder.setMessage(Message);
        dialogBuilder.setPositiveButton(BtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (PositiveAction != null)
                    PositiveAction.run();
                dialog.dismiss();
            }
        });
        AlertDialog dialogView = dialogBuilder.create();
        dialogView.setCanceledOnTouchOutside(false);
        dialogView.show();
    }

    public static void ConfirmationDialog(Activity mContext, String Title, String Message, final Runnable PositiveAction, final Runnable NegativeAction){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle(Title);
        dialogBuilder.setMessage(Message);
        dialogBuilder.setPositiveButton(mContext.getResources().getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (PositiveAction != null)
                    PositiveAction.run();
                dialog.dismiss();
            }
        });
        dialogBuilder.setNegativeButton(mContext.getResources().getString(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (NegativeAction != null)
                    NegativeAction.run();
                dialog.dismiss();
            }
        });
        AlertDialog dialogView = dialogBuilder.create();
        dialogView.setCanceledOnTouchOutside(false);
        dialogView.show();
    }

}
