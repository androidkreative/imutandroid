package com.nmk.siimut.Utilities;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlAddress.SqlAddress;
import com.nmk.siimut.MdlBanner.SqlBanner;
import com.nmk.siimut.MdlBiaya.SqlBiaya;
import com.nmk.siimut.MdlFakultas.SqlFakultas;
import com.nmk.siimut.MdlFasilitas.SqlFasilitas;
import com.nmk.siimut.MdlHome.SqlProfile;
import com.nmk.siimut.MdlRelasi.SqlRelasi;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.MdlPendaftaran.SqlDaftar;
import com.nmk.siimut.MdlPendaftaran.SqlRegistration;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION   = 1;
    private static final String DATABASE_NAME   = "si_imut";

    public DatabaseHandler() {
        super(GlobalApplication.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        SqlUser.onCreateTable(db);
        SqlBanner.onCreateTable(db);
        SqlProfile.onCreateTable(db);
        SqlAddress.onCreateTable(db);
        SqlFakultas.onCreateTable(db);
        SqlFasilitas.onCreateTable(db);
        SqlRelasi.onCreateTable(db);
        SqlBiaya.onCreateTable(db);
        SqlOther.onCreateTable(db);
        SqlDaftar.onCreateTable(db);
        SqlRegistration.onCreateTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch(oldVersion) {
            case 1:
                break;
            default:
                break;
        }
    }
}
