package com.nmk.siimut.Utilities;

import android.widget.ProgressBar;

import com.squareup.picasso.Callback;

/**
 * Created by Warsono on 10/03/2019.
 */

public class ImageLoadedCallback implements Callback {
    public ProgressBar progressBar;

    public ImageLoadedCallback(ProgressBar progBar){
        progressBar = progBar;
    }

    @Override
    public void onSuccess() {
    }

    @Override
    public void onError(Exception e) {
    }
}
