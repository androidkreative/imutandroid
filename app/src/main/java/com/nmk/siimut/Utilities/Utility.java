package com.nmk.siimut.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.MdlPendaftaran.SqlDaftar;
import com.nmk.siimut.MdlPendaftaran.SqlRegistration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Warsono on 10/03/2019.
 */

public class Utility {
    private static final String MONTH_YEAR_DISPLAY_PATTERN = "MMM yyyy";

    public static boolean isNetworkConnected() {
        //HARUS DICHECK LAGI
        ConnectivityManager connectivityManager = (ConnectivityManager) GlobalApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length;i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isTabletDevice(Context activityContext) {
        boolean device_large    = ((activityContext.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) ||
                ((activityContext.getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE);

        if (device_large) {
            DisplayMetrics metrics  = new DisplayMetrics();
            Activity activity       = (Activity) activityContext;
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            if (metrics.densityDpi == DisplayMetrics.DENSITY_DEFAULT
                    || metrics.densityDpi == DisplayMetrics.DENSITY_HIGH
                    || metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM
                    || metrics.densityDpi == DisplayMetrics.DENSITY_TV
                    || metrics.densityDpi == DisplayMetrics.DENSITY_XHIGH) {
                return true;
            }
        }
        return false;
    }
    public static boolean isEmail (String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static int getScreenWidth() {
        DisplayMetrics metrics  = new DisplayMetrics();
        WindowManager wm        = (WindowManager) GlobalApplication.getContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        int mScreenWidth        = metrics.widthPixels;;
        return mScreenWidth;
    }
    public static int getScreenHeight() {
        DisplayMetrics metrics  = new DisplayMetrics();
        WindowManager wm        = (WindowManager) GlobalApplication.getContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        int mScreenHeight       = metrics.heightPixels;;
        return mScreenHeight;
    }
    public static Typeface getFontType(Context context) {
        String strFontFileName          = "Lato_Regular.ttf";
        return Typeface.createFromAsset(context.getAssets(), "fonts/"+strFontFileName);
    }
    public static String currencyFormat(double d) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("###,###");
        return formatter.format(d);
    }
    public static void onUserLogout(Context mContext){
        List<MstUser> dataUser  = SqlUser.getAllData();
        if (dataUser.size() > 0) {
            SqlUser.DeleteData();
            SqlUser.DeleteUserProfile();
            SqlDaftar.UpdateMarketUnChecked();
            SqlRegistration.DeleteRegistration();
        }
        deleteCache(mContext);
        Intent intentLogout = new Intent(mContext, MainActivity.class);
        intentLogout.putExtra(Constants.VIEW_KEY, Constants.VIEW_HOME);
        mContext.startActivity(intentLogout);
    }
    private static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception ignored) {}
    }
    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
    public static String DateView(String inputdate) {
        Date date                       = new Date();
        SimpleDateFormat dateInput      = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateOutput     = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = dateInput.parse(inputdate);
        } catch (ParseException e) {
            return inputdate;
        }
        return dateOutput.format(date);
    }
    public static String simpleDateView(String inputdate) {
        Date date                       = new Date();
        SimpleDateFormat dateInput      = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateOutput     = new SimpleDateFormat("dd/MM/yy");
        try {
            date = dateInput.parse(inputdate);
        } catch (ParseException e) {
            return inputdate;
        }
        return dateOutput.format(date);
    }
    public static String ItemDateView(String inputdate) {
        Date date                       = new Date();
        SimpleDateFormat dateInput      = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        SimpleDateFormat dateOutput     = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        try {
            date = dateInput.parse(inputdate);
        } catch (ParseException e) {
            return inputdate;
        }
        return dateOutput.format(date);
    }
    public static File createFileFromBitmap(byte[] bitmapdata, String FileName){
        File mFileResult = new File(GlobalApplication.getContext().getCacheDir(), FileName);
        try {
            mFileResult.createNewFile();
            FileOutputStream fos 	= null;
            try {
                fos = new FileOutputStream(mFileResult);
            } catch (FileNotFoundException e) {}
            try {
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (IOException e) {}
        } catch (IOException e) {}
        return mFileResult;
    }
    public static boolean checkPermission(Activity activity, String  strPermission) {
        int result     = ContextCompat.checkSelfPermission(activity, strPermission);
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;
        else
            return false;
    }
    public static void requestPermission(Activity activity, String  strPermission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{strPermission}, requestCode);
    }
}
