package com.nmk.siimut.Mdlinformasi;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.nmk.siimut.MdlAccount.User.SignInActivity;
import com.nmk.siimut.MdlAccount.User.UserTask;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;


/**
 * Created by Warsono on 01/07/2019.
 */

public class InformasiAddActivity extends AppCompatActivity implements IPickResult, View.OnClickListener{

    private EditText inputTitle, inputDescription;
    private TextView inputAttach, buttonSave;
    private RelativeLayout loadingMain;
    private String strTitle;
    private String strDescription;
    private Bitmap uploadBitmap = null;

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(InformasiAddActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_INFO);
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_add);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        ImageView buttonBack        = findViewById(R.id.btnBack);
        buttonBack.setOnClickListener(this);
        buttonSave          = findViewById(R.id.btnSubmit);
        inputTitle          = findViewById(R.id.titleInput);
        inputDescription    = findViewById(R.id.descriptionInput);
        inputAttach         = findViewById(R.id.attachInput);
        loadingMain         = findViewById(R.id.mainLoading);
    }
    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.attachInput:
                PickImageDialog.build(new PickSetup()
                        .setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL)
                ).show(InformasiAddActivity.this);
                break;
            case R.id.btnSubmit:
                onClickSubmit();
                break;
        }
    }
    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            uploadBitmap = pickResult.getBitmap();
            inputAttach.setText(pickResult.getPath());
        }else
            Toast.makeText(InformasiAddActivity.this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
    }
    public boolean validateData() {
        boolean valid       = true;
        strTitle            = inputTitle.getText().toString();
        strDescription      = inputDescription.getText().toString();

        if (strTitle.isEmpty() || strTitle.length() == 0 ) {
            inputTitle.setError("Judul tidak boleh kosong");
            valid = false;
        } else {
            inputTitle.setError(null);
            if (strDescription.isEmpty() || strDescription.length() == 0 ) {
                inputDescription.setError("Penjelasan tidak boleh kosong");
                valid = false;
            } else
                inputDescription.setError(null);
        }
        return valid;
    }
    private void onClickSubmit(){
        if (validateData()){
            if (Utility.isNetworkConnected()) {
                onShowLoading.run();
                if (uploadBitmap != null){
                    ByteArrayOutputStream bos   = new ByteArrayOutputStream();
                    uploadBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    byte[] mBitmapData      = bos.toByteArray();
                    InformasiTask.onSaveInformation(InformasiAddActivity.this, strTitle, strDescription, mBitmapData, onSaveSuccess, onHideLoading);
                }else
                    InformasiTask.onSaveInformation(InformasiAddActivity.this, strTitle, strDescription, null, onSaveSuccess, onHideLoading);
            }else
                Dialog.InformationDialog(InformasiAddActivity.this,
                        getResources().getString(R.string.label_warning),
                        getResources().getString(R.string.msg_conection_unavailable),
                        getResources().getString(R.string.btn_close),
                        null);
        }
    }
    private Runnable onSaveSuccess = () -> {
        uploadBitmap.recycle();
        uploadBitmap = null;
        onBackPressed();
    };
    private Runnable onShowLoading = new Runnable() {
        @Override
        public void run() {
            inputTitle.setEnabled(false);
            inputDescription.setEnabled(false);
            buttonSave.setEnabled(false);
            loadingMain.setVisibility(View.VISIBLE);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
            inputTitle.setEnabled(true);
            inputDescription.setEnabled(true);
            buttonSave.setEnabled(true);
            loadingMain.setVisibility(View.GONE);
        }
    };

    


    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}