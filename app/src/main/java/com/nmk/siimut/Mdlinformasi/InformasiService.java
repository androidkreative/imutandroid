package com.nmk.siimut.Mdlinformasi;

import com.nmk.siimut.ApiService.BasicResponse;
import com.nmk.siimut.Utilities.Constants;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface InformasiService {

    @GET(Constants.API_GET_INFORMASI)
    Call<InformasiListResponse> getInformasi(
            @Query("limit") String Limit,
            @Query("page") String Page);

    @GET(Constants.API_GET_INFO_DETAIL)
    Call<InformasiResponse> getInformasiDetail(
            @Query("info_id") String InfoId);

//    @FormUrlEncoded
//    @POST(Constants.API_POST_INFORMASI)
//    Call<BasicResponse> postInformasi(
//            @Field("id") String UserId,
//            @Field("auth") String UserAuth,
//            @Field("title") String Title,
//            @Field("description") String Description);

    @Multipart
    @POST(Constants.API_POST_INFORMASI)
    Call<BasicResponse> postInformasi(
            @PartMap Map<String, RequestBody> params);
}
