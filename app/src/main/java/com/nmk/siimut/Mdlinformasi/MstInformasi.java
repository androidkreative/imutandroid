package com.nmk.siimut.Mdlinformasi;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstInformasi implements Serializable {
    @SerializedName("ID")
    String InfoId;

    @SerializedName("IMAGE")
    String Image;

    @SerializedName("DATE")
    String Date;

    @SerializedName("TITLE")
    String Title;

    @SerializedName("SORT_DESC")
    String SortDesc;

    @SerializedName("DESCRIPTION")
    String Description;

    public MstInformasi(String infoId, String image, String date, String title, String sortDesc, String description) {
        InfoId = infoId;
        Image = image;
        Date = date;
        Title = title;
        SortDesc = sortDesc;
        Description = description;
    }

    public String getInfoId() {
        return InfoId;
    }

    public void setInfoId(String infoId) {
        InfoId = infoId;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSortDesc() {
        return SortDesc;
    }

    public void setSortDesc(String sortDesc) {
        SortDesc = sortDesc;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}