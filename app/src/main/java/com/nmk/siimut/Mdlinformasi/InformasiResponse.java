package com.nmk.siimut.Mdlinformasi;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class InformasiResponse extends BasicResponse {

    @SerializedName("DATA")
    MstInformasi Data;

    public MstInformasi getData() {
        return Data;
    }
}
