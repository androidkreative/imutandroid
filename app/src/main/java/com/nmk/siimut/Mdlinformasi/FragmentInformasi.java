package com.nmk.siimut.Mdlinformasi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.jpardogo.android.googleprogressbar.library.GoogleProgressBar;
import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.FancyToast;
import com.nmk.siimut.UiDesign.RoundedSide;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.TimeFunction;
import com.nmk.siimut.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class FragmentInformasi extends Fragment {

    private List<MstInformasi> informasiList  = new ArrayList<MstInformasi>();
    private ViewGroup RootView;
    private RecyclerView viewinformasi;
    private LinearLayout viewLayoutEmpty;
    private LinearLayout loadingInfo;
    private ShimmerLayout shimmerLayout;
    private LinearLayoutManager layoutManager;
    private informasiAdapter informasiAdapter;
    private boolean isLoading   = false;
    private int page            = 0;
    private ImageView viewImageEmpty;
    private TextView viewTitleEmpty, viewDescEmpty, buttonAdd;
    private SwipeRefreshLayout refreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RootView        = (ViewGroup) inflater.inflate(R.layout.fragment_informasi, null);
        viewLayoutEmpty = RootView.findViewById(R.id.viewEmpty);
        viewImageEmpty  = RootView.findViewById(R.id.viewEmptyImage);
        viewTitleEmpty  = RootView.findViewById(R.id.viewEmptyTitle);
        viewDescEmpty   = RootView.findViewById(R.id.viewEmptyDescription);
        viewinformasi   = RootView.findViewById(R.id.viewInformation);
        buttonAdd       = RootView.findViewById(R.id.btnAdd);
        buttonAdd.setVisibility(View.GONE);
        loadingInfo     = RootView.findViewById(R.id.loadingInformation);
        layoutManager   = new LinearLayoutManager(getActivity());
        viewinformasi.setLayoutManager(layoutManager);
        viewinformasi.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int lastvisibleitemposition = layoutManager.findLastVisibleItemPosition();
                if (lastvisibleitemposition == informasiAdapter.getItemCount() - 1) {
                    if (!isLoading) {
                        isLoading = true;
                        new GetMoreDataTask().execute(0);
                    }
                }
            }
        });
        refreshLayout   = RootView.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.YELLOW);
        refreshLayout.setDistanceToTriggerSync(70);
        refreshLayout.setSize(SwipeRefreshLayout.DEFAULT);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utility.isNetworkConnected()) {
                    if (informasiList.size() > 0)
                        informasiList.clear();
                    new GetInformasiTask().execute(0);
                }else
                    FancyToast.makeMessage(getActivity(), FancyToast.WARNING, getResources().getString(R.string.msg_conection_error), Toast.LENGTH_LONG).show();
            }
        });
        onGetDataInformasi();
        onSetButtonAdd();
        return RootView;
    }
    private void onGetDataInformasi(){
        if (Utility.isNetworkConnected()) {
            onShowLoadingInfo();
            if (informasiList.size() > 0)
                informasiList.clear();
            new GetInformasiTask().execute(0);
        }else{
            viewinformasi.setVisibility(View.GONE);
            viewLayoutEmpty.setVisibility(View.VISIBLE);
            viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_requirement));
            viewTitleEmpty.setText(R.string.no_conection_title);
            viewDescEmpty.setText(R.string.no_conection_desc);
        }
    }
    private void onSetButtonAdd(){
        List<MstUser> dataList = SqlUser.getAllData();
        if (dataList.size() > 0) {
            MstUser objUser = dataList.get(0);
            if (objUser.getUserType().contentEquals("3"))
                buttonAdd.setVisibility(View.VISIBLE);
            else
                buttonAdd.setVisibility(View.GONE);
        }
        buttonAdd.setOnClickListener(v -> onClickAdd());
    }
    private void onClickAdd(){
        Intent intent = new Intent(getActivity(), InformasiAddActivity.class);
        startActivity(intent);
    }
    class GetInformasiTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            page = 0;
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                InformasiService apiService                 = ApiClient.getClient().create(InformasiService.class);
                Call<InformasiListResponse> mRequest            = apiService.getInformasi(String.valueOf(Constants.MIN_LIST_LOADING), String.valueOf(page));
                Response<InformasiListResponse> Response        = mRequest.execute();
                InformasiListResponse responseResult            = Response.body();
                if (responseResult != null){
                    switch (responseResult.getResultState()) {
                        case 200:
                            informasiList = responseResult.getDataList();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) { }
        @Override
        protected void onPostExecute(String result) {
            if (informasiList.size() > 0){
                informasiAdapter = new informasiAdapter(getActivity(), informasiList);
                viewinformasi.setAdapter(informasiAdapter);
                viewinformasi.setVisibility(View.VISIBLE);
                viewLayoutEmpty.setVisibility(View.GONE);
            }else{
                viewinformasi.setVisibility(View.GONE);
                viewLayoutEmpty.setVisibility(View.VISIBLE);
            }
            onHideloadingInfo();
        }
    }
    private void onShowLoadingInfo(){
        viewinformasi.setVisibility(View.GONE);
        viewLayoutEmpty.setVisibility(View.GONE);
        loadingInfo.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        loadingInfo.removeAllViews();
        for (int i = 0; i < 10; i++) {
            View child  = getLayoutInflater().inflate(R.layout.item_informasi_shimmer, null);
            child.setLayoutParams(params);
            shimmerLayout   = child.findViewById(R.id.shimmer_layout);
            shimmerLayout.startShimmerAnimation();
            loadingInfo.setOrientation(LinearLayout.VERTICAL);
            loadingInfo.addView(child);
        }
    }
    private void onHideloadingInfo(){
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            shimmerLayout.stopShimmerAnimation();
            loadingInfo.setVisibility(View.GONE);
        }, 300);
        refreshLayout.setRefreshing(false);
    }

    class GetMoreDataTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            page        = page + 1;
        }
        @Override
        protected String doInBackground(Integer... integers) {
            String strResult = "";
            try {
                InformasiService apiService                 = ApiClient.getClient().create(InformasiService.class);
                Call<InformasiListResponse> mRequest            = apiService.getInformasi(String.valueOf(Constants.MIN_LIST_LOADING), String.valueOf(page));
                Response<InformasiListResponse> Response        = mRequest.execute();
                InformasiListResponse responseResult            = Response.body();
                if (responseResult != null){
                    strResult       = String.valueOf(responseResult.getResultState());
                    switch (responseResult.getResultState()) {
                        case 200:
                            List<MstInformasi> responseData     = responseResult.getDataList();
                            int count                           = responseData.size();
                            if (count > 0){
                                int index = informasiList.size();
                                for (int i= 0; i < count; i++){
                                    MstInformasi objInformasi = responseData.get(i);
                                    informasiList.add( index, objInformasi);
                                    index = index+1;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) {}
            return strResult;
        }
        @Override
        protected void onProgressUpdate(Integer... values) { }
        @Override
        protected void onPostExecute(String result) {
            if (result.contentEquals("200"))
                informasiAdapter.notifyDataSetChanged();
            isLoading = false;
        }
    }

    public class informasiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int VIEW_TYPE_HEADER  = 0;
        private final int VIEW_TYPE_ITEM    = 1;
        private final int VIEW_TYPE_FOOTER  = 2;
        private List<MstInformasi> mItems;
        private Activity mContext;
        private double imageSize;
        private Date createdDate;
        private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

        public informasiAdapter(Activity context, List<MstInformasi> itemData) {
            super();
            mItems      = itemData;
            mContext    = context;
            imageSize   = 0.2 * Utility.getScreenWidth();
        }
        @Override
        public int getItemViewType(int position) {
            if (isPositionFooter(position))
                return VIEW_TYPE_FOOTER;
            else
                return VIEW_TYPE_ITEM;
        }
        private boolean isPositionHeader(int position) {
            return position == 0;
        }
        private boolean isPositionFooter(int position) {
            return position >= mItems.size();
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            if (viewType == VIEW_TYPE_FOOTER) {
                View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading, viewGroup, false);
                return new FooterViewHolder(view);
            }else{
                View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_informasi, viewGroup, false);
                return new ItemViewHolder(view);
            }
        }
        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof ItemViewHolder) {
                setItemHolder((ItemViewHolder) holder, mItems.get(position));
            }else if (holder instanceof FooterViewHolder) {
                FooterViewHolder FooterViewHolder = (FooterViewHolder) holder;
                FooterViewHolder.progressLoading.setVisibility(View.VISIBLE);
                FooterViewHolder.labelLoading.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public int getItemCount() {
            if(mItems.size() % Constants.MIN_LIST_LOADING == 0)
                return mItems.size() + 1;
            else
                return mItems.size();
        }
        private void setItemHolder(final ItemViewHolder viewHolder, final MstInformasi objInformasi){
            if (objInformasi != null && objInformasi.getInfoId() != null){
                if (objInformasi.getImage() != null && !objInformasi.getImage().contentEquals("")){
                    Picasso.get()
                            .load(Uri.parse(objInformasi.getImage()))
                            .resize((int) imageSize, (int) imageSize)
                            .centerCrop()
                            .transform(new RoundedSide(8, 0, RoundedSide.LEFT))
                            .into(viewHolder.viewImage);
                }
                viewHolder.viewTitle.setText(objInformasi.getTitle());
                viewHolder.viewContent.loadData(objInformasi.getSortDesc(), "text/html", "utf-8");
                try {
                    createdDate = dateFormat.parse(objInformasi.getDate());
                } catch (ParseException ignored) {}
                String strDate = Utility.ItemDateView(objInformasi.getDate());
                if (createdDate != null)
                    strDate = TimeFunction.getTimeAgo(createdDate);
                viewHolder.viewDate.setText(strDate);
                viewHolder.viewTitle.setOnClickListener(v -> onClickItem(objInformasi));
                viewHolder.contentLayout.setOnClickListener(v -> onClickItem(objInformasi));
                viewHolder.itemLayout.setOnClickListener(v -> onClickItem(objInformasi));
                viewHolder.buttonMore.setOnClickListener(v -> onClickItem(objInformasi));
            }
        }
        private void onClickItem(MstInformasi objInformasi){
            Intent intent   = new Intent(mContext, InformasiDetailActivity.class);
            intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_INFO);
            intent.putExtra(Constants.PARAMS_KEY, objInformasi);
            startActivity(intent);
        }
        class ItemViewHolder extends RecyclerView.ViewHolder {
            public ImageView viewImage;
            public TextView viewTitle, viewDate, buttonMore;
            public WebView viewContent;
            public RelativeLayout itemLayout, contentLayout;

            public ItemViewHolder(View itemView) {
                super(itemView);
                itemLayout      = itemView.findViewById(R.id.layoutItem);
                viewImage       = itemView.findViewById(R.id.informasiImage);
                viewTitle       = itemView.findViewById(R.id.informasiTitle);
                viewContent     = itemView.findViewById(R.id.informasiContent);
                viewDate        = itemView.findViewById(R.id.informasiDate);
                buttonMore      = itemView.findViewById(R.id.btnMore);
                viewImage.setVisibility(View.GONE);
                contentLayout   = itemView.findViewById(R.id.layOutContent);
            }
        }
        class FooterViewHolder extends RecyclerView.ViewHolder {
            public GoogleProgressBar progressLoading;
            public TextView labelLoading;
            public FooterViewHolder(View view) {
                super(view);
                progressLoading     = view.findViewById(R.id.loadingProgress);
                labelLoading        = view.findViewById(R.id.loadingView);
            }
        }
    }
}
