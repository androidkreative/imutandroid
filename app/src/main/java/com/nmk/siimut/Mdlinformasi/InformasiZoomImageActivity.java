package com.nmk.siimut.Mdlinformasi;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.squareup.picasso.Picasso;

/**
 * Created by Warsono on 01/07/2019.
 */

public class InformasiZoomImageActivity extends AppCompatActivity implements View.OnClickListener{

    private MstInformasi objInformasi;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(InformasiZoomImageActivity.this, InformasiDetailActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_INFO);
        intent.putExtra(Constants.PARAMS_KEY, objInformasi);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_zoom_image);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        Window w            = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        ImageView buttonBack        = findViewById(R.id.btnBack);
        buttonBack.setOnClickListener(this);
        PhotoView viewImage         = findViewById(R.id.imageDetail);

        String imgUrl   = getIntent().getStringExtra(Constants.IMAGE_KEY);
        objInformasi    = (MstInformasi) getIntent().getSerializableExtra(Constants.PARAMS_KEY);
        Picasso.get()
                .load(Uri.parse(imgUrl))
                .into(viewImage);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }





    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
