package com.nmk.siimut.Mdlinformasi;


import android.app.Activity;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.ApiService.BasicResponse;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.FancyToast;
import com.nmk.siimut.Utilities.Utility;

import java.io.File;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

public class InformasiTask {
    private static String strUserId     = "";
    private static String strUserAuth   = "";

    static void onSaveInformation(final Activity mContext, String strTitle, String strDescrption, byte[] mBitmapData, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId       = dataList.get(0).getUserId();
                strUserAuth     = dataList.get(0).getUserAuth();
            }
            Map<String, RequestBody> requestBodyMap = new HashMap<>();
            requestBodyMap.put("id", RequestBody.create(MediaType.parse("text/plain"), strUserId));
            requestBodyMap.put("auth", RequestBody.create(MediaType.parse("text/plain"), strUserAuth));
            requestBodyMap.put("title", RequestBody.create(MediaType.parse("text/plain"), strTitle));
            requestBodyMap.put("description", RequestBody.create(MediaType.parse("text/plain"), strDescrption));
            if (mBitmapData != null){
                String mFileName    = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(new Date())+".JPG";
                File FileSource     = Utility.createFileFromBitmap(mBitmapData, mFileName);
                String fileName     = "image\"; filename=\"" + mFileName;
                RequestBody Body    = RequestBody.create(MediaType.parse("multipart/form-data"), FileSource);
                requestBodyMap.put(fileName, Body);
            }
            InformasiService service        = ApiClient.getClient().create(InformasiService.class);
            Call<BasicResponse> mRequest    = service.postInformasi(requestBodyMap);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        default:
                            FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
//                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
//                        if (FailedAction != null)
//                            FailedAction.run();
                        FancyToast.makeMessage(mContext, FancyToast.SUCCESS, "Penambahan informasi berhasil", Toast.LENGTH_SHORT).show();
                        if (SuccessAction != null)
                            SuccessAction.run();
                    }
                }
            });
        }catch (Exception e) {
            FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
            if (FailedAction != null)
                FailedAction.run();
        }
    }
}
