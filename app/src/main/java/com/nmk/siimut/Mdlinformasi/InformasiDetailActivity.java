package com.nmk.siimut.Mdlinformasi;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.MdlNotifikasi.MstNotifikasi;
import com.nmk.siimut.MdlNotifikasi.NotifikasiActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.RoundedSide;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.TimeFunction;
import com.nmk.siimut.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

public class InformasiDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private MstInformasi objInformasi = null;
    private TextView viewTitle, viewDate;
    private WebView viewContent;
    private ImageView viewImage, buttonZoom;
    private Date createdDate;
    private SimpleDateFormat dateFormat;
    private String strInfoId;
    private String strImageURL;
    private String strState;

    @Override
    public void onBackPressed(){
        Intent intent;
        if (strState.contentEquals(Constants.VIEW_NOTIF))
            intent   = new Intent(InformasiDetailActivity.this, NotifikasiActivity.class);
        else {
            intent   = new Intent(InformasiDetailActivity.this, MainActivity.class);
            intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_INFO);
        }
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        viewTitle       = findViewById(R.id.informasiTitle);
        viewDate        = findViewById(R.id.informasiDate);
        viewContent     = findViewById(R.id.informasiContent);
        viewImage       = findViewById(R.id.informasiImage);
        buttonZoom      = findViewById(R.id.btnZoom);

        strState = getIntent().getStringExtra(Constants.VIEW_KEY);
        if (strState.contentEquals(Constants.VIEW_NOTIF)){
            MstNotifikasi onjNotif  = (MstNotifikasi) getIntent().getSerializableExtra(Constants.PARAMS_KEY);
            objInformasi            = new MstInformasi(onjNotif.getNotifId(), "", onjNotif.getDate(), onjNotif.getTitle(), onjNotif.getSortDesc(), onjNotif.getDescription());
            setInfoDetail.run();
        }else {
            objInformasi = (MstInformasi) getIntent().getSerializableExtra(Constants.PARAMS_KEY);
            strInfoId   = getIntent().getStringExtra(Constants.INFO_ID);
            new onGetInfoDetail().execute(0);
        }
    }
    private Runnable setInfoDetail = new Runnable() {
        @Override
        public void run() {
            dateFormat          = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            viewTitle.setText(objInformasi.getTitle());
            viewContent.loadData(objInformasi.getDescription(), "text/html", "utf-8");
            try {
                createdDate = dateFormat.parse(objInformasi.getDate());
            } catch (ParseException ignored) {}
            String strDate = Utility.ItemDateView(objInformasi.getDate());
            if (createdDate != null)
                strDate = TimeFunction.getTimeAgo(createdDate);
            viewDate.setText(strDate);
            if (objInformasi.getImage() != null && !objInformasi.getImage().contentEquals("")){
                strImageURL = objInformasi.getImage();
                viewImage.setVisibility(View.VISIBLE);
                buttonZoom.setVisibility(View.VISIBLE);
                int imageWidth = Utility.getScreenWidth();
                Picasso.get()
                        .load(Uri.parse(objInformasi.getImage()))
                        .resize((int) imageWidth, (int) imageWidth)
                        .centerCrop()
                        .transform(new RoundedSide(8, 0, RoundedSide.ALL))
                        .into(viewImage);
            }else{
                viewImage.setVisibility(View.GONE);
                buttonZoom.setVisibility(View.GONE);
            }
        }
    };
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
//                onBackPressed();
                break;
            case R.id.btnZoom:
                Intent intent   = new Intent(InformasiDetailActivity.this, InformasiZoomImageActivity.class);
                intent.putExtra(Constants.PARAMS_KEY, objInformasi);
                intent.putExtra(Constants.IMAGE_KEY, strImageURL);
                startActivity(intent);
                break;
        }
    }
    @SuppressLint("StaticFieldLeak")
    class onGetInfoDetail extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                InformasiService service                = ApiClient.getClient().create(InformasiService.class);
                Call<InformasiResponse> mRequest        = service.getInformasiDetail(strInfoId);
                Response<InformasiResponse> Response   = mRequest.execute();
                InformasiResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            objInformasi = Result.getData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            if (objInformasi != null)
                setInfoDetail.run();
        }
    }




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
