package com.nmk.siimut.MdlFasilitas;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.FancyToast;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class FasilitasActivity extends AppCompatActivity implements View.OnClickListener{

    private List<MstFasilitas> dataList   = new ArrayList<MstFasilitas>();
    private RecyclerView dataView;
    private RelativeLayout contentView, noInternetView;
    private SwipeRefreshLayout refreshLayout;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(FasilitasActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_HOME);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fasilitas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        ImageView buttonBack    = findViewById(R.id.btnBack);
        buttonBack.setOnClickListener(this);
        contentView             = findViewById(R.id.viewContent);
        dataView                = findViewById(R.id.viewFasilitas);
        noInternetView          = findViewById(R.id.viewUnavailable);
        noInternetView.setVisibility(View.GONE);
        refreshLayout           = findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.YELLOW);
        refreshLayout.setDistanceToTriggerSync(70);
        refreshLayout.setSize(SwipeRefreshLayout.DEFAULT);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utility.isNetworkConnected())
                    new onGetFasilitas().execute(0);
                else
                    FancyToast.makeMessage(FasilitasActivity.this, FancyToast.WARNING, getResources().getString(R.string.msg_conection_error), Toast.LENGTH_LONG).show();
            }
        });
        if (!Utility.isNetworkConnected()){
            contentView.setVisibility(View.GONE);
            noInternetView.setVisibility(View.VISIBLE);
        }else
            onSetDataFasilitas();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }
    private void onSetDataFasilitas(){
        if (dataList.size() > 0)
            dataList.clear();
        dataList = SqlFasilitas.getAllFasilitas();
        if (dataList.size() > 0){
            dataAdapter newAdapter   = new dataAdapter(dataList);
            dataView.setAdapter(newAdapter);
            dataView.setNestedScrollingEnabled(false);
            dataView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
            dataView.setVisibility(View.VISIBLE);
            contentView.setVisibility(View.VISIBLE);
            noInternetView.setVisibility(View.GONE);
        }else{
            dataView.setVisibility(View.GONE);
            contentView.setVisibility(View.GONE);
            noInternetView.setVisibility(View.VISIBLE);
        }
    }
    class dataAdapter extends RecyclerView.Adapter<dataAdapter.ViewHolder> {
        private List<MstFasilitas> mItems;

        private dataAdapter(List<MstFasilitas> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fasilitas, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstFasilitas objectData   = mItems.get(position);
            viewHolder.viewItem.setText(objectData.getDescription());
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView viewItem;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                viewItem        = itemView.findViewById(R.id.itemView);
            }
        }
    }
    class onGetFasilitas extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                FasilitasService service             = ApiClient.getClient().create(FasilitasService.class);
                Call<FasilitasResponse> mRequest     = service.getDataList("");
                Response<FasilitasResponse> Response = mRequest.execute();
                FasilitasResponse Result             = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlFasilitas.DeleteFasilitas();
                            List<MstFasilitas> ListData     = Result.getDataList();
                            int dataCount                   = ListData.size();
                            if (dataCount > 0){
                                for (int i=0; i < dataCount; i++){
                                    MstFasilitas objData  = ListData.get(i);
                                    SqlFasilitas.addDataFasilitas(objData);
                                }
                            }
                            break;
                        case 202:
                            SqlFasilitas.DeleteFasilitas();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            onSetDataFasilitas();
            refreshLayout.setRefreshing(false);
        }
    }




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
