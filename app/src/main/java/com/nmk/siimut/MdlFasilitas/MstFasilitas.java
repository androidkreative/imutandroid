package com.nmk.siimut.MdlFasilitas;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.MdlBiaya.MstBiayaDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstFasilitas {
    @SerializedName("ID")
    String Id;

    @SerializedName("DESCRIPTION")
    String Description;

    public MstFasilitas() {
    }

    public MstFasilitas(String id, String description) {
        Id          = id;
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}