package com.nmk.siimut.MdlFasilitas;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface FasilitasService {

    @GET(Constants.API_GET_SARANA)
    Call<FasilitasResponse> getDataList(@Query("id") String UserID);
}
