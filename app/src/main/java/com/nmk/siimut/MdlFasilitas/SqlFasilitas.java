package com.nmk.siimut.MdlFasilitas;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlFasilitas {
    private static String TABLE_FASILITAS       = "MST_FASILITAS";
    private static String FIELD_FASILITAS_ID    = "FASILITAS_ID";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_FASILITAS + "("
                + FIELD_FASILITAS_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);
    }
    public static void addDataFasilitas(MstFasilitas Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_FASILITAS_ID, Data.getId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        db.insert(TABLE_FASILITAS, null, values);
    }
    public static List<MstFasilitas> getAllFasilitas() {
        List<MstFasilitas> DataList   = new ArrayList<MstFasilitas>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_FASILITAS;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstFasilitas Data = new MstFasilitas();
                Data.setId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteFasilitas() {
        if (getAllFasilitas().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_FASILITAS);
        }
    }
}