package com.nmk.siimut.MdlFasilitas;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class FasilitasResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstFasilitas> DataList    = new ArrayList<MstFasilitas>();

    public List<MstFasilitas> getDataList() {
        return DataList;
    }
}
