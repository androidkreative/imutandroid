package com.nmk.siimut.MdlNotifikasi;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstNotifikasi implements Serializable {
    @SerializedName("ID")
    String NotifId;

    @SerializedName("INFO_ID")
    String InfoId;

    @SerializedName("DATE")
    String Date;

    @SerializedName("TITLE")
    String Title;

    @SerializedName("SORT_DESC")
    String SortDesc;

    @SerializedName("DESCRIPTION")
    String Description;

    @SerializedName("USER_ID")
    String UserId;

    @SerializedName("STATUS")
    String Status;

    public MstNotifikasi(String notifId, String infoId, String date, String title, String sortDesc, String description, String userId, String status) {
        NotifId     = notifId;
        InfoId      = infoId;
        Date        = date;
        Title       = title;
        SortDesc    = sortDesc;
        Description = description;
        UserId      = userId;
        Status      = status;
    }

    public String getNotifId() {
        return NotifId;
    }

    public void setNotifId(String notifId) {
        NotifId = notifId;
    }

    public String getInfoId() {
        return InfoId;
    }

    public void setInfoId(String infoId) {
        InfoId = infoId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSortDesc() {
        return SortDesc;
    }

    public void setSortDesc(String sortDesc) {
        SortDesc = sortDesc;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}