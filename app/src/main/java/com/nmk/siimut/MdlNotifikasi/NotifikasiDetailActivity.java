package com.nmk.siimut.MdlNotifikasi;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.TimeFunction;
import com.nmk.siimut.Utilities.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

public class NotifikasiDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private MstNotifikasi objNotif = null;
    private TextView viewTitle, viewDate;
    private WebView viewContent;
    private ImageView viewImage, buttonZoom;
    private Date createdDate;
    private SimpleDateFormat dateFormat;
    private String strUserId, strUserAuth, strNotifId;

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(NotifikasiDetailActivity.this, NotifikasiActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        viewTitle       = findViewById(R.id.informasiTitle);
        viewDate        = findViewById(R.id.informasiDate);
        viewContent     = findViewById(R.id.informasiContent);
        viewImage       = findViewById(R.id.informasiImage);
        buttonZoom      = findViewById(R.id.btnZoom);

        if (getIntent().getSerializableExtra(Constants.PARAMS_KEY) != null) {
            objNotif = (MstNotifikasi) getIntent().getSerializableExtra(Constants.PARAMS_KEY);
            setNotifDetail.run();
        }else{
            strNotifId = getIntent().getStringExtra(Constants.NOTIF_ID);
            new onGetNotifDetail().execute(0);
        }
    }
    private Runnable setNotifDetail = new Runnable() {
        @Override
        public void run() {
            dateFormat              = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            viewTitle.setText(objNotif.getTitle());
            viewContent.loadData(objNotif.getDescription(), "text/html", "utf-8");
            try {
                createdDate = dateFormat.parse(objNotif.getDate());
            } catch (ParseException ignored) {}
            String strDate = Utility.ItemDateView(objNotif.getDate());
            if (createdDate != null)
                strDate = TimeFunction.getTimeAgo(createdDate);
            viewDate.setText(strDate);
            viewImage.setVisibility(View.GONE);
            buttonZoom.setVisibility(View.GONE);
        }
    };
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
//                onBackPressed();
                break;
        }
    }
    @SuppressLint("StaticFieldLeak")
    class onGetNotifDetail extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                List<MstUser> dataList = SqlUser.getAllData();
                if (dataList.size() > 0) {
                    MstUser data    = dataList.get(0);
                    strUserId       = data.getUserId();
                    strUserAuth     = data.getUserAuth();
                }

                NotifikasiService service               = ApiClient.getClient().create(NotifikasiService.class);
                Call<NotifikasiResponse> mRequest       = service.getNotifDetail(strUserId, strUserAuth, strNotifId);
                Response<NotifikasiResponse> Response   = mRequest.execute();
                NotifikasiResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            objNotif = Result.getData();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            if (objNotif != null)
                setNotifDetail.run();
        }
    }




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
