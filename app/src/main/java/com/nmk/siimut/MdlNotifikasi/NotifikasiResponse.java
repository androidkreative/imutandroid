package com.nmk.siimut.MdlNotifikasi;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class NotifikasiResponse extends BasicResponse {

    @SerializedName("DATA")
    MstNotifikasi Data;

    public MstNotifikasi getData() {
        return Data;
    }
}
