package com.nmk.siimut.MdlNotifikasi;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface NotifikasiService {

    @GET(Constants.API_GET_NOTIFIKASI)
    Call<NotifikasiListResponse> getNotifikasi(
            @Query("id") String UserId,
            @Query("auth") String UserAuth,
            @Query("limit") String Limit,
            @Query("page") String Page);

    @GET(Constants.API_GET_NOTIF_DETAIL)
    Call<NotifikasiResponse> getNotifDetail(
            @Query("id") String UserId,
            @Query("auth") String UserAuth,
            @Query("notif_id") String notif_id);
}
