package com.nmk.siimut.MdlNotifikasi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jpardogo.android.googleprogressbar.library.GoogleProgressBar;
import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.Mdlinformasi.InformasiDetailActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.TimeFunction;
import com.nmk.siimut.Utilities.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class NotifikasiActivity extends AppCompatActivity implements View.OnClickListener{
    
    private List<MstNotifikasi> notifikasiList  = new ArrayList<MstNotifikasi>();
    private RecyclerView viewNotifikasi;
    private LinearLayout viewLayoutEmpty;
    private LinearLayout loadingInfo;
    private ShimmerLayout shimmerLayout;
    private LinearLayoutManager layoutManager;
    private notifikasiAdapter notifikasiAdapter;
    private boolean isLoading   = false;
    private int page            = 0;
    private ImageView viewImageEmpty;
    private TextView viewTitleEmpty, viewDescEmpty;
    private String strUserId, strUserAuth;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(NotifikasiActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_ACCOUNT);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifikasi);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        List<MstUser> dataList = SqlUser.getAllData();
        if (dataList.size() > 0) {
            MstUser data    = dataList.get(0);
            strUserId       = data.getUserId();
            strUserAuth     = data.getUserAuth();
        }
        ImageView buttonBack    = findViewById(R.id.btnBack);
        buttonBack.setOnClickListener(this);
        viewLayoutEmpty         = findViewById(R.id.viewEmpty);
        viewImageEmpty          = findViewById(R.id.viewEmptyImage);
        viewTitleEmpty          = findViewById(R.id.viewEmptyTitle);
        viewDescEmpty           = findViewById(R.id.viewEmptyDescription);
        viewNotifikasi          = findViewById(R.id.viewNotifikasi);
        loadingInfo             = findViewById(R.id.loadingNotifikasi);
        layoutManager           = new LinearLayoutManager(this);
        viewNotifikasi.setLayoutManager(layoutManager);
        viewNotifikasi.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int lastvisibleitemposition = layoutManager.findLastVisibleItemPosition();
                if (lastvisibleitemposition == notifikasiAdapter.getItemCount() - 1) {
                    if (!isLoading) {
                        isLoading = true;
                        new GetMoreDataTask().execute(0);
                    }
                }
            }
        });
        onGetDataInformasi();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }
    private void onGetDataInformasi(){
        if (Utility.isNetworkConnected()) {
            onShowLoadingInfo();
            if (notifikasiList.size() > 0)
                notifikasiList.clear();
            new GetNotifikasiTask().execute(0);
        }else{
            viewNotifikasi.setVisibility(View.GONE);
            viewLayoutEmpty.setVisibility(View.VISIBLE);
            viewImageEmpty.setImageDrawable(getResources().getDrawable(R.drawable.ic_requirement));
            viewTitleEmpty.setText(R.string.no_conection_title);
            viewDescEmpty.setText(R.string.no_conection_desc);
        }
    }
    class GetNotifikasiTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            onShowLoadingInfo();
        }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                NotifikasiService apiService                = ApiClient.getClient().create(NotifikasiService.class);
                Call<NotifikasiListResponse> mRequest           = apiService.getNotifikasi(strUserId, strUserAuth, String.valueOf(Constants.MIN_LIST_LOADING), String.valueOf(page));
                Response<NotifikasiListResponse> Response       = mRequest.execute();
                NotifikasiListResponse responseResult           = Response.body();
                if (responseResult != null){
                    switch (responseResult.getResultState()) {
                        case 200:
                            notifikasiList = responseResult.getDataList();
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onProgressUpdate(Integer... values) { }
        @Override
        protected void onPostExecute(String result) {
            if (notifikasiList.size() > 0){
                notifikasiAdapter = new notifikasiAdapter(NotifikasiActivity.this, notifikasiList);
                viewNotifikasi.setAdapter(notifikasiAdapter);
                viewNotifikasi.setVisibility(View.VISIBLE);
                viewLayoutEmpty.setVisibility(View.GONE);
            }else{
                viewNotifikasi.setVisibility(View.GONE);
                viewLayoutEmpty.setVisibility(View.VISIBLE);
            }
            onHideloadingInfo();
        }
    }
    private void onShowLoadingInfo(){
        viewNotifikasi.setVisibility(View.GONE);
        viewLayoutEmpty.setVisibility(View.GONE);
        loadingInfo.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        loadingInfo.removeAllViews();
        for (int i = 0; i < 10; i++) {
            View child  = getLayoutInflater().inflate(R.layout.item_notifikasi_shimmer, null);
            child.setLayoutParams(params);
            shimmerLayout   = child.findViewById(R.id.shimmer_layout);
            shimmerLayout.startShimmerAnimation();
            loadingInfo.setOrientation(LinearLayout.VERTICAL);
            loadingInfo.addView(child);
        }
    }
    private void onHideloadingInfo(){
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            shimmerLayout.stopShimmerAnimation();
            loadingInfo.setVisibility(View.GONE);
        }, 300);
    }

    class GetMoreDataTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() {
            page        = page + 1;
        }
        @Override
        protected String doInBackground(Integer... integers) {
            String strResult = "";
            try {
                NotifikasiService apiService                 = ApiClient.getClient().create(NotifikasiService.class);
                Call<NotifikasiListResponse> mRequest            = apiService.getNotifikasi(strUserId, strUserAuth, String.valueOf(Constants.MIN_LIST_LOADING), String.valueOf(page));
                Response<NotifikasiListResponse> Response        = mRequest.execute();
                NotifikasiListResponse responseResult            = Response.body();
                if (responseResult != null){
                    strResult       = String.valueOf(responseResult.getResultState());
                    switch (responseResult.getResultState()) {
                        case 200:
                            List<MstNotifikasi> responseData     = responseResult.getDataList();
                            int count                           = responseData.size();
                            if (count > 0){
                                int index = notifikasiList.size();
                                for (int i= 0; i < count; i++){
                                    MstNotifikasi objInformasi = responseData.get(i);
                                    notifikasiList.add( index, objInformasi);
                                    index = index+1;
                                }
                            }
                            break;
                        case 202:
                            Dialog.ConfirmationDialog(NotifikasiActivity.this, getResources().getString(R.string.label_information),
                                    responseResult.getResultMessage(), expiredAuth(NotifikasiActivity.this), null);
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) {}
            return strResult;
        }
        @Override
        protected void onProgressUpdate(Integer... values) { }
        @Override
        protected void onPostExecute(String result) {
            if (result.contentEquals("200"))
                notifikasiAdapter.notifyDataSetChanged();
            isLoading = false;
        }
    }

    public class notifikasiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int VIEW_TYPE_HEADER  = 0;
        private final int VIEW_TYPE_ITEM    = 1;
        private final int VIEW_TYPE_FOOTER  = 2;
        private List<MstNotifikasi> mItems;
        private Activity mContext;
        private double imageSize;
        private Date createdDate;
        private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

        public notifikasiAdapter(Activity context, List<MstNotifikasi> itemData) {
            super();
            mItems      = itemData;
            mContext    = context;
            imageSize   = 0.2 * Utility.getScreenWidth();
        }
        @Override
        public int getItemViewType(int position) {
            if (isPositionFooter(position))
                return VIEW_TYPE_FOOTER;
            else
                return VIEW_TYPE_ITEM;
        }
        private boolean isPositionHeader(int position) {
            return position == 0;
        }
        private boolean isPositionFooter(int position) {
            return position >= mItems.size();
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            if (viewType == VIEW_TYPE_FOOTER) {
                View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading, viewGroup, false);
                return new FooterViewHolder(view);
            }else{
                View view       = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notifikasi, viewGroup, false);
                return new ItemViewHolder(view);
            }
        }
        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof ItemViewHolder) {
                setItemHolder((ItemViewHolder) holder, mItems.get(position));
            }else if (holder instanceof FooterViewHolder) {
                FooterViewHolder FooterViewHolder = (FooterViewHolder) holder;
                FooterViewHolder.progressLoading.setVisibility(View.VISIBLE);
                FooterViewHolder.labelLoading.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public int getItemCount() {
            if(mItems.size() % Constants.MIN_LIST_LOADING == 0)
                return mItems.size() + 1;
            else
                return mItems.size();
        }
        private void setItemHolder(final ItemViewHolder viewHolder, final MstNotifikasi objNotif){
            if (objNotif != null && objNotif.getNotifId() != null){
                viewHolder.viewTitle.setText(objNotif.getTitle());
                viewHolder.viewContent.loadData(objNotif.getSortDesc(), "text/html", "utf-8");
                viewHolder.viewContent.getSettings().setJavaScriptEnabled(true);
                try {
                    createdDate = dateFormat.parse(objNotif.getDate());
                } catch (ParseException ignored) {}
                String strDate = Utility.ItemDateView(objNotif.getDate());
                if (createdDate != null)
                    strDate = TimeFunction.getTimeAgo(createdDate);
                viewHolder.viewDate.setText(strDate);
                viewHolder.viewTitle.setOnClickListener(v -> onClickItem(objNotif));
                viewHolder.contentLayout.setOnClickListener(v -> onClickItem(objNotif));
                viewHolder.itemLayout.setOnClickListener(v -> onClickItem(objNotif));
            }
        }
        private void onClickItem(MstNotifikasi objNotif){
            if (!objNotif.getNotifId().contentEquals("")){
                Intent intentInfo   = new Intent(mContext, InformasiDetailActivity.class);
                intentInfo.putExtra(Constants.VIEW_KEY, Constants.VIEW_NOTIF);
                intentInfo.putExtra(Constants.PARAMS_KEY, objNotif);
                startActivity(intentInfo);
            }else{
                Intent intentNotif   = new Intent(mContext, NotifikasiDetailActivity.class);
                intentNotif.putExtra(Constants.PARAMS_KEY, objNotif);
                startActivity(intentNotif);
            }
        }
        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView viewTitle, viewDate;
            public WebView viewContent;
            public RelativeLayout itemLayout, contentLayout;
            public LinearLayout layoutDate;

            public ItemViewHolder(View itemView) {
                super(itemView);
                itemLayout      = itemView.findViewById(R.id.layoutItem);
                viewTitle       = itemView.findViewById(R.id.notifikasiTitle);
                viewContent     = itemView.findViewById(R.id.notifikasiContent);
                viewDate        = itemView.findViewById(R.id.notifikasiDate);
                layoutDate      = itemView.findViewById(R.id.dateLayout);
                layoutDate.setVisibility(View.GONE);
                contentLayout   = itemView.findViewById(R.id.layOutContent);
            }
        }
        class FooterViewHolder extends RecyclerView.ViewHolder {
            public GoogleProgressBar progressLoading;
            public TextView labelLoading;
            public FooterViewHolder(View view) {
                super(view);
                progressLoading     = view.findViewById(R.id.loadingProgress);
                labelLoading        = view.findViewById(R.id.loadingView);
            }
        }
    }




    private static Runnable expiredAuth(final Activity activity) {
        Runnable mRun = new Runnable(){
            public void run(){
                Utility.onUserLogout(activity);
            }
        };
        return mRun;
    }
    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
