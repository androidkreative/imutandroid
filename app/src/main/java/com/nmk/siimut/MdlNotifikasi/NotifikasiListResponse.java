package com.nmk.siimut.MdlNotifikasi;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class NotifikasiListResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstNotifikasi> DataList    = new ArrayList<MstNotifikasi>();

    public List<MstNotifikasi> getDataList() {
        return DataList;
    }
}
