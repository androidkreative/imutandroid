package com.nmk.siimut.MdlAddress;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class AddressResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstAddress> DataList    = new ArrayList<MstAddress>();

    public List<MstAddress> getDataList() {
        return DataList;
    }
}
