package com.nmk.siimut.MdlAddress;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface AddressService {

    @GET(Constants.API_GET_ADDRESS)
    Call<AddressResponse> getAddress(@Query("id") String UserID);
}
