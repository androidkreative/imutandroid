package com.nmk.siimut.MdlAddress;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlAddress {
    private static String TABLE_ADDRESS         = "MST_ADDRESS";
    private static String FIELD_ID              = "ID";
    private static String FIELD_TITLE           = "TITLE";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";
    private static String FIELD_PHONE           = "PHONE";
    private static String FIELD_POSITION        = "POSITION";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_ADDRESS + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_TITLE + " TEXT, "
                + FIELD_DESCRIPTION + " TEXT, "
                + FIELD_PHONE + " TEXT, "
                + FIELD_POSITION + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);
    }
    public static void addData(MstAddress Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_TITLE, Data.getTitle());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        values.put(FIELD_PHONE, Data.getPhone());
        values.put(FIELD_POSITION, Data.getPosition());
        db.insert(TABLE_ADDRESS, null, values);
    }
    public static List<MstAddress> getAllData() {
        List<MstAddress> DataList   = new ArrayList<MstAddress>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_ADDRESS;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstAddress Data = new MstAddress();
                Data.setId(cursor.getString(0));
                Data.setTitle(cursor.getString(1));
                Data.setDescription(cursor.getString(2));
                Data.setPhone(cursor.getString(3));
                Data.setPosition(cursor.getString(4));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteData() {
        if (getAllData().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_ADDRESS);
        }
    }
}