package com.nmk.siimut.MdlAddress;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstAddress {
    @SerializedName("ID")
    String Id;

    @SerializedName("TITLE")
    String Title;

    @SerializedName("DESCRIPTION")
    String Description;

    @SerializedName("PHONE")
    String Phone;

    @SerializedName("POSITION")
    String Position;

    public MstAddress() {
    }

    public MstAddress(String id, String title, String description, String phone, String position) {
        Id              = id;
        Title           = title;
        Description     = description;
        Phone           = phone;
        Position        = position;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }
}