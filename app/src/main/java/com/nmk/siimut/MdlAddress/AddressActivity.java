package com.nmk.siimut.MdlAddress;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class AddressActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback {

    private List<MstAddress> dataList = new ArrayList<MstAddress>();
    private GoogleMap mapView;
    private RelativeLayout contentView, noInternetView;
    private BottomSheetBehavior behaviorAddress;
    private BottomSheetDialog addressDialog;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(AddressActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_HOME);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        ImageView buttonBack            = findViewById(R.id.btnBack);
        buttonBack.setOnClickListener(this);
        contentView                     = findViewById(R.id.viewContent);
        noInternetView  = findViewById(R.id.viewUnavailable);
        noInternetView.setVisibility(View.GONE);
        
        if (Utility.isNetworkConnected()) {
            SupportMapFragment mapFragment  = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapLocation);
            mapFragment.getMapAsync(this);
            contentView.setVisibility(View.VISIBLE);
            noInternetView.setVisibility(View.GONE);

            //SET BOTTOM SHEET
            View bottomaddress      = findViewById(R.id.bottomSheetAddress);
            behaviorAddress         = BottomSheetBehavior.from(bottomaddress);
        }else {
            contentView.setVisibility(View.GONE);
            noInternetView.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnShowAddress:
                onShowDialogAddress();
                break;
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapView                     = googleMap;
        if (dataList.size() > 0)
            dataList.clear();
        dataList   = SqlAddress.getAllData();
        if (dataList.size() >0){
            for (int i=0; i<dataList.size(); i++){
                MstAddress data     = dataList.get(i);
                String[] separated  = data.getPosition().split(",");
                double latitude     = Double.valueOf(separated[0]);
                double longitude    = Double.valueOf(separated[1]);
                LatLng location     = new LatLng(latitude, longitude);
                Marker marker       = mapView.addMarker(new MarkerOptions()
                        .position(location)
                        .title(data.getTitle()));
                marker.setTag(i);
                marker.showInfoWindow();
            }
            mapView.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    int pos                 = (int)(marker.getTag());
                    MstAddress objectData   = dataList.get(pos);
                    onGotoMapsDirection(objectData.getPosition());
                    return false;
                }
            });
            mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-6.182386, 106.636148),14f));
            onShowDialogAddress();
        }

    }
    public void onShowDialogAddress() {
        if (behaviorAddress.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behaviorAddress.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        addressDialog               = new BottomSheetDialog(this);
        final View view             = getLayoutInflater().inflate(R.layout.view_list_address, null);
        ImageView buttonClose       = view.findViewById(R.id.btnClose);
        RecyclerView addressView    = view.findViewById(R.id.viewAddress);
        addressView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
        dataAdapter newAdapter      = new dataAdapter(dataList);
        addressView.setAdapter(newAdapter);
        addressView.setNestedScrollingEnabled(false);

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addressDialog != null) addressDialog.dismiss();
            }
        });
        addressDialog.setContentView(view);
        addressDialog.setCanceledOnTouchOutside(false);
        addressDialog.show();
        addressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                addressDialog = null;
            }
        });
    }
    class dataAdapter extends RecyclerView.Adapter<dataAdapter.ViewHolder> {
        private List<MstAddress> mItems;

        private dataAdapter(List<MstAddress> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstAddress objectData   = mItems.get(position);
            viewHolder.viewTitle.setText(objectData.getTitle());
            viewHolder.viewAddress.setText(objectData.getDescription());
            viewHolder.viewPhone.setText(objectData.getPhone());
            viewHolder.buttonDirection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onGotoMapsDirection(objectData.getPosition());
                }
            });
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView viewTitle, viewAddress, viewPhone;
            ImageView buttonDirection;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                viewTitle       = itemView.findViewById(R.id.titleView);
                viewAddress     = itemView.findViewById(R.id.addressView);
                viewPhone       = itemView.findViewById(R.id.phoneView);
                buttonDirection = itemView.findViewById(R.id.btnDirection);
            }
        }
    }
    private void onGotoMapsDirection(String strLocation){
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+strLocation);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }





    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
