package com.nmk.siimut.MdlOther;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface OtherService {

    @GET(Constants.API_GET_AGAMA)
    Call<AgamaResponse> getAgama(@Query("id") String UserID);

    @GET(Constants.API_GET_STATUS)
    Call<StatusResponse> getStatus(@Query("id") String UserID);

    @GET(Constants.API_GET_AJARAN)
    Call<AjaranResponse> getAjaran(@Query("id") String UserID);
}
