package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstAgama {
    @SerializedName("AGAMA_ID")
    String Id;

    @SerializedName("AGAMA_NAME")
    String AgamaName;

    public MstAgama() {
    }

    public MstAgama(String id, String agamaName) {
        Id = id;
        AgamaName = agamaName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAgamaName() {
        return AgamaName;
    }

    public void setAgamaName(String agamaName) {
        AgamaName = agamaName;
    }
}