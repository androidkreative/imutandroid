package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstQuestionEdu {
    @SerializedName("ID")
    String Id;

    @SerializedName("DESC")
    String Description;

    public MstQuestionEdu() {
    }

    public MstQuestionEdu(String id, String description) {
        Id = id;
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}