package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstStatus {
    @SerializedName("STATUS_ID")
    String Id;

    @SerializedName("STATUS_NAME")
    String StatusName;

    public MstStatus() {
    }

    public MstStatus(String id, String statusName) {
        Id = id;
        StatusName = statusName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStatusName() {
        return StatusName;
    }

    public void setStatusName(String statusName) {
        StatusName = statusName;
    }
}