package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class StatusResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstStatus> DataList    = new ArrayList<MstStatus>();

    public List<MstStatus> getDataList() {
        return DataList;
    }
}
