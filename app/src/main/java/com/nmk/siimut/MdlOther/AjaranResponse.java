package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;


/**
 * Created by Warsono on 01/07/2019.
 */

public class AjaranResponse extends BasicResponse {

    @SerializedName("DATA")
    MstAjaran ObjData;

    public MstAjaran getObjData() {
        return ObjData;
    }
}
