package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class AgamaResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstAgama> DataList    = new ArrayList<MstAgama>();

    public List<MstAgama> getDataList() {
        return DataList;
    }
}
