package com.nmk.siimut.MdlOther;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlOther {
    private static String TABLE_AGAMA           = "MST_AGAMA";
    private static String TABLE_STATUS          = "MST_STATUS";
    private static String TABLE_QUESTION_EDU    = "MST_QUESTION_EDU";
    private static String FIELD_ID              = "ID";
    private static String FIELD_NAME            = "NAME";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";

    private static String TABLE_AJARAN          = "MST_AJARAN";
    private static String FIELD_TAHUN_AJARAN    = "TAHUN_AJARAN";
    private static String FIELD_BIAYA_DAFTAR    = "BIAYA_DAFTAR";
    private static String FIELD_BIAYA_SKS       = "BIAYA_SKS";
    private static String FIELD_STATUS          = "STATUS";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_AGAMA + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_NAME + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);

        String CREATE_TABLE_STATUS = "CREATE TABLE " + TABLE_STATUS + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_NAME + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_STATUS);

        String CREATE_TABLE_QUESTION_EDU = "CREATE TABLE " + TABLE_QUESTION_EDU + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_QUESTION_EDU);

        String CREATE_TABLE_AJARAN = "CREATE TABLE " + TABLE_AJARAN + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_TAHUN_AJARAN + " TEXT, "
                + FIELD_BIAYA_DAFTAR + " TEXT, "
                + FIELD_BIAYA_SKS + " TEXT, "
                + FIELD_STATUS + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_AJARAN);
    }
    public static void addDataAgama(MstAgama Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_NAME, Data.getAgamaName());
        db.insert(TABLE_AGAMA, null, values);
    }
    public static List<MstAgama> getAllAgama() {
        List<MstAgama> DataList   = new ArrayList<MstAgama>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_AGAMA;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstAgama Data = new MstAgama();
                Data.setId(cursor.getString(0));
                Data.setAgamaName(cursor.getString(1));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static MstAgama getAgamaById(String strData) {
        String selectQuery 			= "SELECT  * FROM " + TABLE_AGAMA+ " WHERE " + FIELD_ID + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        MstAgama Data               = new MstAgama();
        if (cursor.moveToFirst()) {
            do {
                Data.setId(cursor.getString(0));
                Data.setAgamaName(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return Data;
    }
    public static void DeleteAgama() {
        if (getAllAgama().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_AGAMA);
        }
    }

    public static void addDataStatus(MstStatus Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_NAME, Data.getStatusName());
        db.insert(TABLE_STATUS, null, values);
    }
    public static List<MstStatus> getAllStatus() {
        List<MstStatus> DataList   = new ArrayList<MstStatus>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_STATUS;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstStatus Data = new MstStatus();
                Data.setId(cursor.getString(0));
                Data.setStatusName(cursor.getString(1));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static MstStatus getStatusById(String strData) {
        String selectQuery 			= "SELECT  * FROM " + TABLE_STATUS+ " WHERE " + FIELD_ID + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        MstStatus Data              = new MstStatus();
        if (cursor.moveToFirst()) {
            do {
                Data.setId(cursor.getString(0));
                Data.setStatusName(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return Data;
    }
    public static void DeleteStatus() {
        if (getAllStatus().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_STATUS);
        }
    }

    public static void addDataQuestionEdu(MstQuestionEdu Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        db.insert(TABLE_QUESTION_EDU, null, values);
    }
    public static List<MstQuestionEdu> getAllQuestionEdu() {
        List<MstQuestionEdu> DataList   = new ArrayList<MstQuestionEdu>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_QUESTION_EDU;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstQuestionEdu Data = new MstQuestionEdu();
                Data.setId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteQuestionEdu() {
        if (getAllQuestionEdu().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_QUESTION_EDU);
        }
    }


    public static void addDataAjaran(MstAjaran Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_TAHUN_AJARAN, Data.getTahunAjaran());
        values.put(FIELD_BIAYA_DAFTAR, Data.getBiayaDaftar());
        values.put(FIELD_BIAYA_SKS, Data.getBiayaSKS());
        values.put(FIELD_STATUS, Data.getStatus());
        db.insert(TABLE_AJARAN, null, values);
    }
    public static List<MstAjaran> getAllAjaran() {
        List<MstAjaran> DataList    = new ArrayList<MstAjaran>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_AJARAN;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstAjaran Data = new MstAjaran();
                Data.setId(cursor.getString(0));
                Data.setTahunAjaran(cursor.getString(1));
                Data.setBiayaDaftar(cursor.getString(2));
                Data.setBiayaSKS(cursor.getString(3));
                Data.setStatus(cursor.getString(4));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteAjaran() {
        if (getAllAjaran().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_AJARAN);
        }
    }
}