package com.nmk.siimut.MdlOther;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstAjaran {
    @SerializedName("ID")
    String Id;

    @SerializedName("TAHUN_AJARAN")
    String TahunAjaran;

    @SerializedName("BIAYA_DAFTAR")
    String BiayaDaftar;

    @SerializedName("BIAYA_SKS")
    String BiayaSKS;

    @SerializedName("STATUS")
    String Status;

    public MstAjaran() {
    }

    public MstAjaran(String id, String tahunAjaran, String biayaDaftar, String biayaSKS, String status) {
        Id = id;
        TahunAjaran = tahunAjaran;
        BiayaDaftar = biayaDaftar;
        BiayaSKS = biayaSKS;
        Status = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTahunAjaran() {
        return TahunAjaran;
    }

    public void setTahunAjaran(String tahunAjaran) {
        TahunAjaran = tahunAjaran;
    }

    public String getBiayaDaftar() {
        return BiayaDaftar;
    }

    public void setBiayaDaftar(String biayaDaftar) {
        BiayaDaftar = biayaDaftar;
    }

    public String getBiayaSKS() {
        return BiayaSKS;
    }

    public void setBiayaSKS(String biayaSKS) {
        BiayaSKS = biayaSKS;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}