package com.nmk.siimut.MdlBiaya;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlFakultas.FakultasActivity;
import com.nmk.siimut.MdlOther.MstAjaran;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class BiayaActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(BiayaActivity.this, FakultasActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_biaya_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        ImageView buttonClose       = findViewById(R.id.btnClose);
        buttonClose.setOnClickListener(this);
        RecyclerView biayaMain      = findViewById(R.id.viewBiayaMain);
        biayaMain.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
        TextView sksView            = findViewById(R.id.viewBiayaSks);

        Intent bundle   = getIntent();
        String strData  = bundle.getStringExtra(Constants.PARAMS_KEY);
        List<MstBiaya> dataBiaya    = SqlBiaya.getBiayaByFakultas(strData);
        if (dataBiaya.size() > 0){
            biayaMainAdapter newAdapter = new biayaMainAdapter(dataBiaya);
            biayaMain.setAdapter(newAdapter);
            biayaMain.setNestedScrollingEnabled(false);

            List<MstAjaran> dataAjaran = SqlOther.getAllAjaran();
            if (dataAjaran.size() > 0){
                MstAjaran objAjaran = dataAjaran.get(0);
                sksView.setText("Catatan : Biaya Konversi Rp. "+ Utility.currencyFormat(Double.valueOf(objAjaran.getBiayaSKS())) +" / SKS");
            }
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                onBackPressed();
                break;
        }
    }
    class biayaMainAdapter extends RecyclerView.Adapter<biayaMainAdapter.ViewHolder> {
        private List<MstBiaya> mItems;

        private biayaMainAdapter(List<MstBiaya> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_biaya_child, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstBiaya objectData   = mItems.get(position);
            viewHolder.titleView.setText(objectData.getDescription());
            List<MstBiayaDetail> dataDetail = SqlBiaya.getBiayaByID(objectData.getBiayaId());
            if (dataDetail.size() > 0){
                biayaChildAdapter newAdapter         = new biayaChildAdapter(dataDetail, objectData.getFirst());
                viewHolder.biayaView.setAdapter(newAdapter);
                viewHolder.biayaView.setNestedScrollingEnabled(false);
                viewHolder.totalView.setText("Rp. "+ Utility.currencyFormat(newAdapter.getTotal()));
            }
            if (objectData.getFirst().contentEquals("1"))
                viewHolder.layoutItem.setBackground(getResources().getDrawable(R.drawable.bg_card_first));
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView titleView, totalView;
            RecyclerView biayaView;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                titleView       = itemView.findViewById(R.id.viewTitle);
                totalView       = itemView.findViewById(R.id.viewTotal);
                biayaView       = itemView.findViewById(R.id.viewBiaya);
                biayaView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
            }
        }
    }
    class biayaChildAdapter extends RecyclerView.Adapter<biayaChildAdapter.ViewHolder> {
        private List<MstBiayaDetail> mItems;
        private String strFirst;

        private biayaChildAdapter(List<MstBiayaDetail> itemData, String isFirst) {
            super();
            mItems      = itemData;
            strFirst    = isFirst;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_biaya, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstBiayaDetail objectData   = mItems.get(position);
            viewHolder.viewDesc.setText(objectData.getDescription());
            viewHolder.viewBiaya.setText(Utility.currencyFormat(Double.valueOf(objectData.getBiaya())));
            if (strFirst.contentEquals("1")){
                viewHolder.viewDesc.setTextColor(getResources().getColor(R.color.colorTitle));
                viewHolder.viewBiaya.setTextColor(getResources().getColor(R.color.colorTitle));
            }
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        public int getTotal() {
            int total = 0;
            int countItem = mItems.size();
            for (int i = 0; i < countItem; i++) {
                MstBiayaDetail ObjItem   = mItems.get(i);
                total = total + Integer.valueOf(ObjItem.getBiaya());
            }
            return total;
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView viewDesc, viewBiaya;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                viewDesc        = itemView.findViewById(R.id.descView);
                viewBiaya       = itemView.findViewById(R.id.biayaView);
            }
        }
    }





    private void onStartAnimation(){
        overridePendingTransition(R.anim.slide_in_up, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.slide_in_down, R.anim.no_slide);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
