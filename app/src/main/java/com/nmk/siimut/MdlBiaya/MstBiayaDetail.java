package com.nmk.siimut.MdlBiaya;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstBiayaDetail {
    @SerializedName("ID")
    String DetailId;

    @SerializedName("DESCRIPTION")
    String Description;

    @SerializedName("BIAYA")
    String Biaya;

    String BiayaId;

    public MstBiayaDetail() {
    }

    public MstBiayaDetail(String detailId, String description, String biaya, String biayaId) {
        DetailId    = detailId;
        Description = description;
        Biaya       = biaya;
        BiayaId     = biayaId;
    }

    public String getDetailId() {
        return DetailId;
    }

    public void setDetailId(String detailId) {
        DetailId = detailId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getBiaya() {
        return Biaya;
    }

    public void setBiaya(String biaya) {
        Biaya = biaya;
    }

    public String getBiayaId() {
        return BiayaId;
    }

    public void setBiayaId(String biayaId) {
        BiayaId = biayaId;
    }
}