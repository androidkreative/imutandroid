package com.nmk.siimut.MdlBiaya;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface BiayaService {

    @GET(Constants.API_GET_BIAYA)
    Call<BiayaResponse> getDataList(@Query("id") String UserID);
}
