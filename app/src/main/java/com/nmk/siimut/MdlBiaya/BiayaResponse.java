package com.nmk.siimut.MdlBiaya;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class BiayaResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstBiaya> DataList    = new ArrayList<MstBiaya>();

    public List<MstBiaya> getDataList() {
        return DataList;
    }

}
