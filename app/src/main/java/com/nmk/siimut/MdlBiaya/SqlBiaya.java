package com.nmk.siimut.MdlBiaya;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlBiaya {
    private static String TABLE_BIAYA           = "MST_BIAYA";
    private static String FIELD_BIAYA_ID        = "BIAYA_ID";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";
    private static String FIELD_FAKULTAS        = "FAKULTAS";
    private static String FIELD_FIRST           = "FIRST";

    private static String TABLE_BIAYA_DETAIL    = "MST_BIAYA_DETAIL";
    private static String FIELD_DETAIL_ID       = "DETAIL_ID";
    private static String FIELD_BIAYA           = "BIAYA";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_BIAYA + "("
                + FIELD_BIAYA_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT, "
                + FIELD_FAKULTAS + " TEXT, "
                + FIELD_FIRST + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);

        String CREATE_TABLE_BIAYA_DETAIL = "CREATE TABLE " + TABLE_BIAYA_DETAIL + "("
                + FIELD_DETAIL_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT, "
                + FIELD_BIAYA + " TEXT, "
                + FIELD_BIAYA_ID + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_BIAYA_DETAIL);
    }
    public static void addDataBiaya(MstBiaya Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_BIAYA_ID, Data.getBiayaId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        values.put(FIELD_FAKULTAS, Data.getFakultas());
        values.put(FIELD_FIRST, Data.getFirst());
        db.insert(TABLE_BIAYA, null, values);
    }
    public static List<MstBiaya> getAllBiaya() {
        List<MstBiaya> DataList   = new ArrayList<MstBiaya>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_BIAYA;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstBiaya Data = new MstBiaya();
                Data.setBiayaId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setFakultas(cursor.getString(2));
                Data.setFirst(cursor.getString(3));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static List<MstBiaya> getBiayaByFakultas(String strData) {
        List<MstBiaya> DataList   = new ArrayList<MstBiaya>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_BIAYA+ " WHERE " + FIELD_FAKULTAS + " = '"+strData+"' OR " + FIELD_FAKULTAS + " = '1' ";
//        String selectQuery 			= "SELECT  * FROM " + TABLE_BIAYA+ " WHERE " + FIELD_FAKULTAS + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstBiaya Data = new MstBiaya();
                Data.setBiayaId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setFakultas(cursor.getString(2));
                Data.setFirst(cursor.getString(3));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteBiaya() {
        if (getAllBiaya().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_BIAYA);
        }
    }

    public static void addDataBiayaDetail(MstBiayaDetail Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_DETAIL_ID, Data.getDetailId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        values.put(FIELD_BIAYA, Data.getBiaya());
        values.put(FIELD_BIAYA_ID, Data.getBiayaId());
        db.insert(TABLE_BIAYA_DETAIL, null, values);
    }
    public static List<MstBiayaDetail> getAllBiayaDetail() {
        List<MstBiayaDetail> DataList   = new ArrayList<MstBiayaDetail>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_BIAYA_DETAIL;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstBiayaDetail Data = new MstBiayaDetail();
                Data.setDetailId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setBiaya(cursor.getString(2));
                Data.setBiayaId(cursor.getString(3));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static List<MstBiayaDetail> getBiayaByID(String strData) {
        List<MstBiayaDetail> DataList   = new ArrayList<MstBiayaDetail>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_BIAYA_DETAIL+ " WHERE " + FIELD_BIAYA_ID + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstBiayaDetail Data = new MstBiayaDetail();
                Data.setDetailId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setBiaya(cursor.getString(2));
                Data.setBiayaId(cursor.getString(3));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteBiayaDetail() {
        if (getAllBiayaDetail().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_BIAYA_DETAIL);
        }
    }
}