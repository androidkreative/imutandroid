package com.nmk.siimut.MdlBiaya;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstBiaya {
    @SerializedName("BIAYA_ID")
    String BiayaId;

    @SerializedName("DESCRIPTION")
    String Description;

    @SerializedName("FAKULTAS")
    String Fakultas;

    @SerializedName("IS_FIRST")
    String First;

    @SerializedName("DETAIL")
    List<MstBiayaDetail> DetailList = new ArrayList<MstBiayaDetail>();

    public MstBiaya() {
    }

    public MstBiaya(String biayaId, String description, String fakultas, String first) {
        BiayaId = biayaId;
        Description = description;
        Fakultas = fakultas;
        First = first;
    }

    public String getBiayaId() {
        return BiayaId;
    }

    public void setBiayaId(String biayaId) {
        BiayaId = biayaId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFakultas() {
        return Fakultas;
    }

    public void setFakultas(String fakultas) {
        Fakultas = fakultas;
    }

    public String getFirst() {
        return First;
    }

    public void setFirst(String first) {
        First = first;
    }

    public List<MstBiayaDetail> getDetailList() {
        return DetailList;
    }

    public void setDetailList(List<MstBiayaDetail> detailList) {
        DetailList = detailList;
    }
}