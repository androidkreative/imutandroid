package com.nmk.siimut.MdlBanner;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface BannerService {

    @GET(Constants.API_GET_BANNER)
    Call<BannerResponse> getBannerList(@Query("id") String UserID);
}
