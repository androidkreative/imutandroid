package com.nmk.siimut.MdlBanner;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstBanner {
    @SerializedName("ID")
    String BannerID;

    @SerializedName("DESCRIPTION")
    String BannerName;

    @SerializedName("IMAGE")
    String BannerUrl;

    public MstBanner() {
    }

    public MstBanner(String bannerID, String bannerName, String bannerUrl) {
        BannerID    = bannerID;
        BannerName  = bannerName;
        BannerUrl   = bannerUrl;
    }

    public String getBannerID() {
        return BannerID;
    }

    public void setBannerID(String bannerID) {
        BannerID = bannerID;
    }

    public String getBannerName() {
        return BannerName;
    }

    public void setBannerName(String bannerName) {
        BannerName = bannerName;
    }

    public String getBannerUrl() {
        return BannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        BannerUrl = bannerUrl;
    }

}