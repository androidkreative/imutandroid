package com.nmk.siimut.MdlBanner;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlBanner {
    private static String TABLE_BANNER          = "MST_BANNER";
    private static String FIELD_ID              = "BANNER_ID";
    private static String FIELD_DESCRIPTION     = "BANNER_DESC";
    private static String FIELD_IMAGE           = "BANNER_IMAGE";

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_BANNER + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT, "
                + FIELD_IMAGE + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);
    }
    public static void addData(MstBanner Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getBannerID());
        values.put(FIELD_DESCRIPTION, Data.getBannerName());
        values.put(FIELD_IMAGE, Data.getBannerUrl());
        db.insert(TABLE_BANNER, null, values);
    }
    public static List<MstBanner> getAllData() {
        List<MstBanner> DataList   = new ArrayList<MstBanner>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_BANNER;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstBanner Data = new MstBanner();
                Data.setBannerID(cursor.getString(0));
                Data.setBannerName(cursor.getString(1));
                Data.setBannerUrl(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteData() {
        if (getAllData().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_BANNER);
        }
    }
}