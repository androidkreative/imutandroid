package com.nmk.siimut.MdlBanner;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class BannerResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstBanner> DataList    = new ArrayList<MstBanner>();

    public List<MstBanner> getDataList() {
        return DataList;
    }
}
