package com.nmk.siimut;

import android.app.Application;

/**
 * Created by Warsono on 10/03/2019.
 */

public class GlobalApplication extends Application {
    private static android.app.Activity Activity;
    private static android.content.Context Context;

    @Override
    public void onCreate() {
        super.onCreate();
        Context    = getApplicationContext();
    }

    public static android.app.Activity getActivity(){
        return Activity;
    }

    public static android.content.Context getContext(){
        return Context;
    }
}
