package com.nmk.siimut.MdlAccount;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.MstUserProfile;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.RoundedSide;
import com.nmk.siimut.Utilities.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Warsono on 01/07/2019.
 */

public class FragmentAccount extends Fragment{

    private TextView viewName, viewEmail, viewVersion;
    private RelativeLayout accountLayout;
    private LinearLayout emptyLayout, buttonSignOut;
    private CircleImageView viewPhoto;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup RootView      = (ViewGroup) inflater.inflate(R.layout.fragment_account, null);
        viewPhoto               = RootView.findViewById(R.id.photoView);
        viewName                = RootView.findViewById(R.id.nameView);
        viewEmail               = RootView.findViewById(R.id.emailView);
        emptyLayout             = RootView.findViewById(R.id.layoutEmptyAccount);
        accountLayout           = RootView.findViewById(R.id.layoutAccount);
        buttonSignOut           = RootView.findViewById(R.id.btnSignOut);
        viewVersion             = RootView.findViewById(R.id.versionView);

        onSetVersion();
        onSetProfile();

        return RootView;
    }
    private void onSetVersion(){
        String versionName = Constants.VERSION_DEFAULT;
        try {
            PackageInfo pInfo = GlobalApplication.getContext().getPackageManager().getPackageInfo(GlobalApplication.getContext().getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {}
        viewVersion.setText("v"+versionName);
    }
    private void onSetProfile(){
        List<MstUser> dataList = SqlUser.getAllData();
        if (dataList.size() > 0){
            MstUser objUser = dataList.get(0);
            viewName.setText(objUser.getCompleteName());
            viewEmail.setText(objUser.getEmail());
            emptyLayout.setVisibility(View.GONE);
            accountLayout.setVisibility(View.VISIBLE);
            buttonSignOut.setVisibility(View.VISIBLE);

            if (objUser.getImage() != null && !objUser.getImage().contentEquals("")){
                Picasso.get()
                        .load(Uri.parse(objUser.getImage()))
                        .error(getResources().getDrawable(R.drawable.avatar))
                        .into(viewPhoto);
            }
        }else{
            emptyLayout.setVisibility(View.VISIBLE);
            accountLayout.setVisibility(View.GONE);
            buttonSignOut.setVisibility(View.GONE);
        }
    }
}
