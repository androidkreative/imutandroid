package com.nmk.siimut.MdlAccount.User;

import com.nmk.siimut.ApiService.BasicResponse;
import com.nmk.siimut.Utilities.Constants;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface UserService {

    @FormUrlEncoded
    @POST(Constants.API_SIGN_UP)
    Call<BasicResponse> postSignUp(
            @Field("email") String Email,
            @Field("password") String Password,
            @Field("status") String Status,
            @Field("nim") String NIM);

    @FormUrlEncoded
    @POST(Constants.API_SIGN_IN)
    Call<UserResponse> postSignIn(
            @Field("email") String Email,
            @Field("password") String Password,
            @Field("type") String Type,
            @Field("sosmed") String SosmedID,
            @Field("token") String Token);

    @FormUrlEncoded
    @POST(Constants.API_CHANGE_PASSWORD)
    Call<BasicResponse> postChangePassword(
            @Field("id") String UserId,
            @Field("auth") String UserAuth,
            @Field("password") String OldPassword,
            @Field("new_password") String NewPassword);

    @GET(Constants.API_USER_ACCOUNT)
    Call<UserResponse> getUserAccount(
            @Query("id") String UserID,
            @Query("auth") String UserAuth);

    @GET(Constants.API_USER_PROFILE)
    Call<UserProfileResponse> getUserProfile(
            @Query("id") String UserID,
            @Query("auth") String UserAuth);

    @FormUrlEncoded
    @POST(Constants.API_UPDATE_PROFILE)
    Call<BasicResponse> postUpdateProfile(
            @Field("id") String UserId,
            @Field("auth") String UserAuth,
            @Field("profile") String JsonProfile);

    @Multipart
    @POST(Constants.API_UPLOAD_PROFILE)
    Call<BasicResponse> postUploadProfile(
            @PartMap Map<String, RequestBody> params);

    @FormUrlEncoded
    @POST(Constants.API_INPUT_TOKEN)
    Call<BasicResponse> postInputToken(
            @Field("id") String UserId,
            @Field("auth") String UserAuth,
            @Field("token") String Token);

    @FormUrlEncoded
    @POST(Constants.API_REMOVE_TOKEN)
    Call<BasicResponse> postRemoveToken(
            @Field("id") String UserId,
            @Field("auth") String UserAuth);
}
