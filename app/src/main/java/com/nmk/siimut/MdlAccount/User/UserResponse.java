package com.nmk.siimut.MdlAccount.User;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

/**
 * Created by Warsono on 01/07/2019.
 */

public class UserResponse extends BasicResponse {

    @SerializedName("DATA")
    MstUser Data;

    public MstUser getData() {
        return Data;
    }
}