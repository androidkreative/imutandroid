package com.nmk.siimut.MdlAccount.User;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class UserProfileResponse extends BasicResponse {

    @SerializedName("DATA")
    MstUserProfile Data;

    public MstUserProfile getData() {
        return Data;
    }
}
