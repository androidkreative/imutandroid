package com.nmk.siimut.MdlAccount.User;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nmk.siimut.Fcm.Config;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.MdlOther.MstAjaran;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.MdlPendaftaran.RegMahasiswaActivity;
import com.nmk.siimut.MdlPendaftaran.RegOneActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;

import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    //FOR LOGIN GOOGLE
    private FirebaseAuth mAuth;
    private GoogleSignInClient googleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private String sosmedID, sosmedToken, sosmedEmail;

    private EditText inputEmail, inputPassword;
    private RelativeLayout loadingMain;
    private String strUser, strPassword;
    private String strView;

    @Override
    public void onBackPressed(){
        Intent intentMain = new Intent(SignInActivity.this, MainActivity.class);
        intentMain.putExtra(Constants.VIEW_KEY, strView);
        startActivity(intentMain);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        mAuth           = FirebaseAuth.getInstance();
        initialiseGoogleSignin();

        inputEmail      = findViewById(R.id.emailInput);
        inputPassword   = findViewById(R.id.passwordInput);
        loadingMain     = findViewById(R.id.mainLoading);
        inputPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        onClickSignIn();
                    }
                }
                return false;
            }
        });
        if (getIntent().getStringExtra(Constants.VIEW_KEY) != null )
            strView = getIntent().getStringExtra(Constants.VIEW_KEY);
    }
    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnSignIn:
                onClickSignIn();
                break;
            case R.id.btnForgot:
                break;
            case R.id.btnSignUp:
                goToSignUp();
                break;
            case R.id.btnAdd:
                goToSignUp();
                break;
            case R.id.btnFacebook:
                break;
            case R.id.btnGoogle:
                if (Utility.isNetworkConnected()) {
                    onShowLoading.run();
                    signInWithGoogle();
                }else
                    Dialog.InformationDialog(SignInActivity.this,
                            getResources().getString(R.string.label_warning),
                            getResources().getString(R.string.msg_conection_unavailable),
                            getResources().getString(R.string.btn_close),
                            null);
                break;
        }
    }
    private void goToSignUp(){
        Intent intentSignUp = new Intent(SignInActivity.this, SignUpChooseActivity.class);
        intentSignUp.putExtra(Constants.VIEW_KEY, strView);
        startActivity(intentSignUp);
    }
    public boolean validateData() {
        boolean valid   = true;
        strUser         = inputEmail.getText().toString();
        strPassword     = inputPassword.getText().toString();

        if (strUser.isEmpty() || strUser.length() == 0 ) {
            inputEmail.setError(getString(R.string.msg_empty_user));
            inputEmail.requestFocus();
            valid = false;
        } else {
            inputEmail.setError(null);
            if (strPassword.isEmpty() || strPassword.length() == 0 ) {
                inputPassword.setError(getString(R.string.msg_empty_password));
                inputPassword.requestFocus();
                valid = false;
            } else
                inputPassword.setError(null);
        }
        return valid;
    }
    private void onClickSignIn(){
        if (validateData()){
            if (Utility.isNetworkConnected()) {
                onShowLoading.run();
                UserTask.onLogin(SignInActivity.this, strUser, strPassword, Constants.SOSMED_DEFAULT, "", "", postToken, onHideLoading);
            }else
                Dialog.InformationDialog(SignInActivity.this,
                        getResources().getString(R.string.label_warning),
                        getResources().getString(R.string.msg_conection_unavailable),
                        getResources().getString(R.string.btn_close),
                        null);
        }
    }
    //FOR LOGIN GOOGLE
    private void initialiseGoogleSignin(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getResources().getString(R.string.server_client_id))
                .build();
        googleApiClient = GoogleSignIn.getClient(this, gso);
        googleApiClient.signOut();
    }
    private void signInWithGoogle() {
        Intent signInIntent = googleApiClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            onShowLoading.run();
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Dialog.InformationDialog(SignInActivity.this,
                        getResources().getString(R.string.label_warning),
                        getResources().getString(R.string.msg_google_failed),
                        getResources().getString(R.string.btn_close),
                        onHideLoading);
            }
        }
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
//                    FirebaseUser user       = mAuth.getCurrentUser();
                    sosmedID                = acct.getId();
                    sosmedToken             = acct.getIdToken();
                    sosmedEmail             = acct.getEmail();
                    UserTask.onLogin(
                            SignInActivity.this,
                            sosmedEmail,
                            "",
                            Constants.SOSMED_GOOGLE,
                            sosmedID,
                            sosmedToken,
                            postToken,
                            onHideLoading);
                } else {
                    Dialog.InformationDialog(SignInActivity.this,
                            getResources().getString(R.string.label_warning),
                            getResources().getString(R.string.msg_google_failed),
                            getResources().getString(R.string.btn_close),
                            onHideLoading);
                }
            }
        });
    }
    private Runnable postToken = new Runnable() {
        @Override
        public void run() {
            SharedPreferences pref  = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
            String FcmToken         = pref.getString(Config.FCM_TOKEN, "");
            UserTask.postUserToken(SignInActivity.this, FcmToken, onGetProfile, onGetProfile);

            //CREATE AND JOIN TOPIC
            List<MstUser> dataList  = SqlUser.getAllData();
            MstUser objUser         = dataList.get(0);
            if (objUser.getUserType().contentEquals("3"))
                FirebaseMessaging.getInstance().unsubscribeFromTopic("global");
            else
                FirebaseMessaging.getInstance().subscribeToTopic("global");
        }
    };
    private Runnable onGetProfile = new Runnable() {
        @Override
        public void run() {
            UserTask.onGetProfile(SignInActivity.this, onLoginSuccess, onLoginSuccess);
        }
    };
    private Runnable onLoginSuccess = new Runnable() {
        @Override
        public void run() {
            switch (strView) {
                case Constants.VIEW_PMB:
                    List<MstUser> dataList = SqlUser.getAllData();
                    if (dataList.size() > 0) {
                        MstUser objUser                     = dataList.get(0);
                        List<MstUserProfile> dataProfile    = SqlUser.getAllDataProfile();
                        switch (objUser.getUserType()) {
                            case "0":           //UMUM
                                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                break;
                            case "1":           //CALON MAHASISWA
                                List<MstAjaran> dataAjaran = SqlOther.getAllAjaran();
                                if (dataAjaran.size() > 0) {
                                    MstAjaran objAjaran = dataAjaran.get(0);
                                    if (objAjaran.getStatus().contentEquals("1")) {
                                        if (dataProfile.size() > 0){
                                            if (dataProfile.get(0).getId() != null && !dataProfile.get(0).getId().contentEquals(""))
                                                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                            else
                                                startActivity(new Intent(SignInActivity.this, RegOneActivity.class));
                                        }else
                                            startActivity(new Intent(SignInActivity.this, RegOneActivity.class));
                                    }else
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                }else
                                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                break;
                            case "2":           //MAHASISWA
                                if (dataProfile.size() > 0){
                                    if (dataProfile.get(0).getJurusan().contentEquals("0"))
                                        startActivity(new Intent(SignInActivity.this, RegMahasiswaActivity.class));
                                    else
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                }else
                                    startActivity(new Intent(SignInActivity.this, RegMahasiswaActivity.class));
                                break;
                            case "3":           //ADMIN
                                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                break;
                        }
                    }else
                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    break;
                default:
                    Intent intentlogin = new Intent(SignInActivity.this, MainActivity.class);
                    intentlogin.putExtra(Constants.VIEW_KEY, strView);
                    startActivity(intentlogin);
                    break;
            }
        }
    };
    private Runnable onShowLoading = new Runnable() {
        @Override
        public void run() {
            inputEmail.setEnabled(false);
            inputPassword.setEnabled(false);
            loadingMain.setVisibility(View.VISIBLE);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
            inputEmail.setEnabled(true);
            inputPassword.setEnabled(true);
            loadingMain.setVisibility(View.GONE);
        }
    };




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}