package com.nmk.siimut.MdlAccount.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlFakultas.MstJurusan;
import com.nmk.siimut.MdlFakultas.SqlFakultas;
import com.nmk.siimut.MdlFasilitas.MstFasilitas;
import com.nmk.siimut.MdlFasilitas.SqlFasilitas;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.MdlOther.MstAgama;
import com.nmk.siimut.MdlOther.MstAjaran;
import com.nmk.siimut.MdlOther.MstStatus;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.MdlPendaftaran.RegOneActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class ProfileDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private MstUser objUser;
    private List<MstUserProfile> dataProfile;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(ProfileDetailActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_ACCOUNT);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

//        ImageView buttonBack        = findViewById(R.id.btnBack);
//        buttonBack.setOnClickListener(this);
        CircleImageView viewPhoto   = findViewById(R.id.photoView);
        TextView viewName           = findViewById(R.id.nameView);
        TextView viewEmail          = findViewById(R.id.emailView);
        TextView viewId             = findViewById(R.id.idView);
        TextView viewPanggilan      = findViewById(R.id.panggilanView);
        TextView viewJurusan        = findViewById(R.id.jurusanView);
        TextView viewStatus         = findViewById(R.id.statusView);
        TextView viewAgama          = findViewById(R.id.agamaView);
        TextView viewJob            = findViewById(R.id.jobView);
        TextView viewLahir          = findViewById(R.id.lahirView);
        TextView viewAlamatKtp      = findViewById(R.id.alamatKtpView);
        TextView viewAlamatSurat    = findViewById(R.id.alamatSuratView);
        TextView viewPhone          = findViewById(R.id.phoneView);
        TextView viewHp             = findViewById(R.id.hpView);
        TextView buttonEdit         = findViewById(R.id.btnEdit);
        LinearLayout profileLayout  = findViewById(R.id.profileLayout);

        List<MstUser> dataList  = SqlUser.getAllData();
        objUser                 = dataList.get(0);
        if (objUser.getImage() != null && !objUser.getImage().contentEquals("")){
            Picasso.get()
                    .load(Uri.parse(objUser.getImage()))
                    .error(getResources().getDrawable(R.drawable.avatar))
                    .into(viewPhoto);
        }

        dataProfile = SqlUser.getAllDataProfile();
        if (dataProfile.size() > 0){
            MstUserProfile objProfile = dataProfile.get(0);
            viewName.setText(objProfile.getNamaLengkap());
            viewEmail.setText(objProfile.getEmail());

            switch (objUser.getUserType()) {
                case "0":           //UMUM
                    viewId.setVisibility(View.GONE);
                    profileLayout.setVisibility(View.GONE);
                    break;
                case "1":           //CALON MAHASISWA
                    viewId.setVisibility(View.VISIBLE);
                    viewId.setText("Reg. No "+objProfile.getId());
                    if (dataProfile.size() == 0){
                        List<MstAjaran> dataAjaran = SqlOther.getAllAjaran();
                        if (dataAjaran.size() > 0) {
                            MstAjaran objAjaran = dataAjaran.get(0);
                            if (objAjaran.getStatus().contentEquals("1"))
                                buttonEdit.setVisibility(View.VISIBLE);
                            else
                                buttonEdit.setVisibility(View.GONE);
                        }else
                            buttonEdit.setVisibility(View.GONE);
                    }else
                        buttonEdit.setVisibility(View.VISIBLE);
                    break;
                case "2":           //MAHASISWA
                    viewId.setVisibility(View.VISIBLE);
                    viewId.setText("NIM "+objProfile.getId());
                    break;
                case "3":           //ADMIN
                    viewId.setVisibility(View.GONE);
                    profileLayout.setVisibility(View.GONE);
                    buttonEdit.setVisibility(View.GONE);
                    break;
            }
            viewPanggilan.setText(objProfile.getNamaPanggilan());

            MstJurusan objJurusan   = SqlFakultas.getJurusanById(objProfile.getJurusan());
            viewJurusan.setText(objJurusan.getJurusanName());

            if (objProfile.getStatusNikah() != null && !objProfile.getStatusNikah().contentEquals("0")){
                MstStatus objStatus     = SqlOther.getStatusById(objProfile.getStatusNikah());
                viewStatus.setText(objStatus.getStatusName());
            }else
                viewStatus.setText("-");

            if (objProfile.getAgama() != null && !objProfile.getAgama().contentEquals("0")){
                MstAgama objAgama       = SqlOther.getAgamaById(objProfile.getAgama());
                viewAgama.setText(objAgama.getAgamaName());
            }else
                viewAgama.setText("-");

            if (objProfile.getPekerjaan() != null && !objProfile.getPekerjaan().contentEquals(""))
                viewJob.setText(objProfile.getPekerjaan());
            else
                viewJob.setText("-");

            if (objProfile.getTempatLahir() != null && !objProfile.getTempatLahir().contentEquals(""))
                viewLahir.setText(objProfile.getTempatLahir()+", "+Utility.DateView(objProfile.getTanggalLahir()));
            else
                viewLahir.setText("-");

            if (objProfile.getAlamatKTP() != null && !objProfile.getAlamatKTP().contentEquals(""))
                viewAlamatKtp.setText(objProfile.getAlamatKTP()+", "+objProfile.getPostKTP());
            else
                viewAlamatKtp.setText("-");

            if (objProfile.getAlamatSurat() != null && !objProfile.getAlamatSurat().contentEquals(""))
                viewAlamatSurat.setText(objProfile.getAlamatSurat()+", "+objProfile.getPostSurat());
            else
                viewAlamatSurat.setText("-");

            if (objProfile.getPhone() != null && !objProfile.getPhone().contentEquals(""))
                viewPhone.setText(objProfile.getPhone());
            else
                viewPhone.setText("-");

            if (objProfile.getHp() != null && !objProfile.getHp().contentEquals(""))
                viewHp.setText(objProfile.getHp());
            else
                viewHp.setText("-");
        }else{
            viewId.setText("");
            viewId.setVisibility(View.GONE);
            viewName.setText(objUser.getCompleteName());
            viewEmail.setText(objUser.getEmail());
            viewPanggilan.setText("");
            viewPanggilan.setVisibility(View.GONE);
            viewJurusan.setText("");
            viewJurusan.setVisibility(View.GONE);
            viewStatus.setText("-");
            viewAgama.setText("-");
            viewJob.setText("-");
            viewLahir.setText("-");
            viewAlamatKtp.setText("-");
            viewAlamatSurat.setText("-");
            viewPhone.setText("-");
            viewHp.setText("-");
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnEdit:
                Intent intent;
                if (objUser.getUserType().contentEquals("1") && dataProfile.size() == 0)
                    intent   = new Intent(ProfileDetailActivity.this, RegOneActivity.class);
                else
                    intent   = new Intent(ProfileDetailActivity.this, ProfileEditActivity.class);
                startActivity(intent);
                break;
            case R.id.photoView:
                Intent intentPhoto   = new Intent(ProfileDetailActivity.this, ProfilePhotoActivity.class);
                startActivity(intentPhoto);
                break;
            case R.id.btnChangePfhoto:
                Intent intentImage   = new Intent(ProfileDetailActivity.this, ProfilePhotoActivity.class);
                startActivity(intentImage);
                break;
        }
    }





    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
