package com.nmk.siimut.MdlAccount.User;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstUser {
    @SerializedName("USER_ID")
    String UserId;

    @SerializedName("USER_AUTH")
    String UserAuth;

    @SerializedName("EMAIL")
    String Email;

    @SerializedName("COMPLETE_NAME")
    String CompleteName;

    @SerializedName("USER_TYPE")
    String UserType;

    @SerializedName("SOSMED_TYPE")
    String SosmedType;

    @SerializedName("IMAGE")
    String Image;

    public MstUser() {
    }

    public MstUser(String userId, String userAuth, String email, String completeName, String userType, String sosmedType, String image) {
        UserId = userId;
        UserAuth = userAuth;
        Email = email;
        CompleteName = completeName;
        UserType = userType;
        SosmedType = sosmedType;
        Image = image;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserAuth() {
        return UserAuth;
    }

    public void setUserAuth(String userAuth) {
        UserAuth = userAuth;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCompleteName() {
        return CompleteName;
    }

    public void setCompleteName(String completeName) {
        CompleteName = completeName;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getSosmedType() {
        return SosmedType;
    }

    public void setSosmedType(String sosmedType) {
        SosmedType = sosmedType;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}