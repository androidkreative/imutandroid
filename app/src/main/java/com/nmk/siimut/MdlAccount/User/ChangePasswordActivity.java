package com.nmk.siimut.MdlAccount.User;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Warsono on 01/07/2019.
 */

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

//    private ImageView buttonBack;
    private CircleImageView viewPhoto;
    private EditText inputOldPassword, inputNewPassword, inputRetype;
    private TextView buttonChange;
    private RelativeLayout loadingMain;
    private String strOldPassword;
    private String strNewPassword;

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
        intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_ACCOUNT);
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        TextView viewName   = findViewById(R.id.nameView);
        TextView viewEmail  = findViewById(R.id.emailView);
        viewPhoto           = findViewById(R.id.photoView);
//        buttonBack          = findViewById(R.id.btnBack);
        buttonChange        = findViewById(R.id.btnSubmit);
        inputOldPassword    = findViewById(R.id.inputOldPassword);
        inputNewPassword    = findViewById(R.id.inputNewPassword);
        inputRetype         = findViewById(R.id.inputRetypePassword);
        loadingMain         = findViewById(R.id.mainLoading);
        inputRetype.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        onClickChange();
                    }
                }
                return false;
            }
        });
        List<MstUser> dataList  = SqlUser.getAllData();
        MstUser objUser         = dataList.get(0);
        if (objUser.getImage() != null && !objUser.getImage().contentEquals("")){
            Picasso.get()
                    .load(Uri.parse(objUser.getImage()))
                    .error(getResources().getDrawable(R.drawable.avatar))
                    .into(viewPhoto);
        }

        List<MstUserProfile> dataProfile = SqlUser.getAllDataProfile();
        if (dataProfile.size() > 0){
            MstUserProfile objProfile = dataProfile.get(0);
            viewName.setText(objProfile.getNamaLengkap());
            viewEmail.setText(objProfile.getEmail());
        }else{
            viewName.setText(objUser.getCompleteName());
            viewEmail.setText(objUser.getEmail());
        }
    }
    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnSubmit:
                onClickChange();
                break;
        }
    }
    public boolean validateData() {
        boolean valid       = true;
        strOldPassword      = inputOldPassword.getText().toString();
        strNewPassword      = inputNewPassword.getText().toString();
        String strRetype    = inputRetype.getText().toString();

        if (strOldPassword.isEmpty() || strOldPassword.length() == 0 ) {
            inputOldPassword.setError(getString(R.string.msg_empty_password));
            valid = false;
        } else {
            inputOldPassword.setError(null);
            if (strNewPassword.isEmpty() || strNewPassword.length() == 0 ) {
                inputNewPassword.setError(getString(R.string.msg_empty_password));
                valid = false;
            } else {
                inputNewPassword.setError(null);
                if (strRetype.isEmpty() || strRetype.length() == 0 ) {
                    inputRetype.setError(getString(R.string.msg_empty_password));
                    valid = false;
                } else {
                    if (!strNewPassword.contentEquals(strRetype)){
                        inputRetype.setError(getString(R.string.msg_retype_notmatch));
                        valid = false;
                    }else
                        inputRetype.setError(null);
                }
            }
        }
        return valid;
    }
    private void onClickChange(){
        if (validateData()){
            if (Utility.isNetworkConnected()) {
                onShowLoading.run();
                UserTask.onChangePassword(ChangePasswordActivity.this, strOldPassword, strNewPassword, onChangeSuccess, onHideLoading);
            }else
                Dialog.InformationDialog(ChangePasswordActivity.this,
                        getResources().getString(R.string.label_warning),
                        getResources().getString(R.string.msg_conection_unavailable),
                        getResources().getString(R.string.btn_close),
                        null);
        }
    }
    private Runnable onChangeSuccess = new Runnable() {
        @Override
        public void run() {
            Utility.onUserLogout(ChangePasswordActivity.this);
            Intent intent = new Intent(ChangePasswordActivity.this, SignInActivity.class);
            startActivity(intent);
        }
    };
    private Runnable onShowLoading = new Runnable() {
        @Override
        public void run() {
//            buttonBack.setEnabled(false);
            inputOldPassword.setEnabled(false);
            inputNewPassword.setEnabled(false);
            inputRetype.setEnabled(false);
            buttonChange.setEnabled(false);
            loadingMain.setVisibility(View.VISIBLE);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
//            buttonBack.setEnabled(true);
            inputOldPassword.setEnabled(true);
            inputNewPassword.setEnabled(true);
            inputRetype.setEnabled(true);
            buttonChange.setEnabled(true);
            loadingMain.setVisibility(View.GONE);
        }
    };

    


    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}