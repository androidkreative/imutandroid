package com.nmk.siimut.MdlAccount.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.isseiaoki.simplecropview.CropImageView;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.ImageLoadedCallback;
import com.nmk.siimut.Utilities.Utility;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class ProfilePhotoActivity extends AppCompatActivity implements IPickResult, View.OnClickListener{

    private CropImageView profileImageView;
    private ProgressBar mProgressBar;
    private Bitmap croppingBitmap;
    private RelativeLayout loadingMain;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(ProfilePhotoActivity.this, ProfileDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_photo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        profileImageView    = findViewById(R.id.profileImage);
        mProgressBar        = findViewById(R.id.progressBar);
        loadingMain         = findViewById(R.id.mainLoading);

        List<MstUser> dataList  = SqlUser.getAllData();
        MstUser objUser                 = dataList.get(0);
        if (objUser.getImage() != null && !objUser.getImage().contentEquals("")){
            Picasso.get()
                    .load(Uri.parse(objUser.getImage()))
                    .error(getResources().getDrawable(R.drawable.avatar))
                    .into(profileImageView, new ImageLoadedCallback(mProgressBar) {
                        @Override
                        public void onSuccess() {
                            mProgressBar.setVisibility(View.GONE);
                            if (this.progressBar != null) {
                                this.progressBar.setVisibility(View.GONE);
                            }
                        }
                        public void onError(Exception e) {
                            mProgressBar.setVisibility(View.GONE);
                            PickImageDialog.build(new PickSetup()
                                    .setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL)
                            ).show(ProfilePhotoActivity.this);
                        }
                    });
        }else{
            mProgressBar.setVisibility(View.GONE);
            PickImageDialog.build(new PickSetup()
                    .setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL)
            ).show(ProfilePhotoActivity.this);
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnLibrary:
                PickImageDialog.build(new PickSetup()
                        .setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL)
                ).show(ProfilePhotoActivity.this);
                break;
            case R.id.btnRotateLeft:
                profileImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
                break;
            case R.id.btnRotateRight:
                profileImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
                break;
            case R.id.btnDone:
                if (Utility.isNetworkConnected()) {
                    onShowLoading.run();
                    profileImageView.cropAsSingle();
                    croppingBitmap  = profileImageView.getCroppedBitmap();
                    profileImageView.setImageBitmap(profileImageView.getCroppedBitmap());
                    onUploadProgress.run();
                }else
                    Dialog.InformationDialog(ProfilePhotoActivity.this,
                            getResources().getString(R.string.label_warning),
                            getResources().getString(R.string.msg_conection_unavailable),
                            getResources().getString(R.string.btn_close),
                            null);
                break;
        }
    }
    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            profileImageView.setImageBitmap(pickResult.getBitmap());
        }else
            Toast.makeText(ProfilePhotoActivity.this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
    }
    private Runnable onUploadProgress = new Runnable() {
        @Override
        public void run() {
            ByteArrayOutputStream bos   = new ByteArrayOutputStream();
            croppingBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            byte[] mBitmapData      = bos.toByteArray();
            UserTask.postPhotoProfile(ProfilePhotoActivity.this, mBitmapData, onGetAccount, onHideLoading);
        }
    };
    private Runnable onGetAccount = new Runnable() {
        @Override
        public void run() {
            UserTask.onGetAccount(ProfilePhotoActivity.this, onUpdateSuccess, onUpdateSuccess);
        }
    };
    private Runnable onUpdateSuccess = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(ProfilePhotoActivity.this, ProfileDetailActivity.class);
            startActivity(intent);
        }
    };
    private Runnable onShowLoading = new Runnable() {
        @Override
        public void run() {
            loadingMain.setVisibility(View.VISIBLE);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
            loadingMain.setVisibility(View.GONE);
        }
    };




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
