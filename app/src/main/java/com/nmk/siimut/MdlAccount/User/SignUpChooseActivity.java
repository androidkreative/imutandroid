package com.nmk.siimut.MdlAccount.User;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;

/**
 * Created by Warsono on 01/07/2019.
 */

public class SignUpChooseActivity extends AppCompatActivity implements View.OnClickListener {

    private String strView;

    @Override
    public void onBackPressed(){
        Intent intentMain = new Intent(SignUpChooseActivity.this, SignInActivity.class);
        intentMain.putExtra(Constants.VIEW_KEY, strView);
        startActivity(intentMain);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_choose);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        if (getIntent().getStringExtra(Constants.VIEW_KEY) != null )
            strView = getIntent().getStringExtra(Constants.VIEW_KEY);
    }
    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnCalon:
                Intent intentCalon = new Intent(SignUpChooseActivity.this, SignUpActivity.class);
                intentCalon.putExtra(Constants.VIEW_KEY, strView);
                intentCalon.putExtra(Constants.PARAMS_KEY, Constants.CALON_KEY);
                startActivity(intentCalon);
                break;
            case R.id.btnMahasiswa:
                Intent intentMhs = new Intent(SignUpChooseActivity.this, SignUpActivity.class);
                intentMhs.putExtra(Constants.VIEW_KEY, strView);
                intentMhs.putExtra(Constants.PARAMS_KEY, Constants.MAHASISWA_KEY);
                startActivity(intentMhs);
                break;
        }
    }




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}