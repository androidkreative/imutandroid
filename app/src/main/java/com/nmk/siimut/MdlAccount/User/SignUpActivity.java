package com.nmk.siimut.MdlAccount.User;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;

/**
 * Created by Warsono on 01/07/2019.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText inputEmail, inputPassword, inputNomor;
    private RelativeLayout loadingMain;
    private String strUser, strPassword;
    private String strNomor = "";
    private FirebaseAuth mAuth;
    private String strView, strStatus;
    private LinearLayout layoutNomor;

    @Override
    public void onBackPressed(){
        Intent intentMain = new Intent(SignUpActivity.this, SignInActivity.class);
        intentMain.putExtra(Constants.VIEW_KEY, strView);
        startActivity(intentMain);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        mAuth           = FirebaseAuth.getInstance();

        inputEmail      = findViewById(R.id.emailInput);
        inputPassword   = findViewById(R.id.passwordInput);
        layoutNomor     = findViewById(R.id.nomorLayout);
        inputNomor      = findViewById(R.id.nomorInput);
        loadingMain     = findViewById(R.id.mainLoading);
        inputPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        if (!strStatus.contentEquals(Constants.MAHASISWA_KEY))
                            onClickSignUp();
                    }
                }
                return false;
            }
        });
        inputNomor.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        onClickSignUp();
                    }
                }
                return false;
            }
        });
        if (getIntent().getStringExtra(Constants.VIEW_KEY) != null )
            strView     = getIntent().getStringExtra(Constants.VIEW_KEY);
        if (getIntent().getStringExtra(Constants.PARAMS_KEY) != null )
            strStatus   = getIntent().getStringExtra(Constants.PARAMS_KEY);
        if (strStatus.contentEquals(Constants.MAHASISWA_KEY))
            layoutNomor.setVisibility(View.VISIBLE);
    }
    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnSignIn:
                onBackPressed();
                break;
            case R.id.btnSignUp:
                onClickSignUp();
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnFacebook:
                break;
            case R.id.btnGoogle:
                break;
        }
    }
    public boolean validateData() {
        boolean valid   = true;
        strUser         = inputEmail.getText().toString();
        strPassword     = inputPassword.getText().toString();
        strNomor        = inputNomor.getText().toString();

        if (strUser.isEmpty() || strUser.length() == 0 ) {
            inputEmail.setError(getString(R.string.msg_empty_user));
            inputEmail.requestFocus();
            valid = false;
        } else {
            inputEmail.setError(null);
            if (strPassword.isEmpty() || strPassword.length() == 0 ) {
                inputPassword.setError(getString(R.string.msg_empty_password));
                inputPassword.requestFocus();
                valid = false;
            } else {
                inputPassword.setError(null);
                if (strStatus.contentEquals(Constants.MAHASISWA_KEY)){
                    if (strNomor.isEmpty() || strNomor.length() == 0 ) {
                        inputNomor.setError(getString(R.string.msg_empty_nim));
                        inputNomor.requestFocus();
                        valid = false;
                    } else
                        inputNomor.setError(null);
                }
            }
        }
        return valid;
    }
    private void onClickSignUp(){
        if (validateData()){
            if (Utility.isNetworkConnected()) {
                onShowLoading.run();
                UserTask.onSignUp(SignUpActivity.this, strUser, strPassword, strStatus, strNomor, onRegisSuccess, onHideLoading);
            }else
                Dialog.InformationDialog(SignUpActivity.this,
                        getResources().getString(R.string.label_warning),
                        getResources().getString(R.string.msg_conection_unavailable),
                        getResources().getString(R.string.btn_close),
                        null);
        }
    }
    private Runnable onRegisSuccess = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
            intent.putExtra(Constants.VIEW_KEY, strView);
            startActivity(intent);
        }
    };
    private Runnable onShowLoading = new Runnable() {
        @Override
        public void run() {
            inputEmail.setEnabled(false);
            inputPassword.setEnabled(false);
            loadingMain.setVisibility(View.VISIBLE);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
            inputEmail.setEnabled(true);
            inputPassword.setEnabled(true);
            loadingMain.setVisibility(View.GONE);
        }
    };




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}