package com.nmk.siimut.MdlAccount.User;


import android.app.Activity;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.ApiService.BasicResponse;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.FancyToast;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;

import java.io.File;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

public class UserTask {
    private static String strUserId     = "";
    private static String strUserAuth   = "";

    public static void onSignUp(final Activity mContext, String strEmail, String strPassword, String strStatus, String strNomor, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<BasicResponse> mRequest    = service.postSignUp(strEmail, strPassword, strStatus, strNomor);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        default:
                            FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
//                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
//                        if (FailedAction != null)
//                            FailedAction.run();
                        FancyToast.makeMessage(mContext, FancyToast.SUCCESS, "Registrasi berhasil silahkan masuk", Toast.LENGTH_SHORT).show();
                        if (SuccessAction != null)
                            SuccessAction.run();
                    }
                }
            });
        }catch (Exception e) {
//            FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
//            if (FailedAction != null)
//                FailedAction.run();
            if (SuccessAction != null)
                SuccessAction.run();
        }
    }
    public static void onLogin(final Activity mContext, String strEmail, String strPassword, String strType, String strSosmedID, String strToken, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<UserResponse> mRequest     = service.postSignIn(strEmail, strPassword, strType, strSosmedID, strToken);
            mRequest.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> Response) {
                    UserResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            MstUser objData = Result.getData();
                            SqlUser.DeleteData();
                            SqlUser.addData(objData);
                            FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        default:
                            FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void onGetAccount(final Activity mContext, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList = SqlUser.getAllData();
            if (dataList.size() > 0) {
                MstUser data    = dataList.get(0);
                strUserId       = data.getUserId();
                strUserAuth     = data.getUserAuth();
            }

            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<UserResponse> mRequest     = service.getUserAccount(strUserId, strUserAuth);
            mRequest.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> Response) {
                    UserResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            MstUser objData = Result.getData();
                            SqlUser.DeleteData();
                            SqlUser.addData(objData);
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        case 202:
                            Dialog.ConfirmationDialog(mContext, mContext.getResources().getString(R.string.label_information),
                                    Result.getResultMessage(), expiredAuth(mContext), null);
                            break;
                        default:
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void onGetProfile(final Activity mContext, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList = SqlUser.getAllData();
            if (dataList.size() > 0) {
                MstUser data    = dataList.get(0);
                strUserId       = data.getUserId();
                strUserAuth     = data.getUserAuth();
            }

            UserService service                 = ApiClient.getClient().create(UserService.class);
            Call<UserProfileResponse> mRequest  = service.getUserProfile(strUserId, strUserAuth);
            mRequest.enqueue(new Callback<UserProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<UserProfileResponse> call, @NonNull Response<UserProfileResponse> Response) {
                    UserProfileResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            MstUserProfile objData = Result.getData();
                            SqlUser.DeleteUserProfile();
                            SqlUser.addDataProfile(objData);
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        case 202:
                            Dialog.ConfirmationDialog(mContext, mContext.getResources().getString(R.string.label_information),
                                    Result.getResultMessage(), expiredAuth(mContext), null);
                            break;
                        default:
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<UserProfileResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void onChangePassword(final Activity mContext, String strOldPassword, String strNewPassword, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId       = dataList.get(0).getUserId();
                strUserAuth     = dataList.get(0).getUserAuth();
            }

            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<BasicResponse> mRequest    = service.postChangePassword(strUserId, strUserAuth, strOldPassword, strNewPassword);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        case 202:
                            Dialog.ConfirmationDialog(mContext, mContext.getResources().getString(R.string.label_information),
                                    Result.getResultMessage(), expiredAuth(mContext), null);
                            break;
                        default:
                            FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void postUpdateProfile(final Activity mContext, String strJsonProfile, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId       = dataList.get(0).getUserId();
                strUserAuth     = dataList.get(0).getUserAuth();
            }

            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<BasicResponse> mRequest    = service.postUpdateProfile(strUserId, strUserAuth, strJsonProfile);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        case 202:
                            Dialog.ConfirmationDialog(mContext, mContext.getResources().getString(R.string.label_information),
                                    Result.getResultMessage(), expiredAuth(mContext), null);
                            break;
                        default:
                            FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void postPhotoProfile(final Activity mContext, byte[] mBitmapData, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId       = dataList.get(0).getUserId();
                strUserAuth     = dataList.get(0).getUserAuth();
            }
            String mFileName    = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(new Date())+".JPG";
            File FileSource     = Utility.createFileFromBitmap(mBitmapData, mFileName);

            Map<String, RequestBody> requestBodyMap = new HashMap<>();
            requestBodyMap.put("user_id", RequestBody.create(MediaType.parse("text/plain"), strUserId));
            requestBodyMap.put("user_auth", RequestBody.create(MediaType.parse("text/plain"), strUserAuth));
            String fileName     = "image\"; filename=\"" + mFileName;
            RequestBody Body    = RequestBody.create(MediaType.parse("multipart/form-data"), FileSource);
            requestBodyMap.put(fileName, Body);

            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<BasicResponse> mRequest    = service.postUploadProfile(requestBodyMap);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        case 202:
                            Dialog.ConfirmationDialog(mContext, mContext.getResources().getString(R.string.label_information),
                                    Result.getResultMessage(), expiredAuth(mContext), null);
                            break;
                        default:
                            FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void postUserToken(final Activity mContext, String strToken, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId       = dataList.get(0).getUserId();
                strUserAuth     = dataList.get(0).getUserAuth();
            }

            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<BasicResponse> mRequest    = service.postInputToken(strUserId, strUserAuth, strToken);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        default:
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            if (FailedAction != null)
                FailedAction.run();
        }
    }
    public static void postRemoveToken(final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId       = dataList.get(0).getUserId();
                strUserAuth     = dataList.get(0).getUserAuth();
            }

            UserService service             = ApiClient.getClient().create(UserService.class);
            Call<BasicResponse> mRequest    = service.postRemoveToken(strUserId, strUserAuth);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    switch (Result.getResultState()) {
                        case 200:
                            if (SuccessAction != null)
                                SuccessAction.run();
                            break;
                        default:
                            if (FailedAction != null)
                                FailedAction.run();
                            break;
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
                        if (FailedAction != null)
                            FailedAction.run();
                    }
                }
            });
        }catch (Exception e) {
            if (FailedAction != null)
                FailedAction.run();
        }
    }

    private static Runnable expiredAuth(final Activity activity) {
        Runnable mRun = new Runnable(){
            public void run(){
                Utility.onUserLogout(activity);
            }
        };
        return mRun;
    }
}
