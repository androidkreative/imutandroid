package com.nmk.siimut.MdlAccount.User;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstUserProfile {
    @SerializedName("ID") String Id;
    @SerializedName("USER_ID") String UserId;
    @SerializedName("JURUSAN") String Jurusan;
    @SerializedName("NAMA_LENGKAP") String NamaLengkap;
    @SerializedName("NAMA_PANGGILAN") String NamaPanggilan;
    @SerializedName("STATUS_NIKAH") String StatusNikah;
    @SerializedName("GENDER") String Gender;
    @SerializedName("AGAMA") String Agama;
    @SerializedName("PEKERJAAN") String Pekerjaan;
    @SerializedName("TEMPAT_LAHIR") String TempatLahir;
    @SerializedName("TANGGAL_LAHIR") String TanggalLahir;
    @SerializedName("ALAMAT_KTP") String AlamatKTP;
    @SerializedName("KODE_POS_KTP") String PostKTP;
    @SerializedName("ALAMAT_SURAT") String AlamatSurat;
    @SerializedName("KODE_POS_SURAT") String PostSurat;
    @SerializedName("PHONE") String Phone;
    @SerializedName("HP") String Hp;
    @SerializedName("EMAIL") String Email;
    @SerializedName("IMAGE") String Image;

    public MstUserProfile() {
    }

    public MstUserProfile(String id, String userId, String jurusan, String namaLengkap, String namaPanggilan, String statusNikah, String gender, String agama, String pekerjaan, String tempatLahir, String tanggalLahir, String alamatKTP, String postKTP, String alamatSurat, String postSurat, String phone, String hp, String email, String image) {
        Id = id;
        UserId = userId;
        Jurusan = jurusan;
        NamaLengkap = namaLengkap;
        NamaPanggilan = namaPanggilan;
        StatusNikah = statusNikah;
        Gender = gender;
        Agama = agama;
        Pekerjaan = pekerjaan;
        TempatLahir = tempatLahir;
        TanggalLahir = tanggalLahir;
        AlamatKTP = alamatKTP;
        PostKTP = postKTP;
        AlamatSurat = alamatSurat;
        PostSurat = postSurat;
        Phone = phone;
        Hp = hp;
        Email = email;
        Image = image;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getJurusan() {
        return Jurusan;
    }

    public void setJurusan(String jurusan) {
        Jurusan = jurusan;
    }

    public String getNamaLengkap() {
        return NamaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        NamaLengkap = namaLengkap;
    }

    public String getNamaPanggilan() {
        return NamaPanggilan;
    }

    public void setNamaPanggilan(String namaPanggilan) {
        NamaPanggilan = namaPanggilan;
    }

    public String getStatusNikah() {
        return StatusNikah;
    }

    public void setStatusNikah(String statusNikah) {
        StatusNikah = statusNikah;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getAgama() {
        return Agama;
    }

    public void setAgama(String agama) {
        Agama = agama;
    }

    public String getPekerjaan() {
        return Pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        Pekerjaan = pekerjaan;
    }

    public String getTempatLahir() {
        return TempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        TempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return TanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        TanggalLahir = tanggalLahir;
    }

    public String getAlamatKTP() {
        return AlamatKTP;
    }

    public void setAlamatKTP(String alamatKTP) {
        AlamatKTP = alamatKTP;
    }

    public String getPostKTP() {
        return PostKTP;
    }

    public void setPostKTP(String postKTP) {
        PostKTP = postKTP;
    }

    public String getAlamatSurat() {
        return AlamatSurat;
    }

    public void setAlamatSurat(String alamatSurat) {
        AlamatSurat = alamatSurat;
    }

    public String getPostSurat() {
        return PostSurat;
    }

    public void setPostSurat(String postSurat) {
        PostSurat = postSurat;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getHp() {
        return Hp;
    }

    public void setHp(String hp) {
        Hp = hp;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}