package com.nmk.siimut.MdlAccount.User;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


public class SqlUser {
    private static String TABLE_USER            = "MST_USER";
    private static String FIELD_USER_ID         = "USER_ID";
    private static String FIELD_USER_AUTH       = "USER_AUTH";
    private static String FIELD_EMAIL           = "EMAIL";
    private static String FIELD_COMPLETE_NAME   = "COMPLETE_NAME";
    private static String FIELD_USER_TYPE 	    = "USER_TYPE";
    private static String FIELD_SOSMED 	        = "USER_SOSMED";
    private static String FIELD_IMAGE 	            = "IMAGE";

    private static String TABLE_USER_PROFILE        = "MST_USER_PROFILE";
    private static String FIELD_ID                  = "ID";
    private static String FIELD_JURUSAN             = "JURUSAN";
    private static String FIELD_NAMA_LENGKAP        = "NAMA_LENGKAP";
    private static String FIELD_NAMA_PANGGILAN      = "NAMA_PANGGILAN";
    private static String FIELD_STATUS_NIKAH        = "STATUS_NIKAH";
    private static String FIELD_GENDER              = "GENDER";
    private static String FIELD_AGAMA 	            = "AGAMA";
    private static String FIELD_PEKERJAAN           = "PEKERJAAN";
    private static String FIELD_TEMPAT_LAHIR        = "TEMPAT_LAHIR";
    private static String FIELD_TANGGAL_LAHIR       = "TANGGAL_LAHIR";
    private static String FIELD_ALAMAT_KTP          = "ALAMAT_KTP";
    private static String FIELD_KODE_POS_KTP        = "KODE_POS_KTP";
    private static String FIELD_ALAMAT_SURAT        = "ALAMAT_SURAT";
    private static String FIELD_KODE_POS_SURAT 	    = "KODE_POS_SURAT";
    private static String FIELD_PHONE               = "PHONE";
    private static String FIELD_HP                  = "HP";


    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + FIELD_USER_ID + " TEXT PRIMARY KEY, "
                + FIELD_USER_AUTH + " TEXT, "
                + FIELD_EMAIL + " TEXT, "
                + FIELD_COMPLETE_NAME + " TEXT, "
                + FIELD_USER_TYPE + " TEXT, "
                + FIELD_SOSMED + " TEXT, "
                + FIELD_IMAGE + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);

        String CREATE_TABLE_USER_PROFILE = "CREATE TABLE " + TABLE_USER_PROFILE + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_USER_ID + " TEXT, "
                + FIELD_JURUSAN + " TEXT, "
                + FIELD_NAMA_LENGKAP + " TEXT, "
                + FIELD_NAMA_PANGGILAN + " TEXT, "
                + FIELD_STATUS_NIKAH + " TEXT, "
                + FIELD_GENDER + " TEXT, "
                + FIELD_AGAMA + " TEXT, "
                + FIELD_PEKERJAAN + " TEXT, "
                + FIELD_TEMPAT_LAHIR + " TEXT, "
                + FIELD_TANGGAL_LAHIR + " TEXT, "
                + FIELD_ALAMAT_KTP + " TEXT, "
                + FIELD_KODE_POS_KTP + " TEXT, "
                + FIELD_ALAMAT_SURAT + " TEXT, "
                + FIELD_KODE_POS_SURAT + " TEXT, "
                + FIELD_PHONE + " TEXT, "
                + FIELD_HP + " TEXT, "
                + FIELD_EMAIL + " TEXT, "
                + FIELD_IMAGE + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_USER_PROFILE);
    }
    public static void addData(MstUser Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_USER_ID, Data.getUserId());
        values.put(FIELD_USER_AUTH, Data.getUserAuth());
        values.put(FIELD_EMAIL, Data.getEmail());
        values.put(FIELD_COMPLETE_NAME, Data.getCompleteName());
        values.put(FIELD_USER_TYPE, Data.getUserType());
        values.put(FIELD_SOSMED, Data.getSosmedType());
        values.put(FIELD_IMAGE, Data.getImage());
        db.insert(TABLE_USER, null, values);
    }
    public static List<MstUser> getAllData() {
        List<MstUser> DataList   = new ArrayList<MstUser>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_USER;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstUser Data = new MstUser();
                Data.setUserId(cursor.getString(0));
                Data.setUserAuth(cursor.getString(1));
                Data.setEmail(cursor.getString(2));
                Data.setCompleteName(cursor.getString(3));
                Data.setUserType(cursor.getString(4));
                Data.setSosmedType(cursor.getString(5));
                Data.setImage(cursor.getString(6));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteData() {
        if (getAllData().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_USER);
        }
    }

    public static void addDataProfile(MstUserProfile Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_USER_ID, Data.getUserId());
        values.put(FIELD_JURUSAN, Data.getJurusan());
        values.put(FIELD_NAMA_LENGKAP, Data.getNamaLengkap());
        values.put(FIELD_NAMA_PANGGILAN, Data.getNamaPanggilan());
        values.put(FIELD_STATUS_NIKAH, Data.getStatusNikah());
        values.put(FIELD_GENDER, Data.getGender());
        values.put(FIELD_AGAMA, Data.getAgama());
        values.put(FIELD_PEKERJAAN, Data.getPekerjaan());
        values.put(FIELD_TEMPAT_LAHIR, Data.getTempatLahir());
        values.put(FIELD_TANGGAL_LAHIR, Data.getTanggalLahir());
        values.put(FIELD_ALAMAT_KTP, Data.getAlamatKTP());
        values.put(FIELD_KODE_POS_KTP, Data.getPostKTP());
        values.put(FIELD_ALAMAT_SURAT, Data.getAlamatSurat());
        values.put(FIELD_KODE_POS_SURAT, Data.getPostSurat());
        values.put(FIELD_PHONE, Data.getPhone());
        values.put(FIELD_HP, Data.getHp());
        values.put(FIELD_EMAIL, Data.getEmail());
        values.put(FIELD_IMAGE, Data.getImage());
        db.insert(TABLE_USER_PROFILE, null, values);
    }
    public static List<MstUserProfile> getAllDataProfile() {
        List<MstUserProfile> DataList   = new ArrayList<MstUserProfile>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_USER_PROFILE;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstUserProfile Data = new MstUserProfile();
                Data.setId(cursor.getString(0));
                Data.setUserId(cursor.getString(1));
                Data.setJurusan(cursor.getString(2));
                Data.setNamaLengkap(cursor.getString(3));
                Data.setNamaPanggilan(cursor.getString(4));
                Data.setStatusNikah(cursor.getString(5));
                Data.setGender(cursor.getString(6));
                Data.setAgama(cursor.getString(7));
                Data.setPekerjaan(cursor.getString(8));
                Data.setTempatLahir(cursor.getString(9));
                Data.setTanggalLahir(cursor.getString(10));
                Data.setAlamatKTP(cursor.getString(11));
                Data.setPostKTP(cursor.getString(12));
                Data.setAlamatSurat(cursor.getString(13));
                Data.setPostSurat(cursor.getString(14));
                Data.setPhone(cursor.getString(15));
                Data.setHp(cursor.getString(16));
                Data.setEmail(cursor.getString(17));
                Data.setImage(cursor.getString(18));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteUserProfile() {
        if (getAllDataProfile().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_USER_PROFILE);
        }
    }

}