package com.nmk.siimut.ApiService;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.Utilities.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Warsono on 10/03/2019.
 */

public class ApiInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        try {
            PackageInfo pInfo = GlobalApplication.getContext().getPackageManager().getPackageInfo(GlobalApplication.getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {}

        Request original    = chain.request();
        Request request     = original.newBuilder()
                .header("X-App-Key", Constants.APP_KEY)
                .header("User-Agent", Constants.API_HEADER)
                .method(original.method(), original.body())
                .build();
        Response response   = chain.proceed(request);
        String bodyString   = response.body().string();

        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), bodyString))
                .build();
    }
}
