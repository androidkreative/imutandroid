package com.nmk.siimut.ApiService;

import android.os.Build;

import com.nmk.siimut.Utilities.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Warsono on 10/03/2019.
 */

public class ApiClient {
    public static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if(retrofit==null){
            ConnectionSpec spec = new
                    ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .cipherSuites(
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                    .build();

            OkHttpClient client;
            if (Build.VERSION.SDK_INT >= 21) {
                client = new OkHttpClient.Builder()
                        .connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                        .readTimeout(Constants.READ_TIMEOUT, TimeUnit.MILLISECONDS)
                        .writeTimeout(Constants.WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .addInterceptor(new ApiInterceptor())
                        .build();
            }else{
                client = new OkHttpClient.Builder()
                        .connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                        .readTimeout(Constants.READ_TIMEOUT, TimeUnit.MILLISECONDS)
                        .writeTimeout(Constants.WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .addInterceptor(new ApiInterceptor())
                        .build();
            }
            retrofit  = new Retrofit.Builder()
                    .baseUrl(Constants.API_SERVER)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
