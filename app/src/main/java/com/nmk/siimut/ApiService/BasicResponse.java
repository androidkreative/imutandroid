package com.nmk.siimut.ApiService;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 10/03/2019.
 */

public class BasicResponse {

    @SerializedName("STATUS")
    private int ResultState;

    @SerializedName("MESSAGE")
    private String ResultMessage;

    public int  getResultState() {
        return ResultState;
    }

    public String getResultMessage() {
        return ResultMessage;
    }

}
