package com.nmk.siimut.MdlHome;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlProfile {
    private static String TABLE_PROFILE         = "MST_PROFILE";
    private static String FIELD_ID              = "ID";
    private static String FIELD_TITLE           = "TITLE";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_PROFILE + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_TITLE + " TEXT, "
                + FIELD_DESCRIPTION + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);
    }
    public static void addData(MstProfile Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_TITLE, Data.getTitle());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        db.insert(TABLE_PROFILE, null, values);
    }
    public static List<MstProfile> getAllData() {
        List<MstProfile> DataList   = new ArrayList<MstProfile>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_PROFILE;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstProfile Data = new MstProfile();
                Data.setId(cursor.getString(0));
                Data.setTitle(cursor.getString(1));
                Data.setDescription(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteData() {
        if (getAllData().size() > 0){
            DatabaseHandler dataBase    = new DatabaseHandler();
            SQLiteDatabase db 		    = dataBase.getWritableDatabase();
            db.execSQL("DELETE FROM "+ TABLE_PROFILE);
        }
    }
}