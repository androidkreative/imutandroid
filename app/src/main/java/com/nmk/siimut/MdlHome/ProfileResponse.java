package com.nmk.siimut.MdlHome;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class ProfileResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstProfile> DataList    = new ArrayList<MstProfile>();

    public List<MstProfile> getDataList() {
        return DataList;
    }
}
