package com.nmk.siimut.MdlHome;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstProfile {
    @SerializedName("ID")
    String Id;

    @SerializedName("TITLE")
    String Title;

    @SerializedName("DESCRIPTION")
    String Description;

    public MstProfile() {
    }

    public MstProfile(String id, String title, String description) {
        Id          = id;
        Title       = title;
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}