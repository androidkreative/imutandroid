package com.nmk.siimut.MdlHome;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nmk.siimut.MdlAddress.AddressActivity;
import com.nmk.siimut.MdlBanner.MstBanner;
import com.nmk.siimut.MdlBanner.SqlBanner;
import com.nmk.siimut.MdlFakultas.FakultasActivity;
import com.nmk.siimut.MdlFasilitas.FasilitasActivity;
import com.nmk.siimut.MdlRelasi.RelasiActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.Autoslide.AutoScrollViewPager;
import com.nmk.siimut.UiDesign.Autoslide.AutoSlideBanner;
import com.nmk.siimut.Utilities.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */

public class FragmentHome extends Fragment implements View.OnClickListener{

    private ViewGroup RootView;
    private AutoScrollViewPager pagerView;
    private LinearLayout indicatorView;
    private List<MstBanner> bannerList      = new ArrayList<MstBanner>();
    private TextView viewVisiTitle, viewTujuanTitle, viewMisiTitle;
    private WebView viewVisiDesc, viewMisiDesc, viewTujuanDesc;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RootView            = (ViewGroup) inflater.inflate(R.layout.fragment_home, null);
        pagerView           = RootView.findViewById(R.id.slideBanner);
        indicatorView       = RootView.findViewById(R.id.slideIndicator);
        LinearLayout buttonStudi        = RootView.findViewById(R.id.btnStudi);
        buttonStudi.setOnClickListener(this);
        LinearLayout buttonFasilitas    = RootView.findViewById(R.id.btnFasilitas);
        buttonFasilitas.setOnClickListener(this);
        LinearLayout buttonNetwork      = RootView.findViewById(R.id.btnNetwork);
        buttonNetwork.setOnClickListener(this);
        LinearLayout buttonLocation     = RootView.findViewById(R.id.btnLocation);
        buttonLocation.setOnClickListener(this);

        viewVisiTitle       = RootView.findViewById(R.id.visiTitleView);
        viewVisiDesc        = RootView.findViewById(R.id.visiDescView);
        viewMisiTitle       = RootView.findViewById(R.id.misiTitleView);
        viewMisiDesc        = RootView.findViewById(R.id.misiDescView);
        viewTujuanTitle     = RootView.findViewById(R.id.tujuanTitleView);
        viewTujuanDesc      = RootView.findViewById(R.id.tujuanDescView);

        onSetSliderView();
        onSetAkreditasiBlink();
        onSetVisiMisi();

        return RootView;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnStudi:
                onClickFakultas();
                break;
            case R.id.btnFasilitas:
                onClickFasilitas();
                break;
            case R.id.btnNetwork:
                onClickKerjasama();
                break;
            case R.id.btnLocation:
                onClickLocation();
                break;
        }
    }
    private void onClickFakultas(){
        YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .playOn(RootView.findViewById(R.id.imgStudi));
        final Handler handler           = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent   = new Intent(getActivity(), FakultasActivity.class);
                startActivity(intent);
            }
        }, 500);

    }
    private void onClickFasilitas(){
        YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .playOn(RootView.findViewById(R.id.imgFasilitas));
        final Handler handler           = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent   = new Intent(getActivity(), FasilitasActivity.class);
                startActivity(intent);
            }
        }, 500);

    }
    private void onClickKerjasama(){
        YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .playOn(RootView.findViewById(R.id.imgNetwork));
        final Handler handler           = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent   = new Intent(getActivity(), RelasiActivity.class);
                startActivity(intent);
            }
        }, 500);

    }
    private void onClickLocation(){
        YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .playOn(RootView.findViewById(R.id.imgLocation));
        final Handler handler           = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent   = new Intent(getActivity(), AddressActivity.class);
                startActivity(intent);
            }
        }, 500);
    }
    private void onSetSliderView() {
        if (bannerList.size() > 0)
            bannerList.clear();
        bannerList = SqlBanner.getAllData();
        if (bannerList.size() > 0) {
            AutoSlideBanner.onSetSlideBanner(getActivity(), pagerView, bannerList, Constants.VIEW_HOME);
            AutoSlideBanner.onSetSlideIndicator(getActivity(), pagerView, bannerList, indicatorView);
        }
    }
    private void onSetAkreditasiBlink() {
        Animation animation = new AlphaAnimation((float) 1.0, 0.2f);   // Change alpha from fully visible to invisible
        animation.setDuration(1000);                                 // duration - half a second
        animation.setInterpolator(new LinearInterpolator());        // do not alter
        animation.setRepeatCount(Animation.INFINITE);               // Repeat animation
        animation.setRepeatMode(Animation.REVERSE);                 // Reverse animation at the
        RootView.findViewById(R.id.akreditasiLayout).startAnimation(animation);
    }
    private void onSetVisiMisi() {
        List<MstProfile> dataList = SqlProfile.getAllData();
        if (dataList.size() > 0) {
            viewVisiTitle.setText(dataList.get(0).getTitle());
            viewMisiTitle.setText(dataList.get(1).getTitle());
            viewTujuanTitle.setText(dataList.get(2).getTitle());
            viewVisiDesc.loadData(dataList.get(0).getDescription(), "text/html", "utf-8");
            viewMisiDesc.loadData(dataList.get(1).getDescription(), "text/html", "utf-8");
            viewTujuanDesc.loadData(dataList.get(2).getDescription(), "text/html", "utf-8");

//            viewVisiDesc.setText(Html.fromHtml(dataList.get(0).getDescription(), null, new UlTagHandler()));
//            viewMisiDesc.setText(Html.fromHtml(dataList.get(1).getDescription(), null, new UlTagHandler()));
//            viewTujuanDesc.setText(Html.fromHtml(dataList.get(2).getDescription(), null, new UlTagHandler()));
        }
    }
}
