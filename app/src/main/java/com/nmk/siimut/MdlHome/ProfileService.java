package com.nmk.siimut.MdlHome;

import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface ProfileService {

    @GET(Constants.API_GET_PROFILE)
    Call<ProfileResponse> getProfile(@Query("id") String UserID);
}
