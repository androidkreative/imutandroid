package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlDaftar {
    private static String TABLE_SYARAT          = "MST_SYARAT";
    private static String FIELD_ID              = "ID";
    private static String FIELD_DESCRIPTION     = "DESCRIPTION";
    private static String FIELD_JUMLAH          = "JUMLAH";

    private static String TABLE_JADWAL          = "MST_JADWAL";
    private static String FIELD_TITLE           = "TITLE";
    private static String FIELD_PERIODE         = "PERIODE";
    private static String FIELD_STATUS          = "STATUS";

    private static String TABLE_MARKET          = "MST_MARKET";
    private static String FIELD_CHECKED         = "CHECKED";


    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_SYARAT = "CREATE TABLE " + TABLE_SYARAT + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT, "
                + FIELD_JUMLAH + " TEXT " + ")";
        db.execSQL(CREATE_SYARAT);

        String CREATE_JADWAL = "CREATE TABLE " + TABLE_JADWAL + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_TITLE + " TEXT, "
                + FIELD_PERIODE + " TEXT, "
                + FIELD_STATUS + " TEXT " + ")";
        db.execSQL(CREATE_JADWAL);

        String CREATE_MARKET = "CREATE TABLE " + TABLE_MARKET + "("
                + FIELD_ID + " TEXT PRIMARY KEY, "
                + FIELD_DESCRIPTION + " TEXT, "
                + FIELD_CHECKED + " TEXT " + ")";
        db.execSQL(CREATE_MARKET);
    }
    public static void addDataSyarat(MstSyarat Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        values.put(FIELD_JUMLAH, Data.getJumlah());
        db.insert(TABLE_SYARAT, null, values);
    }
    public static List<MstSyarat> getSyarat() {
        List<MstSyarat> DataList   = new ArrayList<MstSyarat>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_SYARAT;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstSyarat Data = new MstSyarat();
                Data.setId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setJumlah(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteSyarat() {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_SYARAT);
    }

    public static void addDataJadwal(MstJadwal Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_TITLE, Data.getTitle());
        values.put(FIELD_PERIODE, Data.getPeriode());
        values.put(FIELD_STATUS, Data.getStatus());
        db.insert(TABLE_JADWAL, null, values);
    }
    public static List<MstJadwal> getJadwal() {
        List<MstJadwal> DataList    = new ArrayList<MstJadwal>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_JADWAL;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstJadwal Data = new MstJadwal();
                Data.setId(cursor.getString(0));
                Data.setTitle(cursor.getString(1));
                Data.setPeriode(cursor.getString(2));
                Data.setStatus(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteJadwal() {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_JADWAL);
    }

    public static void addDataMarket(MstMarket Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_ID, Data.getId());
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        values.put(FIELD_CHECKED, Data.getChecked());
        db.insert(TABLE_MARKET, null, values);
    }
    public static void UpdateMarket(MstMarket Data){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_DESCRIPTION, Data.getDescription());
        values.put(FIELD_CHECKED, Data.getChecked());
        db.update(TABLE_MARKET, values, FIELD_ID+ " = ?" ,
                new String[] {Data.getId()});
    }
    public static void UpdateMarketChecked(String strId, String strChecked){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_CHECKED, strChecked);
        db.update(TABLE_MARKET, values, FIELD_ID+ " = ?" ,
                new String[] {strId});
    }
    public static void UpdateMarketUnChecked(){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_CHECKED, "0");
        db.update(TABLE_MARKET, values, "" ,
                new String[] {});
    }
    public static List<MstMarket> getMarket() {
        List<MstMarket> DataList   = new ArrayList<MstMarket>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_MARKET;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstMarket Data = new MstMarket();
                Data.setId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setChecked(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static List<MstMarket> getMarketById(String strData) {
        List<MstMarket> DataList   = new ArrayList<MstMarket>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_MARKET+ " WHERE " + FIELD_ID + " = '"+strData+"'";
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstMarket Data = new MstMarket();
                Data.setId(cursor.getString(0));
                Data.setDescription(cursor.getString(1));
                Data.setChecked(cursor.getString(2));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteMarket() {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_MARKET);
    }
}