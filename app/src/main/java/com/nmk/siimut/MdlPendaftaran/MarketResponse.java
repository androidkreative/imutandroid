package com.nmk.siimut.MdlPendaftaran;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class MarketResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstMarket> DataList    = new ArrayList<MstMarket>();

    public List<MstMarket> getDataList() {
        return DataList;
    }
}
