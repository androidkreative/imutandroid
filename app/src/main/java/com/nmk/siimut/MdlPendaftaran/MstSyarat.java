package com.nmk.siimut.MdlPendaftaran;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstSyarat {
    @SerializedName("ID")
    String Id;

    @SerializedName("DESCRIPTION")
    String Description;

    @SerializedName("JUMLAH")
    String Jumlah;

    public MstSyarat() {
    }

    public MstSyarat(String id, String description, String jumlah) {
        Id          = id;
        Description = description;
        Jumlah      = jumlah;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getJumlah() {
        return Jumlah;
    }

    public void setJumlah(String jumlah) {
        Jumlah = jumlah;
    }
}