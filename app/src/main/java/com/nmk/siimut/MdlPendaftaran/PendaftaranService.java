package com.nmk.siimut.MdlPendaftaran;

import com.nmk.siimut.ApiService.BasicResponse;
import com.nmk.siimut.Utilities.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by Warsono on 01/07/2019.
 */

public interface PendaftaranService {

    @GET(Constants.API_GET_SYARAT)
    Call<SyaratResponse> getDataList(@Query("id") String UserID);

    @GET(Constants.API_GET_JADWAL)
    Call<JadwalResponse> getJadwalList(@Query("id") String UserID);

    @GET(Constants.API_GET_MARKET)
    Call<MarketResponse> getMarketList(@Query("id") String UserID);

    @FormUrlEncoded
    @POST(Constants.API_REGISTER)
    Call<BasicResponse> postRegister(
            @Field("id") String UserID,
            @Field("auth") String UserAuth,
            @Field("email") String Email,
            @Field("ajaran") String TahunAjaran,
            @Field("json_1") String JsonOne,
            @Field("json_2") String JsonTwo,
            @Field("json_3") String JsonThree,
            @Field("json_4") String JsonFour);
}
