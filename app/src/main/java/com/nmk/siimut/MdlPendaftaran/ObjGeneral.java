package com.nmk.siimut.MdlPendaftaran;


/**
 * Created by Warsono on 01/07/2019.
 */

public class ObjGeneral {

    String Id;
    String Name;

    public ObjGeneral() {
    }

    public ObjGeneral(String id, String name) {
        Id      = id;
        Name    = name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}