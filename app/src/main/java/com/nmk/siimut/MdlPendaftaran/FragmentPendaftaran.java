package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.MstUserProfile;
import com.nmk.siimut.MdlAccount.User.SignInActivity;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlOther.AjaranResponse;
import com.nmk.siimut.MdlOther.MstAjaran;
import com.nmk.siimut.MdlOther.OtherService;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Utility;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class FragmentPendaftaran extends Fragment implements View.OnClickListener{

    private List<MstUser> dataList;
    private MstUser objUser;
    private RecyclerView dataView, periodeView;
    private LinearLayout contentView;
    private RelativeLayout noInternetView;
    private TextView buttonDaftar;
    private ViewGroup RootView;
    private ImageView buttonJoin;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RootView            = (ViewGroup) inflater.inflate(R.layout.fragment_pendaftaran, null);
        contentView         = RootView.findViewById(R.id.viewContent);
        contentView.setVisibility(View.VISIBLE);
        dataView            = RootView.findViewById(R.id.viewSyarat);
        periodeView         = RootView.findViewById(R.id.viewPeriode);
        noInternetView      = RootView.findViewById(R.id.viewUnavailable);
        noInternetView.setVisibility(View.GONE);
        buttonJoin          = RootView.findViewById(R.id.btnJoin);
        buttonJoin.setOnClickListener(this);
        buttonDaftar        = RootView.findViewById(R.id.btnDaftar);
        buttonDaftar.setOnClickListener(this);
        onSetJoinBlink();

        List<MstSyarat> syaratList = SqlDaftar.getSyarat();
        if (syaratList.size() > 0){
            dataView.setNestedScrollingEnabled(false);
            dataView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
            dataAdapter newAdapter   = new dataAdapter(syaratList);
            dataView.setAdapter(newAdapter);
        }
        List<MstJadwal> jadwalList = SqlDaftar.getJadwal();
        if (jadwalList.size() > 0){
            periodeView.setNestedScrollingEnabled(false);
            periodeView.setLayoutManager(new LinearLayoutManager(GlobalApplication.getContext()));
            periodeAdapter newAdapter   = new periodeAdapter(jadwalList);
            periodeView.setAdapter(newAdapter);
        }

        setButtonDaftar.run();
        if (Utility.isNetworkConnected())
            new onGetAjaran().execute(0);

        return RootView;
    }
    private Runnable setButtonDaftar = new Runnable() {
        @Override
        public void run() {
            dataList = SqlUser.getAllData();
            if (dataList.size() > 0) {
                objUser = dataList.get(0);
                switch (objUser.getUserType()) {
                    case "0":           //UMUM
                        buttonDaftar.setVisibility(View.GONE);
                        break;
                    case "1":           //CALON MAHASISWA
                        List<MstUserProfile> dataProfile = SqlUser.getAllDataProfile();
                        if (dataProfile.size() > 0){
                            if (dataProfile.get(0).getId() != null && !dataProfile.get(0).getId().contentEquals(""))
                                buttonDaftar.setVisibility(View.GONE);
                            else
                                buttonDaftar.setVisibility(View.VISIBLE);
                        }else
                            buttonDaftar.setVisibility(View.VISIBLE);
                        break;
                    case "2":           //MAHASISWA
                        buttonDaftar.setVisibility(View.GONE);
                        break;
                    case "3":           //ADMIN
                        buttonDaftar.setVisibility(View.GONE);
                        break;
                }
            }else
                buttonDaftar.setVisibility(View.VISIBLE);

            List<MstAjaran> dataAjaran = SqlOther.getAllAjaran();
            if (dataAjaran.size() > 0){
                MstAjaran objAjaran = dataAjaran.get(0);
                if (objAjaran.getStatus().contentEquals("1")){
                    buttonDaftar.setEnabled(true);
                    buttonDaftar.setText(getResources().getString(R.string.label_pendaftaran));
                    buttonDaftar.setBackground(getResources().getDrawable(R.drawable.bg_button_gradient));
                }else{
                    buttonDaftar.setEnabled(false);
                    buttonDaftar.setText(getResources().getString(R.string.label_pendaftaran_close));
                    buttonDaftar.setBackground(getResources().getDrawable(R.drawable.bg_button_disable));
                }
            }else{
                buttonDaftar.setEnabled(false);
                buttonDaftar.setText(getResources().getString(R.string.label_pendaftaran_close));
                buttonDaftar.setBackground(getResources().getDrawable(R.drawable.bg_button_disable));
            }
        }
    };
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnJoin:
                onGotoSignIn();
                break;
            case R.id.btnDaftar:
                onGotoSignIn();
                break;
        }
    }
    private void onGotoSignIn(){
        if (dataList.size() > 0) {
            startActivity(new Intent(getActivity(), RegOneActivity.class));
        }else {
            Intent intentLogin = new Intent(getActivity(), SignInActivity.class);
            intentLogin.putExtra(Constants.VIEW_KEY, Constants.VIEW_PMB);
            startActivity(intentLogin);
        }
    }
    private void onSetJoinBlink() {
        Animation animation = new AlphaAnimation((float) 1.0, 0.2f);   // Change alpha from fully visible to invisible
        animation.setDuration(1500);                                 // duration - half a second
        animation.setInterpolator(new LinearInterpolator());        // do not alter
        animation.setRepeatCount(Animation.INFINITE);               // Repeat animation
        animation.setRepeatMode(Animation.REVERSE);                 // Reverse animation at the
        buttonJoin.startAnimation(animation);
    }
    
    class dataAdapter extends RecyclerView.Adapter<dataAdapter.ViewHolder> {
        private List<MstSyarat> mItems;

        private dataAdapter(List<MstSyarat> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public dataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_syarat, viewGroup, false);
            return new dataAdapter.ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstSyarat objectData   = mItems.get(position);
            viewHolder.viewSyarat.setText(objectData.getDescription());
            viewHolder.viewJumlah.setText(objectData.getJumlah()+" lembar");
            if (objectData.getJumlah().contentEquals("0") || objectData.getJumlah().contentEquals("1"))
                viewHolder.viewJumlah.setVisibility(View.GONE);
            else
                viewHolder.viewJumlah.setVisibility(View.VISIBLE);
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView viewSyarat, viewJumlah;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                viewSyarat      = itemView.findViewById(R.id.syaratView);
                viewJumlah      = itemView.findViewById(R.id.jumlahView);
            }
        }
    }
    class periodeAdapter extends RecyclerView.Adapter<periodeAdapter.ViewHolder> {
        private List<MstJadwal> mItems;

        private periodeAdapter(List<MstJadwal> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_periode, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final MstJadwal objectData   = mItems.get(position);
            viewHolder.viewGelombang.setText(objectData.getTitle());
            viewHolder.viewPeriode.setText(objectData.getPeriode());
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            TextView viewGelombang, viewPeriode;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                viewGelombang   = itemView.findViewById(R.id.gelombangView);
                viewPeriode     = itemView.findViewById(R.id.periodeView);
            }
        }
    }
    class onGetAjaran extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() { }
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                OtherService service                = ApiClient.getClient().create(OtherService.class);
                Call<AjaranResponse> mRequest       = service.getAjaran("");
                Response<AjaranResponse> Response   = mRequest.execute();
                AjaranResponse Result               = Response.body();
                if (Result != null){
                    switch (Result.getResultState()) {
                        case 200:
                            SqlOther.DeleteAjaran();
                            MstAjaran objData   = Result.getObjData();
                            SqlOther.addDataAjaran(objData);
                            break;
                        default:
                            break;
                    }
                }
            }catch (Exception ignored) { }
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            setButtonDaftar.run();
        }
    }
}
