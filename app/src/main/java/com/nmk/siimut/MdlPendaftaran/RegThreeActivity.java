package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class RegThreeActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, DatePickerCallback {

    private List<ObjGeneral> itemAnswer5List     = new ArrayList<ObjGeneral>();
    private List<ObjGeneral> itemAnswer6List     = new ArrayList<ObjGeneral>();
    private Spinner inputAnswer5, inputAnswer6;
    private EditText inputAnswer1, inputAnswer2, inputAnswer3;
    private EditText inputAnswer4, inputAnswer7;
    private CheckBox emptyCheckbox, mobilCheckbox, motorCheckbox;
    private EditText inputMobil, inputMotor;
    private TextView viewAnswer9;
    private String strAnswer1, strAnswer2, strAnswer3, strAnswer4;
    private String strAnswer5, strAnswer6, strAnswer7;
    private String strMobil, strJmlMobil, strMotor, strJmlMotor;
    private String strAnswer9 = "";
    private String strEmptyTransport;
    private RelativeLayout loadingMain;
    private String strEmail;


    @Override
    public void onBackPressed(){
        getInputData();
        String strJsonThree  = getJsonString();
        SqlRegistration.UpdateRegThree(strEmail, strJsonThree);
        Intent intent   = new Intent(RegThreeActivity.this, RegTwoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_three);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        inputAnswer1    = findViewById(R.id.answer1Input);
        inputAnswer2    = findViewById(R.id.answer2Input);
        inputAnswer3    = findViewById(R.id.answer3Input);
        inputAnswer4    = findViewById(R.id.answer4Input);
        inputAnswer5    =  findViewById(R.id.answer5Input);
        inputAnswer5.setOnItemSelectedListener(this);
        inputAnswer6    =  findViewById(R.id.answer6Input);
        inputAnswer6.setOnItemSelectedListener(this);
        inputAnswer7    =  findViewById(R.id.answer7Input);
        emptyCheckbox   =  findViewById(R.id.emptyCheck);
        mobilCheckbox   =  findViewById(R.id.mobilCheck);
        motorCheckbox   =  findViewById(R.id.motorCheck);
        inputMobil      =  findViewById(R.id.mobilQtyInput);
        inputMotor      =  findViewById(R.id.motorQtyInput);
        viewAnswer9     = findViewById(R.id.answer9Input);
        loadingMain     = findViewById(R.id.mainLoading);
        inputMobil.setEnabled(false);
        inputMotor.setEnabled(false);
        inputMobil.setText("");
        inputMotor.setText("");

        emptyCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
               if (isChecked){
                   mobilCheckbox.setChecked(false);
                   motorCheckbox.setChecked(false);
                   inputMobil.setEnabled(false);
                   inputMotor.setEnabled(false);
                   inputMobil.setText("");
                   inputMotor.setText("");
               }
           }
        });
        mobilCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked){
                    emptyCheckbox.setChecked(false);
                    inputMobil.setEnabled(true);
                }else
                    inputMobil.setEnabled(false);
            }
        });
        motorCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked){
                    emptyCheckbox.setChecked(false);
                    inputMotor.setEnabled(true);
                }else
                    inputMotor.setEnabled(false);
            }
        });
        onSetData();
    }
    private void onSetData(){
        List<MstRegistration> regList = SqlRegistration.getRegistration();
        if (regList.size() > 0){
            MstRegistration objReg  = regList.get(0);
            strEmail                = objReg.getEmail();
            try {
                JSONObject jObj = new JSONObject(objReg.getJsonThree());
                strAnswer1      = jObj.getString("ANSWER1");
                strAnswer2      = jObj.getString("ANSWER2");
                strAnswer3      = jObj.getString("ANSWER3");
                strAnswer4      = jObj.getString("ANSWER4");
                strAnswer5      = jObj.getString("ANSWER5");
                strAnswer6      = jObj.getString("ANSWER6");
                strAnswer7      = jObj.getString("ANSWER7");
                strAnswer9     = jObj.getString("ANSWER9");
                strEmptyTransport   = jObj.getString("IS_EMPTY");
                strMobil        = jObj.getString("IS_MOBIL");
                strMotor        = jObj.getString("IS_MOTOR");
                strJmlMobil     = jObj.getString("MOBIL");
                strJmlMotor     = jObj.getString("MOTOR");

                inputAnswer1.setText(strAnswer1);
                inputAnswer2.setText(strAnswer2);
                inputAnswer3.setText(strAnswer3);
                inputAnswer4.setText(strAnswer4);
                inputAnswer7.setText(strAnswer7);
                viewAnswer9.setText(Utility.DateView(strAnswer9));

                if (strEmptyTransport.contentEquals("1")){
                    emptyCheckbox.setChecked(true);
                    mobilCheckbox.setChecked(false);
                    motorCheckbox.setChecked(false);
                    inputMobil.setText("");
                    inputMotor.setText("");
                }else{
                    emptyCheckbox.setSelected(false);
                    if (strMobil.contentEquals("1")){
                        mobilCheckbox.setChecked(true);
                        inputMobil.setText(strJmlMobil);
                    }else
                        mobilCheckbox.setChecked(false);
                    if (strMotor.contentEquals("1")){
                        motorCheckbox.setChecked(true);
                        inputMotor.setText(strJmlMotor);
                    }else
                        motorCheckbox.setChecked(false);
                }
            } catch (JSONException e) {}
        }
        onSetSpinnerAnswer5();
        onSetSpinnerAnswer6();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPrev:
                onBackPressed();
                break;
            case R.id.btnNext:
                if (validateData()){
                    loadingMain.setVisibility(View.VISIBLE);
                    inputMotor.requestFocus();

                    String strJsonThree   = getJsonString();
                    SqlRegistration.UpdateRegThree(strEmail, strJsonThree);

                    Intent intent   = new Intent(RegThreeActivity.this, RegFourActivity.class);
                    startActivity(intent);
                    loadingMain.setVisibility(View.GONE);
                }
                break;
            case R.id.answer9Input:
                onClickDate();
                break;
        }
    }
    private void onClickDate(){
        Calendar currentDate    = Calendar.getInstance();
        Calendar maxDate        = Calendar.getInstance();
        long selectDateLong;
            if (!viewAnswer9.getText().toString().contentEquals("")){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Date date       = dateFormat.parse(viewAnswer9.getText().toString());
                selectDateLong  = date.getTime();
            } catch (ParseException e) {
                selectDateLong    = currentDate.getTimeInMillis();
            }
        }else
            selectDateLong    = currentDate.getTimeInMillis();

        maxDate.set(currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH));
        DatePickerFragmentDialog.newInstance(
                DateTimeBuilder.newInstance()
                        .withTime(false)
                        .withSelectedDate(selectDateLong)
                        .withMinDate(maxDate.getTimeInMillis()))
                .show(getSupportFragmentManager(), "DatePickerFragmentDialog");
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()) {
            case R.id.answer5Input:
                strAnswer5  = itemAnswer5List.get(position).getId();
                break;
            case R.id.answer6Input:
                strAnswer6   = itemAnswer6List.get(position).getId();
                break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    public void onDateSet(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        strAnswer9  = dateFormat.format(new Date(date));
        viewAnswer9.setText(Utility.DateView(strAnswer9));
    }
    private void onSetSpinnerAnswer5() {
        if (itemAnswer5List.size() > 0)
            itemAnswer5List.clear();
        itemAnswer5List.add(new ObjGeneral("1", getResources().getString(R.string.label_milik_sendiri)));
        itemAnswer5List.add(new ObjGeneral("2", getResources().getString(R.string.label_milik_keluarga)));
        itemAnswer5List.add(new ObjGeneral("3", getResources().getString(R.string.label_kpr)));
        itemAnswer5List.add(new ObjGeneral("4", getResources().getString(R.string.label_company)));
        itemAnswer5List.add(new ObjGeneral("5", getResources().getString(R.string.label_sewa)));
        itemAnswer5List.add(new ObjGeneral("6", getResources().getString(R.string.label_asrama)));
        itemAnswer5List.add(new ObjGeneral("7", getResources().getString(R.string.label_other)));
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemAnswer5List.size();
            }

            @Override
            public String get(int location) {
                return itemAnswer5List.get(location).getName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputAnswer5.setAdapter(dataAdapter);
        int mPos = 0;
        if (strAnswer5 != null && !strAnswer5.contentEquals("")) {
            int countData = itemAnswer5List.size();
            for (int i=0; i<countData; i++) {
                ObjGeneral objData = itemAnswer5List.get(i);
                if (objData.getId().contentEquals(strAnswer5))
                    mPos = i;
            }
        }
        inputAnswer5.setSelection(mPos);
    }
    private void onSetSpinnerAnswer6() {
        if (itemAnswer6List.size() > 0)
            itemAnswer6List.clear();
        itemAnswer6List.add(new ObjGeneral("1", getResources().getString(R.string.label_1)));
        itemAnswer6List.add(new ObjGeneral("2", getResources().getString(R.string.label_2)));
        itemAnswer6List.add(new ObjGeneral("3", getResources().getString(R.string.label_3)));
        itemAnswer6List.add(new ObjGeneral("4", getResources().getString(R.string.label_4)));
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemAnswer6List.size();
            }

            @Override
            public String get(int location) {
                return itemAnswer6List.get(location).getName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputAnswer6.setAdapter(dataAdapter);
        int mPos = 0;
        if (strAnswer6 != null && !strAnswer6.contentEquals("")) {
            int countData = itemAnswer6List.size();
            for (int i=0; i<countData; i++) {
                ObjGeneral objData = itemAnswer6List.get(i);
                if (objData.getId().contentEquals(strAnswer6))
                    mPos = i;
            }
        }
        inputAnswer6.setSelection(mPos);
    }
    private void getInputData(){
        strAnswer1      = inputAnswer1.getText().toString().trim();
        strAnswer2      = inputAnswer2.getText().toString().trim();
        strAnswer3      = inputAnswer3.getText().toString().trim();
        strAnswer4      = inputAnswer4.getText().toString().trim();
        strAnswer7      = inputAnswer7.getText().toString().trim();

        strEmptyTransport   = "0";
        strMobil            = "0";
        strJmlMobil         = "0";
        strMotor            = "0";
        strJmlMotor         = "0";
        if (emptyCheckbox.isChecked())
            strEmptyTransport   = "1";
        if (mobilCheckbox.isChecked()){
            strMobil    = "1";
            strJmlMobil = inputMobil.getText().toString().trim();
        }
        if (motorCheckbox.isChecked()){
            strMotor    = "1";
            strJmlMotor = inputMotor.getText().toString().trim();
        }
    }
    public boolean validateData() {
        boolean valid   = true;
        getInputData();
        if (strAnswer1.isEmpty() || strAnswer1.length() == 0 ) {
            inputAnswer1.setError("Jawaban tidak boleh kosong");
            YoYo.with(Techniques.Shake).duration(300).playOn(inputAnswer1);
            inputAnswer1.requestFocus();
            valid = false;
        }else {
            inputAnswer1.setError(null);
            if (strAnswer2.isEmpty() || strAnswer2.length() == 0 ) {
                inputAnswer2.setError("Jawaban tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputAnswer2);
                inputAnswer2.requestFocus();
                valid = false;
            }else {
                inputAnswer2.setError(null);
                if (strAnswer3.isEmpty() || strAnswer3.length() == 0 ) {
                    inputAnswer3.setError("Jawaban tidak boleh kosong");
                    YoYo.with(Techniques.Shake).duration(300).playOn(inputAnswer3);
                    inputAnswer3.requestFocus();
                    valid = false;
                }else {
                    inputAnswer3.setError(null);
                    if (strAnswer4.isEmpty() || strAnswer4.length() == 0 ) {
                        inputAnswer4.setError("Jawaban tidak boleh kosong");
                        YoYo.with(Techniques.Shake).duration(300).playOn(inputAnswer4);
                        inputAnswer4.requestFocus();
                        valid = false;
                    }else {
                        inputAnswer4.setError(null);
                        if (strAnswer7.isEmpty() || strAnswer7.length() == 0 ) {
                            inputAnswer7.setError("Jawaban tidak boleh kosong");
                            YoYo.with(Techniques.Shake).duration(300).playOn(inputAnswer7);
                            inputAnswer7.requestFocus();
                            valid = false;
                        }else {
                            inputAnswer7.setError(null);
                            if (strAnswer9.isEmpty() || strAnswer9.length() == 0 ) {
                                viewAnswer9.setError("Jawaban tidak boleh kosong");
                                YoYo.with(Techniques.Shake).duration(300).playOn(viewAnswer9);
                                viewAnswer9.requestFocus();
                                valid = false;
                            }else {
                                viewAnswer9.setError(null);
                            }
                        }
                    }
                }
            }
        }
        return valid;
    }
    public String getJsonString() {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("ANSWER1", strAnswer1);
            jsonObj.put("ANSWER2", strAnswer2);
            jsonObj.put("ANSWER3", strAnswer3);
            jsonObj.put("ANSWER4", strAnswer4);
            jsonObj.put("ANSWER5", strAnswer5);
            jsonObj.put("ANSWER6", strAnswer6);
            jsonObj.put("ANSWER7", strAnswer7);
            jsonObj.put("ANSWER9", strAnswer9);
            jsonObj.put("IS_EMPTY", strEmptyTransport);
            jsonObj.put("IS_MOBIL", strMobil);
            jsonObj.put("IS_MOTOR", strMotor);
            jsonObj.put("MOBIL", strJmlMobil);
            jsonObj.put("MOTOR", strJmlMotor);
            return jsonObj.toString();
        }
        catch(JSONException ex) {
            return "";
        }
    }

    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
