package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nmk.siimut.Utilities.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Warsono on 01/07/2019.
 */


@SuppressLint("Recycle")
public class SqlRegistration {
    private static String TABLE_REGISTRATION    = "MST_REGISTRATION";
    private static String FIELD_EMAIL           = "EMAIL";
    private static String FIELD_KELOMPOK        = "KELOMPOK";
    private static String FIELD_AJARAN          = "AJARAN";
    private static String FIELD_JSON_ONE        = "JSON_ONE";
    private static String FIELD_JSON_TWO        = "JSON_TWO";
    private static String FIELD_JSON_THREE      = "JSON_THREE";
    private static String FIELD_JSON_FOUR       = "JSON_FOUR";
    

    public static void onCreateTable(SQLiteDatabase db) {
        String CREATE_REGISTRATION = "CREATE TABLE " + TABLE_REGISTRATION + "("
                + FIELD_EMAIL + " TEXT PRIMARY KEY, "
                + FIELD_AJARAN + " TEXT, "
                + FIELD_KELOMPOK + " TEXT, "
                + FIELD_JSON_ONE + " TEXT, "
                + FIELD_JSON_TWO + " TEXT, "
                + FIELD_JSON_THREE + " TEXT, "
                + FIELD_JSON_FOUR + " TEXT " + ")";
        db.execSQL(CREATE_REGISTRATION);
    }
    public static void addDataRegOne(MstRegistration Data) {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_EMAIL, Data.getEmail());
        values.put(FIELD_AJARAN, Data.getAjaran());
        values.put(FIELD_KELOMPOK, Data.getGroup());
        values.put(FIELD_JSON_ONE, Data.getJsonOne());
        values.put(FIELD_JSON_TWO, Data.getJsonTwo());
        values.put(FIELD_JSON_THREE, Data.getJsonThree());
        values.put(FIELD_JSON_FOUR, Data.getJsonFour());
        db.insert(TABLE_REGISTRATION, null, values);
    }
    public static void UpdateRegOne(String strEmail, String strGroup, String JsonOne){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_KELOMPOK, strGroup);
        values.put(FIELD_JSON_ONE, JsonOne);
        db.update(TABLE_REGISTRATION, values, FIELD_EMAIL+ " = ?" ,
                new String[] {strEmail});
    }
    public static void UpdateRegTwo(String strEmail, String JsonTwo){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_JSON_TWO, JsonTwo);
        db.update(TABLE_REGISTRATION, values, FIELD_EMAIL+ " = ?" ,
                new String[] {strEmail});
    }
    public static void UpdateRegThree(String strEmail, String JsonThree){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_JSON_THREE, JsonThree);
        db.update(TABLE_REGISTRATION, values, FIELD_EMAIL+ " = ?" ,
                new String[] {strEmail});
    }
    public static void UpdateRegFour(String strEmail, String JsonFour){
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        ContentValues values 	    = new ContentValues();
        values.put(FIELD_JSON_FOUR, JsonFour);
        db.update(TABLE_REGISTRATION, values, FIELD_EMAIL+ " = ?" ,
                new String[] {strEmail});
    }
    public static List<MstRegistration> getRegistration() {
        List<MstRegistration> DataList   = new ArrayList<MstRegistration>();
        String selectQuery 			= "SELECT  * FROM " + TABLE_REGISTRATION;
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        Cursor cursor 				= db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                MstRegistration Data = new MstRegistration();
                Data.setEmail(cursor.getString(0));
                Data.setAjaran(cursor.getString(1));
                Data.setGroup(cursor.getString(2));
                Data.setJsonOne(cursor.getString(3));
                Data.setJsonTwo(cursor.getString(4));
                Data.setJsonThree(cursor.getString(5));
                Data.setJsonFour(cursor.getString(6));
                DataList.add(Data);
            } while (cursor.moveToNext());
        }
        return DataList;
    }
    public static void DeleteRegistration() {
        DatabaseHandler dataBase    = new DatabaseHandler();
        SQLiteDatabase db 		    = dataBase.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_REGISTRATION);
    }
}