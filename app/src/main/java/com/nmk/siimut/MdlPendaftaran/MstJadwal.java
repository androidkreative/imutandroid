package com.nmk.siimut.MdlPendaftaran;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstJadwal {
    @SerializedName("ID")
    String Id;

    @SerializedName("TITLE")
    String Title;

    @SerializedName("PERIODE")
    String Periode;

    @SerializedName("STATUS")
    String Status;

    public MstJadwal() {
    }

    public MstJadwal(String id, String title, String periode, String status) {
        Id          = id;
        Title       = title;
        Periode     = periode;
        Status      = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPeriode() {
        return Periode;
    }

    public void setPeriode(String periode) {
        Periode = periode;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}