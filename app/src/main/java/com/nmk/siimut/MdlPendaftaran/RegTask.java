package com.nmk.siimut.MdlPendaftaran;


import android.app.Activity;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.nmk.siimut.ApiService.ApiClient;
import com.nmk.siimut.ApiService.BasicResponse;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.FancyToast;

import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Warsono on 01/07/2019.
 */

public class RegTask {
    private static String strUserId = "";
    private static String strAuth    = "";

    static void onRegister(final Activity mContext, final Runnable SuccessAction, final Runnable FailedAction) {
        try {
            List<MstUser> dataList          = SqlUser.getAllData();
            if (dataList.size() > 0) {
                strUserId   = dataList.get(0).getUserId();
                strAuth     = dataList.get(0).getUserAuth();
            }
            List<MstRegistration> regData   = SqlRegistration.getRegistration();
            MstRegistration objData         = regData.get(0);
            PendaftaranService service      = ApiClient.getClient().create(PendaftaranService.class);
            Call<BasicResponse> mRequest    = service.postRegister(strUserId, strAuth, objData.getEmail(), objData.getAjaran(), objData.JsonOne, objData.JsonTwo, objData.JsonThree, objData.JsonFour);
            mRequest.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> Response) {
                    BasicResponse Result     = Response.body();
                    if (Result != null){
                        switch (Result.getResultState()) {
                            case 200:
                                FancyToast.makeMessage(mContext, FancyToast.SUCCESS, Result.getResultMessage(), Toast.LENGTH_SHORT).show();
                                if (SuccessAction != null)
                                    SuccessAction.run();
                                break;
                            default:
                                FancyToast.makeMessage(mContext, FancyToast.WARNING, Result.getResultMessage(), Toast.LENGTH_LONG).show();
                                if (FailedAction != null)
                                    FailedAction.run();
                                break;
                        }
                    }else{
                        if (SuccessAction != null)
                            SuccessAction.run();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                    if(t instanceof SocketTimeoutException){
                        FancyToast.makeMessage(mContext, FancyToast.INFO, mContext.getResources().getString(R.string.msg_connection_time_out), Toast.LENGTH_LONG).show();
                        if (FailedAction != null)
                            FailedAction.run();
                    }else{
//                        FancyToast.makeMessage(mContext, FancyToast.WARNING, mContext.getResources().getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
//                        if (FailedAction != null)
//                            FailedAction.run();
                        FancyToast.makeMessage(mContext, FancyToast.SUCCESS, "Pendaftaran mahasiswa baru berhasil", Toast.LENGTH_SHORT).show();
                        if (SuccessAction != null)
                            SuccessAction.run();
                    }
                }
            });
        }catch (Exception e) {
            if (SuccessAction != null)
                SuccessAction.run();
        }
    }
}
