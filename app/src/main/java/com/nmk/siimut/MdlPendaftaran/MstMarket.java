package com.nmk.siimut.MdlPendaftaran;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Warsono on 01/07/2019.
 */

public class MstMarket {
    @SerializedName("ID")
    String Id;

    @SerializedName("INFO_NAME")
    String Description;

    String Checked;

    public MstMarket() {
    }

    public MstMarket(String id, String description, String checked) {
        Id          = id;
        Description = description;
        Checked     = checked;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getChecked() {
        return Checked;
    }

    public void setChecked(String checked) {
        Checked = checked;
    }
}