package com.nmk.siimut.MdlPendaftaran;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class JadwalResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstJadwal> DataList    = new ArrayList<MstJadwal>();

    public List<MstJadwal> getDataList() {
        return DataList;
    }
}
