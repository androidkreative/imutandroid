package com.nmk.siimut.MdlPendaftaran;

import com.google.gson.annotations.SerializedName;
import com.nmk.siimut.ApiService.BasicResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

public class SyaratResponse extends BasicResponse {

    @SerializedName("DATA")
    List<MstSyarat> DataList    = new ArrayList<MstSyarat>();

    public List<MstSyarat> getDataList() {
        return DataList;
    }
}
