package com.nmk.siimut.MdlPendaftaran;


/**
 * Created by Warsono on 01/07/2019.
 */

public class MstRegistration {
    String Email;
    String Ajaran;
    String Group;
    String JsonOne;
    String JsonTwo;
    String JsonThree;
    String JsonFour;

    public MstRegistration() {
    }

    public MstRegistration(String email, String ajaran, String group, String jsonOne, String jsonTwo, String jsonThree, String jsonFour) {
        Email       = email;
        Ajaran      = ajaran;
        Group       = group;
        JsonOne     = jsonOne;
        JsonTwo     = jsonTwo;
        JsonThree   = jsonThree;
        JsonFour    = jsonFour;
    }

    public String getAjaran() {
        return Ajaran;
    }

    public void setAjaran(String ajaran) {
        Ajaran = ajaran;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGroup() {
        return Group;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public String getJsonOne() {
        return JsonOne;
    }

    public void setJsonOne(String jsonOne) {
        JsonOne = jsonOne;
    }

    public String getJsonTwo() {
        return JsonTwo;
    }

    public void setJsonTwo(String jsonTwo) {
        JsonTwo = jsonTwo;
    }

    public String getJsonThree() {
        return JsonThree;
    }

    public void setJsonThree(String jsonThree) {
        JsonThree = jsonThree;
    }

    public String getJsonFour() {
        return JsonFour;
    }

    public void setJsonFour(String jsonFour) {
        JsonFour = jsonFour;
    }
}