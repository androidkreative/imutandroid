package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.nmk.siimut.MdlAccount.User.MstUser;
import com.nmk.siimut.MdlAccount.User.MstUserProfile;
import com.nmk.siimut.MdlAccount.User.ProfileDetailActivity;
import com.nmk.siimut.MdlAccount.User.SqlUser;
import com.nmk.siimut.MdlAccount.User.UserTask;
import com.nmk.siimut.MdlFakultas.MstJurusan;
import com.nmk.siimut.MdlFakultas.SqlFakultas;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.MdlOther.MstAgama;
import com.nmk.siimut.MdlOther.MstStatus;
import com.nmk.siimut.MdlOther.SqlOther;
import com.nmk.siimut.R;
import com.nmk.siimut.UiDesign.FancyToast;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class RegMahasiswaActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, DatePickerCallback {

    private List<MstJurusan> itemJurusanList    = new ArrayList<MstJurusan>();
    private List<MstStatus> itemStatusList      = new ArrayList<MstStatus>();
    private List<MstAgama> itemAgamaList        = new ArrayList<MstAgama>();
    private Spinner inputJurusan, inputStatus;
    private Spinner inputAgama;
    private LinearLayout layoutNIM;
    private EditText inputNIM, inputName, inputNickName, inputPOB;
    private TextView viewDOB;
    private EditText inputAddressKtp, inputPostKtp;
    private EditText inputAddressSurat, inputPostSurat;
    private EditText inputPhone, inputHP;
    private String strJurusan;
    private String strId, strName, strNickName, strGender, strStatus;
    private String strAgama, strJob, strPob;
    private String strDob = "";
    private String strAddress, strPost, strAlamatSurat, strPostSurat;
    private String strPhone, strHP;
    private RadioGroup radioGender;
    private EditText inputJob;
    private RelativeLayout loadingMain;
    private MstUser objUser;

    @Override
    public void onBackPressed(){
        Intent intent   = new Intent(RegMahasiswaActivity.this, ProfileDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_crud);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        inputJurusan    = findViewById(R.id.jurusanInput);
        inputJurusan.setOnItemSelectedListener(this);
        layoutNIM       = findViewById(R.id.nimLayout);
        inputNIM        = findViewById(R.id.nimInput);
        inputName       = findViewById(R.id.completeNameInput);
        inputNickName   = findViewById(R.id.nickNameInput);
        radioGender     = findViewById(R.id.radioGroupGender);
        inputStatus     = findViewById(R.id.statusInput);
        inputStatus.setOnItemSelectedListener(this);
        inputAgama      = findViewById(R.id.agamaInput);
        inputAgama.setOnItemSelectedListener(this);
        inputJob        = findViewById(R.id.jobInput);
        inputPOB        = findViewById(R.id.pobInput);
        viewDOB         = findViewById(R.id.dobInput);
        inputAddressKtp = findViewById(R.id.addressInput);
        inputPostKtp    = findViewById(R.id.postInput);
        inputAddressSurat   = findViewById(R.id.addressSuratInput);
        inputPostSurat      = findViewById(R.id.postSuratInput);
        inputPhone      = findViewById(R.id.phoneInput);
        inputHP         = findViewById(R.id.hpInput);
        loadingMain     = findViewById(R.id.mainLoading);

        List<MstUser> dataList      = SqlUser.getAllData();
        objUser                     = dataList.get(0);
        onSetData();
    }
    private void onSetData(){
        List<MstUserProfile> dataProfile = SqlUser.getAllDataProfile();
        if (dataProfile.size() > 0) {
            MstUserProfile objProfile   = dataProfile.get(0);
            if (objProfile.getId().contentEquals(""))
                layoutNIM.setVisibility(View.VISIBLE);
            else
                layoutNIM.setVisibility(View.GONE);
            strId           = objProfile.getId();
            strName         = objProfile.getNamaLengkap();
            strNickName     = objProfile.getNamaPanggilan();
            strGender       = objProfile.getGender();
            strJurusan      = objProfile.getJurusan();
            strStatus       = objProfile.getStatusNikah();
            strAgama        = objProfile.getAgama();
            strJob          = objProfile.getPekerjaan();
            strPob          = objProfile.getTempatLahir();
            strDob          = objProfile.getTanggalLahir();
            strAddress      = objProfile.getAlamatKTP();
            strPost         = objProfile.getPostKTP();
            strAlamatSurat  = objProfile.getAlamatSurat();
            strPostSurat    = objProfile.getPostSurat();
            strPhone        = objProfile.getPhone();
            strHP           = objProfile.getHp();

            radioGender.clearCheck();
            if (strGender.contentEquals("1"))
                radioGender.check(R.id.radioPria);
            else
                radioGender.check(R.id.radioWanita);

            inputNIM.setText(strId);
            inputName.setText(strName);
            inputNickName.setText(strNickName);
            inputJob.setText(strJob);
            inputPOB.setText(strPob);
            if (!objProfile.getTanggalLahir().contentEquals("0000-00-00"))
                viewDOB.setText(Utility.DateView(strDob));
            inputAddressKtp.setText(strAddress);
            inputPostKtp.setText(strPost);
            inputAddressSurat.setText(strAlamatSurat);
            inputPostSurat.setText(strPostSurat);
            inputPhone.setText(strPhone);
            inputHP.setText(strHP);
        }
        onSetSpinnerJurusan();
        onSetSpinnerStatus();
        onSetSpinnerAgama();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnSubmit:
                onClickSave();
                break;
            case R.id.dobInput:
                onClickBod();
                break;
        }
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()) {
            case R.id.jurusanInput:
                strJurusan  = itemJurusanList.get(position).getJurusanId();
                break;
            case R.id.statusInput:
                strStatus   = itemStatusList.get(position).getId();
                break;
            case R.id.agamaInput:
                strAgama   = itemAgamaList.get(position).getId();
                break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    private void onClickBod(){
        Calendar currentDate    = Calendar.getInstance();
        Calendar maxDate        = Calendar.getInstance();
        long selectDateLong;
        if (!viewDOB.getText().toString().contentEquals("")){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Date date       = dateFormat.parse(viewDOB.getText().toString());
                selectDateLong  = date.getTime();
            } catch (ParseException e) {
                selectDateLong    = currentDate.getTimeInMillis();
            }
        }else
            selectDateLong    = currentDate.getTimeInMillis();

        maxDate.set(currentDate.get(Calendar.YEAR)-15, currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH));
        DatePickerFragmentDialog.newInstance(
                DateTimeBuilder.newInstance()
                        .withTime(false)
                        .withSelectedDate(selectDateLong)
                        .withMaxDate(maxDate.getTimeInMillis()))
                .show(getSupportFragmentManager(), "DatePickerFragmentDialog");
    }
    public void onDateSet(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        strDob  = dateFormat.format(new Date(date));
        viewDOB.setText(Utility.DateView(strDob));
    }
    private void onSetSpinnerJurusan() {
        if (itemJurusanList.size() > 0)
            itemJurusanList.clear();
        itemJurusanList.add(new MstJurusan("", getResources().getString(R.string.label_pilih_jurusan), "", ""));
        List<MstJurusan> data   = SqlFakultas.getAllJurusan();
        int count               = data.size();
        if (count > 0){
            for (int i=0; i<count; i++){
                itemJurusanList.add(data.get(i));
            }
        }
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemJurusanList.size();
            }

            @Override
            public String get(int location) {
                return itemJurusanList.get(location).getJurusanName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputJurusan.setAdapter(dataAdapter);
        int mPos = 0;
        if (strJurusan != null && !strJurusan.contentEquals("")) {
            int countData = itemJurusanList.size();
            for (int i=0; i<countData; i++) {
                MstJurusan objData = itemJurusanList.get(i);
                if (objData.getJurusanId().contentEquals(strJurusan))
                    mPos = i;
            }
        }
        inputJurusan.setSelection(mPos);
    }
    private void onSetSpinnerStatus() {
        if (itemStatusList.size() > 0)
            itemStatusList.clear();
        itemStatusList.add(new MstStatus("", getResources().getString(R.string.label_pilih_status)));
        List<MstStatus> data    = SqlOther.getAllStatus();
        int count               = data.size();
        if (count > 0){
            for (int i=0; i<count; i++){
                itemStatusList.add(data.get(i));
            }
        }
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemStatusList.size();
            }

            @Override
            public String get(int location) {
                return itemStatusList.get(location).getStatusName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputStatus.setAdapter(dataAdapter);
        int mPos = 0;
        if (strStatus != null && !strStatus.contentEquals("")) {
            int countData = itemStatusList.size();
            for (int i=0; i<countData; i++) {
                MstStatus objData = itemStatusList.get(i);
                if (objData.getId().contentEquals(strStatus))
                    mPos = i;
            }
        }
        inputStatus.setSelection(mPos);
    }
    private void onSetSpinnerAgama() {
        if (itemAgamaList.size() > 0)
            itemAgamaList.clear();
        itemAgamaList.add(new MstAgama("", getResources().getString(R.string.label_pilih_agama)));
        List<MstAgama> data     = SqlOther.getAllAgama();
        int count               = data.size();
        if (count > 0){
            for (int i=0; i<count; i++){
                itemAgamaList.add(data.get(i));
            }
        }
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemAgamaList.size();
            }

            @Override
            public String get(int location) {
                return itemAgamaList.get(location).getAgamaName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputAgama.setAdapter(dataAdapter);
        int mPos = 0;
        if (strAgama != null && !strAgama.contentEquals("")) {
            int countData = itemAgamaList.size();
            for (int i=0; i<countData; i++) {
                MstAgama objData = itemAgamaList.get(i);
                if (objData.getId().contentEquals(strAgama))
                    mPos = i;
            }
        }
        inputAgama.setSelection(mPos);
    }
    public boolean validateData() {
        boolean valid   = true;
        int radioGenId = radioGender.getCheckedRadioButtonId();
        if (radioGenId == R.id.radioPria) strGender = "1";
        else strGender = "2";

        strId           = inputNIM.getText().toString().trim();
        strName         = inputName.getText().toString().trim();
        strNickName     = inputNickName.getText().toString().trim();
        strPob          = inputPOB.getText().toString().trim();
        strAddress      = inputAddressKtp.getText().toString().trim();
        strPost         = inputPostKtp.getText().toString().trim();
        strAlamatSurat  = inputAddressSurat.getText().toString().trim();
        strPostSurat    = inputPostSurat.getText().toString().trim();
        strPhone        = inputPhone.getText().toString().trim();
        strHP           = inputHP.getText().toString().trim();
        strJob          = inputJob.getText().toString().trim();

        if (objUser.getUserType().contentEquals("2")){
            if (strId.isEmpty() || strId.length() == 0 ) {
                inputNIM.setError("NIM tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputNIM);
                inputNIM.requestFocus();
                valid = false;
            }else
                inputNIM.setError(null);
        }
        if (strJurusan.isEmpty() || strJurusan.length() == 0 ) {
            FancyToast.makeMessage(this, FancyToast.WARNING, "Jurusan belum dipilih", Toast.LENGTH_LONG).show();
            YoYo.with(Techniques.Shake).duration(300).playOn(inputJurusan);
            inputJurusan.requestFocus();
            valid = false;
        }else {
            if (strName.isEmpty() || strName.length() == 0 ) {
                inputName.setError("Nama tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputName);
                inputName.requestFocus();
                valid = false;
            }else {
                inputName.setError(null);
                if (strStatus.isEmpty() || strStatus.length() == 0 ) {
                    FancyToast.makeMessage(this, FancyToast.WARNING, "Status perkawinan belum dipilih", Toast.LENGTH_LONG).show();
                    YoYo.with(Techniques.Shake).duration(300).playOn(inputStatus);
                    inputStatus.requestFocus();
                    valid = false;
                }else {
                    if (strAgama.isEmpty() || strAgama.length() == 0 ) {
                        FancyToast.makeMessage(this, FancyToast.WARNING, "Agama belum dipilih", Toast.LENGTH_LONG).show();
                        YoYo.with(Techniques.Shake).duration(300).playOn(inputAgama);
                        inputAgama.requestFocus();
                        valid = false;
                    }else {
                        if (strJob.isEmpty() || strJob.length() == 0 ) {
                            inputJob.setError("Pekerjaan tidak boleh kosong");
                            YoYo.with(Techniques.Shake).duration(300).playOn(inputJob);
                            inputJob.requestFocus();
                            valid = false;
                        }else {
                            inputJob.setError(null);
                            if (strPob.isEmpty() || strPob.length() == 0 ) {
                                inputPOB.setError("Tempat lahir tidak boleh kosong");
                                YoYo.with(Techniques.Shake).duration(300).playOn(inputPOB);
                                inputPOB.requestFocus();
                                valid = false;
                            }else {
                                inputPOB.setError(null);
                                if (strDob.isEmpty() || strDob.length() == 0 ) {
                                    viewDOB.setError("Tanggal lahir tidak boleh kosong");
                                    YoYo.with(Techniques.Shake).duration(300).playOn(viewDOB);
                                    viewDOB.requestFocus();
                                    valid = false;
                                }else {
                                    viewDOB.setError(null);
                                    if (strAddress.isEmpty() || strAddress.length() == 0 ) {
                                        inputAddressKtp.setError("Alamat tidak boleh kosong");
                                        YoYo.with(Techniques.Shake).duration(300).playOn(inputAddressKtp);
                                        inputAddressKtp.requestFocus();
                                        valid = false;
                                    }else {
                                        inputAddressKtp.setError(null);
                                        if (strAlamatSurat.isEmpty() || strAlamatSurat.length() == 0 ) {
                                            inputAddressSurat.setError("Alamat tidak boleh kosong");
                                            YoYo.with(Techniques.Shake).duration(300).playOn(inputAddressSurat);
                                            inputAddressSurat.requestFocus();
                                            valid = false;
                                        }else {
                                            inputAddressSurat.setError(null);
                                            if (strHP.isEmpty() || strHP.length() == 0 ) {
                                                inputHP.setError("No HP tidak boleh kosong");
                                                YoYo.with(Techniques.Shake).duration(300).playOn(inputHP);
                                                inputHP.requestFocus();
                                                valid = false;
                                            }else {
                                                inputHP.setError(null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return valid;
    }
    public String getJsonString() {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("ID", strId);
            jsonObj.put("JURUSAN", strJurusan);
            jsonObj.put("NAME", strName);
            jsonObj.put("NICKNAME", strNickName);
            jsonObj.put("GENDER", strGender);
            jsonObj.put("STATUS", strStatus);
            jsonObj.put("AGAMA", strAgama);
            jsonObj.put("JOB", strJob);
            jsonObj.put("POB", strPob);
            jsonObj.put("DOB", strDob);
            jsonObj.put("ADDRESS", strAddress);
            jsonObj.put("POST", strPost);
            jsonObj.put("ADDRESS_SURAT", strAlamatSurat);
            jsonObj.put("POST_SURAT", strPostSurat);
            jsonObj.put("PHONE", strPhone);
            jsonObj.put("HP", strHP);
            return jsonObj.toString();
        }
        catch(JSONException ex) {
            return "";
        }
    }
    private void onClickSave(){
        if (validateData()){
            if (Utility.isNetworkConnected()) {
                onShowLoading.run();
                inputHP.requestFocus();
                String strJsonProfile   = getJsonString();
                UserTask.postUpdateProfile(RegMahasiswaActivity.this, strJsonProfile, onGetProfile, onHideLoading);
            }else
                Dialog.InformationDialog(RegMahasiswaActivity.this,
                        getResources().getString(R.string.label_warning),
                        getResources().getString(R.string.msg_conection_unavailable),
                        getResources().getString(R.string.btn_close),
                        null);
        }
    }
    private Runnable onGetProfile = new Runnable() {
        @Override
        public void run() {
            UserTask.onGetProfile(RegMahasiswaActivity.this, onUpdateSuccess, onHideLoading);
        }
    };
    private Runnable onUpdateSuccess = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(RegMahasiswaActivity.this, ProfileDetailActivity.class);
            startActivity(intent);
        }
    };
    private Runnable onShowLoading = new Runnable() {
        @Override
        public void run() {
            loadingMain.setVisibility(View.VISIBLE);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
            loadingMain.setVisibility(View.GONE);
        }
    };


    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
