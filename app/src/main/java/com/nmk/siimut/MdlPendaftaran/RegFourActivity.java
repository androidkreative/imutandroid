package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nmk.siimut.GlobalApplication;
import com.nmk.siimut.MdlAccount.User.SignInActivity;
import com.nmk.siimut.MdlAccount.User.UserTask;
import com.nmk.siimut.MdlMain.MainActivity;
import com.nmk.siimut.R;
import com.nmk.siimut.Utilities.Constants;
import com.nmk.siimut.Utilities.Dialog;
import com.nmk.siimut.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class RegFourActivity extends AppCompatActivity implements View.OnClickListener{

    private List<MstMarket> dataList   = new ArrayList<MstMarket>();
    private RecyclerView marketing1View;
    private RadioGroup radioMkt2, radioMkt3, radioMkt4;
    private EditText inputMkt2Yes, inputMkt2No;
    private EditText inputMkt3Yes, inputMkt3No, inputMkt4No;
    private RelativeLayout loadingMain;
    private String strEmail;
    private String strMkt2Yes, strMkt2No, strMkt3Yes, strMkt3No, strMkt4No;
    private String strMkt1, strMkt2, strMkt3, strMkt4;
    private dataAdapter adapter;

    @Override
    public void onBackPressed(){
        getInputData();
        String strJsonFour  = getJsonString();
        SqlRegistration.UpdateRegFour(strEmail, strJsonFour);
        Intent intent       = new Intent(RegFourActivity.this, RegThreeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_four);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        marketing1View  = findViewById(R.id.viewMarketing1);
        marketing1View.setNestedScrollingEnabled(false);
        marketing1View.setLayoutManager(new GridLayoutManager(GlobalApplication.getContext(), 2));
        radioMkt2       = findViewById(R.id.radioMarketing2);
        radioMkt3       = findViewById(R.id.radioMarketing3);
        radioMkt4       = findViewById(R.id.radioMarketing4);
        inputMkt2Yes    = findViewById(R.id.mkt2YesInput);
        inputMkt2No     = findViewById(R.id.mkt2NoInput);
        inputMkt3Yes    = findViewById(R.id.mkt3YesInput);
        inputMkt3No     = findViewById(R.id.mkt3NoInput);
        inputMkt4No     = findViewById(R.id.mkt4NoInput);
        loadingMain     = findViewById(R.id.mainLoading);

        radioMkt2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioYes2) {
                    inputMkt2Yes.setVisibility(View.VISIBLE);
                    inputMkt2No.setVisibility(View.GONE);
                    inputMkt2No.setText("");
                }else {
                    inputMkt2Yes.setVisibility(View.GONE);
                    inputMkt2No.setVisibility(View.VISIBLE);
                    inputMkt2Yes.setText("");
                }
            }
        });
        radioMkt3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioYes3) {
                    inputMkt3Yes.setVisibility(View.VISIBLE);
                    inputMkt3No.setVisibility(View.GONE);
                    inputMkt3No.setText("");
                }else {
                    inputMkt3Yes.setVisibility(View.GONE);
                    inputMkt3No.setVisibility(View.VISIBLE);
                    inputMkt3Yes.setText("");
                }
            }
        });
        radioMkt4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioYes4) {
                    inputMkt4No.setVisibility(View.GONE);
                    inputMkt3No.setText("");
                }else
                    inputMkt4No.setVisibility(View.VISIBLE);
            }
        });
        onSetDataMarketing();
    }
    private void onSetDataMarketing(){
        List<MstRegistration> regList = SqlRegistration.getRegistration();
        if (regList.size() > 0){
            MstRegistration objReg  = regList.get(0);
            strEmail                = objReg.getEmail();
            try {
                JSONObject jObj = new JSONObject(objReg.getJsonFour());
                strMkt1     = jObj.getString("MKT1");
                strMkt2     = jObj.getString("IS_MKT2");
                strMkt2Yes  = jObj.getString("MKT2_YES");
                strMkt2No   = jObj.getString("MKT2_NO");
                strMkt3     = jObj.getString("IS_MKT3");
                strMkt3Yes  = jObj.getString("MKT3_YES");
                strMkt3No   = jObj.getString("MKT3_NO");
                strMkt4     = jObj.getString("IS_MKT4");
                strMkt4No   = jObj.getString("MKT4_NO");

                radioMkt2.clearCheck();
                if (strMkt2.contentEquals("1"))
                    radioMkt2.check(R.id.radioYes2);
                else
                    radioMkt2.check(R.id.radioNo2);

                radioMkt3.clearCheck();
                if (strMkt3.contentEquals("1"))
                    radioMkt3.check(R.id.radioYes3);
                else
                    radioMkt3.check(R.id.radioNo3);

                radioMkt4.clearCheck();
                if (strMkt4.contentEquals("1"))
                    radioMkt4.check(R.id.radioYes4);
                else
                    radioMkt4.check(R.id.radioNo4);
                inputMkt2Yes.setText(strMkt2Yes);
                inputMkt2No.setText(strMkt2No);
                inputMkt3Yes.setText(strMkt3Yes);
                inputMkt3No.setText(strMkt3No);
                inputMkt4No.setText(strMkt4No);
            } catch (JSONException e) {}
        }
        if (dataList.size() > 0)
            dataList.clear();
        dataList = SqlDaftar.getMarket();
        if (dataList.size() > 0){
            adapter   = new dataAdapter(dataList);
            marketing1View.setAdapter(adapter);
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPrev:
                onBackPressed();
                break;
            case R.id.btnFinish:
                if (validateData()){
                    loadingMain.setVisibility(View.VISIBLE);
                    inputMkt3No.requestFocus();

                    String strJsonFour   = getJsonString();
                    SqlRegistration.UpdateRegFour(strEmail, strJsonFour);

                    if (Utility.isNetworkConnected())
                        RegTask.onRegister(RegFourActivity.this, onGetProfile, onHideLoading);
                    else
                        Dialog.InformationDialog(RegFourActivity.this,
                                getResources().getString(R.string.label_warning),
                                getResources().getString(R.string.msg_conection_unavailable),
                                getResources().getString(R.string.btn_close),
                                onHideLoading);
                }
                break;
        }
    }
    class dataAdapter extends RecyclerView.Adapter<dataAdapter.ViewHolder> {
        private List<MstMarket> mItems;

        private dataAdapter(List<MstMarket> itemData) {
            super();
            mItems          = itemData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_market, viewGroup, false);
            return new ViewHolder(v);
        }
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            MstMarket objectData   = mItems.get(position);
            viewHolder.checkItem.setText(objectData.getDescription());
            if (objectData.getChecked().contentEquals("1"))
                viewHolder.checkItem.setChecked(true);
            else
                viewHolder.checkItem.setChecked(false);

            viewHolder.checkItem.setTag(position);
            viewHolder.checkItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer pos = (Integer) viewHolder.checkItem.getTag();
                    if (mItems.get(pos).getChecked().contentEquals("1")) {
                        viewHolder.checkItem.setChecked(false);
                        SqlDaftar.UpdateMarketChecked(objectData.getId(), "0");
                    }else {
                        viewHolder.checkItem.setChecked(true);
                        SqlDaftar.UpdateMarketChecked(objectData.getId(), "1");
                    }
                }
            });
            viewHolder.layoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer pos = (Integer) viewHolder.checkItem.getTag();
                    if (mItems.get(pos).getChecked().contentEquals("1")) {
                        viewHolder.checkItem.setChecked(false);
                        SqlDaftar.UpdateMarketChecked(objectData.getId(), "0");
                    }else {
                        viewHolder.checkItem.setChecked(true);
                        SqlDaftar.UpdateMarketChecked(objectData.getId(), "1");
                    }
                }
            });
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
        private class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutItem;
            CheckBox checkItem;

            private ViewHolder(View itemView) {
                super(itemView);
                layoutItem      = itemView.findViewById(R.id.itemLayout);
                checkItem       = itemView.findViewById(R.id.itemCheck);
            }
        }
        public String getSelectedMarket(List<MstMarket> marketList) {
            StringBuilder selectedArray = new StringBuilder();
            int countItem = marketList.size();
            for (int i = 0; i < countItem; i++) {
                MstMarket objMarket    = marketList.get(i);
                if (objMarket.getChecked().contentEquals("1")){
                    if (selectedArray.length() == 0)
                        selectedArray.append(objMarket.getId());
                    else
                        selectedArray.append(",").append(objMarket.getId());
                }
            }
            return selectedArray.toString();
        }
    }

    private void getInputData(){
        if (dataList.size() > 0)
            dataList.clear();
        dataList        = SqlDaftar.getMarket();
        strMkt1         = adapter.getSelectedMarket(dataList);
        strMkt2Yes      = inputMkt2Yes.getText().toString().trim();
        strMkt2No       = inputMkt2No.getText().toString().trim();
        strMkt3Yes      = inputMkt3Yes.getText().toString().trim();
        strMkt3No       = inputMkt3No.getText().toString().trim();
        strMkt4No       = inputMkt4No.getText().toString().trim();

        int idMkt2   = radioMkt2.getCheckedRadioButtonId();
        if (idMkt2 == R.id.radioYes2) strMkt2 = "1";
        else strMkt2 = "0";

        int idMkt3   = radioMkt3.getCheckedRadioButtonId();
        if (idMkt3 == R.id.radioYes3) strMkt3 = "1";
        else strMkt3 = "0";

        int idMkt4   = radioMkt4.getCheckedRadioButtonId();
        if (idMkt4 == R.id.radioYes4) strMkt4 = "1";
        else strMkt4 = "0";
    }
    public boolean validateData() {
        boolean valid   = true;
        getInputData();
        if (strMkt2.contentEquals("1")){
            if (strMkt2Yes.isEmpty() || strMkt2Yes.length() == 0 ) {
                inputMkt2Yes.setError("Tanggal tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputMkt2Yes);
                inputMkt2Yes.requestFocus();
                valid = false;
            }else
                inputMkt2Yes.setError(null);
        }else{
            if (strMkt2No.isEmpty() || strMkt2No.length() == 0 ) {
                inputMkt2No.setError("Alasan tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputMkt2No);
                inputMkt2No.requestFocus();
                valid = false;
            }else
                inputMkt2No.setError(null);
        }
        if (strMkt3.contentEquals("1")){
            if (strMkt3Yes.isEmpty() || strMkt3Yes.length() == 0 ) {
                inputMkt3Yes.setError("Nomor WA tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputMkt3Yes);
                inputMkt3Yes.requestFocus();
                valid = false;
            }else
                inputMkt3Yes.setError(null);
        }else{
            if (strMkt3No.isEmpty() || strMkt3No.length() == 0 ) {
                inputMkt3No.setError("Alasan tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputMkt3No);
                inputMkt3No.requestFocus();
                valid = false;
            }else
                inputMkt3No.setError(null);
        }
        if (strMkt4.contentEquals("0")){
            if (strMkt4No.isEmpty() || strMkt3No.length() == 0 ) {
                inputMkt4No.setError("Alasan tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputMkt4No);
                inputMkt4No.requestFocus();
                valid = false;
            }else
                inputMkt4No.setError(null);
        }
        return valid;
    }
    public String getJsonString() {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("MKT1", strMkt1);
            jsonObj.put("IS_MKT2", strMkt2);
            jsonObj.put("MKT2_YES", strMkt2Yes);
            jsonObj.put("MKT2_NO", strMkt2No);
            jsonObj.put("IS_MKT3", strMkt3);
            jsonObj.put("MKT3_YES", strMkt3Yes);
            jsonObj.put("MKT3_NO", strMkt3No);
            jsonObj.put("IS_MKT4", strMkt4);
            jsonObj.put("MKT4_NO", strMkt4No);
            return jsonObj.toString();
        }
        catch(JSONException ex) {
            return "";
        }
    }
    private Runnable onGetProfile = new Runnable() {
        @Override
        public void run() {
            UserTask.onGetProfile(RegFourActivity.this, onRegisSuccess, onRegisSuccess);
        }
    };
    private Runnable onRegisSuccess = new Runnable() {
        @Override
        public void run() {
            SqlDaftar.UpdateMarketUnChecked();
            SqlRegistration.DeleteRegistration();
            Intent intent = new Intent(RegFourActivity.this, MainActivity.class);
            intent.putExtra(Constants.VIEW_KEY, Constants.VIEW_PMB);
            startActivity(intent);
        }
    };
    private Runnable onHideLoading = new Runnable() {
        @Override
        public void run() {
            loadingMain.setVisibility(View.GONE);
        }
    };




    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
