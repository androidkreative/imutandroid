package com.nmk.siimut.MdlPendaftaran;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nmk.siimut.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Warsono on 01/07/2019.
 */

@SuppressLint("StaticFieldLeak")
public class RegTwoActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    private List<ObjGeneral> itemBiayaList     = new ArrayList<ObjGeneral>();
    private Spinner inputBiaya;
    private LinearLayout smuLayout, d3Layout;
    private EditText inputNamaSmu, inputLulusSmu, inputNilaiSmu;
    private EditText inputJurusanSmu, inputAlamatSmu;
    private EditText inputNamaD3, inputLulusD3, inputNilaiD3;
    private EditText inputJurusanD3, inputKonsenD3, inputAlamatD3;
    private EditText inputCompany, inputJabatan, inputAddress;
    private EditText inputKodePos, inputPhone;
    private RadioGroup radioPengembangan, radioPendidikan;
    private LinearLayout kembangLayout, didikLayout;
    private EditText inputKaliKembang, inputKaliDidik;
    private String strNamaSekolah, strTahunLulus, strNilai, strJurusan, strAlamatSekolah;
    private String strKonsentrasi = "";
    private String strNamaCompany, strJabatan, strAlamat, strKodePos, strPhone;
    private String strBiaya, strKembang, strDidik;
    private String strKaliKembang, strKaliDidik;
    private RelativeLayout loadingMain;
    private String strGroup, strEmail;


    @Override
    public void onBackPressed(){
        getInputData();
        String strJsonTwo   = getJsonString();
        SqlRegistration.UpdateRegTwo(strEmail, strJsonTwo);
        Intent intent       = new Intent(RegTwoActivity.this, RegOneActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_two);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        smuLayout       =  findViewById(R.id.layoutLulusanSMU);
        d3Layout        =  findViewById(R.id.layoutLulusanD3);
        inputNamaSmu    = findViewById(R.id.namaSekolahInput);
        inputLulusSmu   = findViewById(R.id.tahunLulusSmuInput);
        inputNilaiSmu   = findViewById(R.id.nilaiSmuInput);
        inputJurusanSmu = findViewById(R.id.jurusanSmuInput);
        inputAlamatSmu  = findViewById(R.id.alamatSekolahInput);
        inputNamaD3     = findViewById(R.id.namaPTNInput);
        inputLulusD3    = findViewById(R.id.tahunLulusInput);
        inputNilaiD3    = findViewById(R.id.ipkInput);
        inputJurusanD3  = findViewById(R.id.jurusanInput);
        inputKonsenD3   = findViewById(R.id.konsentrasiInput);
        inputAlamatD3   = findViewById(R.id.alamatPTNInput);
        inputCompany    = findViewById(R.id.companyInput);
        inputJabatan    = findViewById(R.id.jabatanInput);
        inputAddress    = findViewById(R.id.alamatCompanyInput);
        inputKodePos    = findViewById(R.id.postInput);
        inputPhone      = findViewById(R.id.phoneInput);
        inputBiaya          =  findViewById(R.id.biayaInput);
        inputBiaya.setOnItemSelectedListener(this);
        radioPengembangan   = findViewById(R.id.radioPengembangan);
        kembangLayout       =  findViewById(R.id.layoutKembangAngsur);
        kembangLayout.setVisibility(View.GONE);
        inputKaliKembang    = findViewById(R.id.kaliKembangInput);
        radioPendidikan     = findViewById(R.id.radioPendidikan);
        didikLayout         =  findViewById(R.id.layoutDidikAngsur);
        didikLayout.setVisibility(View.GONE);
        inputKaliDidik      = findViewById(R.id.kaliDidikInput);
        loadingMain         = findViewById(R.id.mainLoading);

        radioPengembangan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioKembangAngsur)
                    kembangLayout.setVisibility(View.VISIBLE);
                else {
                    kembangLayout.setVisibility(View.GONE);
                    inputKaliKembang.setText("");
                }
            }
        });
        radioPendidikan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioDidikAngsur)
                    didikLayout.setVisibility(View.VISIBLE);
                else {
                    didikLayout.setVisibility(View.GONE);
                    inputKaliDidik.setText("");
                }
            }
        });

        onSetData();
    }
    private void onSetData(){
        List<MstRegistration> regList = SqlRegistration.getRegistration();
        if (regList.size() > 0){
            MstRegistration objReg  = regList.get(0);
            strEmail                = objReg.getEmail();
            strGroup                = objReg.getGroup();
            try {
                JSONObject jObj = new JSONObject(objReg.getJsonTwo());
                strNamaSekolah  = jObj.getString("NAMA_SEKOLAH");
                strTahunLulus   = jObj.getString("TAHUN_LULUS");
                strNilai        = jObj.getString("NILAI");
                strJurusan      = jObj.getString("JURUSAN");
                strAlamatSekolah= jObj.getString("ALAMAT_SEKOLAH");
                strNamaCompany  = jObj.getString("COMPANY");
                strJabatan      = jObj.getString("JABATAN");
                strAlamat       = jObj.getString("ALAMAT_COMPANY");
                strKodePos      = jObj.getString("KODE_POS");
                strPhone        = jObj.getString("PHONE");
                strBiaya        = jObj.getString("BIAYA");
                strKembang      = jObj.getString("PENGEMBANGAN");
                strKaliKembang  = jObj.getString("ANGSUR_KEMBANG");
                strDidik        = jObj.getString("PENDIDIKAN");
                strKaliDidik    = jObj.getString("ANGSUR_DIDIK");

                radioPengembangan.clearCheck();
                if (strKembang.contentEquals("0"))
                    radioPengembangan.check(R.id.radioKembangTunai);
                else
                    radioPengembangan.check(R.id.radioKembangAngsur);

                radioPendidikan.clearCheck();
                if (strDidik.contentEquals("0"))
                    radioPendidikan.check(R.id.radioDidikTunai);
                else
                    radioPendidikan.check(R.id.radioDidikAngsur);

                if (strGroup.contentEquals("0")){
                    inputNamaSmu.setText(strNamaSekolah);
                    inputLulusSmu.setText(strTahunLulus);
                    inputNilaiSmu.setText(strNilai);
                    inputJurusanSmu.setText(strJurusan);
                    inputAlamatSmu.setText(strAlamatSekolah);
                    inputNamaD3.setText("");
                    inputLulusD3.setText("");
                    inputNilaiD3.setText("");
                    inputJurusanD3.setText("");
                    inputKonsenD3.setText("");
                    inputAlamatD3.setText("");
                }else
                {
                    strKonsentrasi  = jObj.getString("KONSENTRASI");
                    inputNamaSmu.setText("");
                    inputLulusSmu.setText("");
                    inputNilaiSmu.setText("");
                    inputJurusanSmu.setText("");
                    inputAlamatSmu.setText("");
                    inputNamaD3.setText(strNamaSekolah);
                    inputLulusD3.setText(strTahunLulus);
                    inputNilaiD3.setText(strNilai);
                    inputJurusanD3.setText(strJurusan);
                    inputKonsenD3.setText(strKonsentrasi);
                    inputAlamatD3.setText(strAlamatSekolah);
                }
                inputCompany.setText(strNamaCompany);
                inputJabatan.setText(strJabatan);
                inputAddress.setText(strAlamat);
                inputKodePos.setText(strKodePos);
                inputPhone.setText(strPhone);
                inputKaliKembang.setText(strKaliKembang);
                inputKaliDidik.setText(strKaliDidik);
            } catch (JSONException e) {}
        }
        if (strGroup.contentEquals("0")){
            smuLayout.setVisibility(View.VISIBLE);
            d3Layout.setVisibility(View.GONE);
        }else
        {
            smuLayout.setVisibility(View.GONE);
            d3Layout.setVisibility(View.VISIBLE);
        }
        onSetSpinnerBiaya();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPrev:
                onBackPressed();
                break;
            case R.id.btnNext:
                if (validateData()){
                    loadingMain.setVisibility(View.VISIBLE);
                    inputKaliDidik.requestFocus();

                    String strJsonTwo   = getJsonString();
                    SqlRegistration.UpdateRegTwo(strEmail, strJsonTwo);

                    Intent intent   = new Intent(RegTwoActivity.this, RegThreeActivity.class);
                    startActivity(intent);
                    loadingMain.setVisibility(View.GONE);
                }
                break;
        }
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()) {
            case R.id.biayaInput:
                strBiaya  = itemBiayaList.get(position).getId();
                break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    private void onSetSpinnerBiaya() {
        if (itemBiayaList.size() > 0)
            itemBiayaList.clear();
        itemBiayaList.add(new ObjGeneral("1", "Orang Tua/Wali"));
        itemBiayaList.add(new ObjGeneral("2", "Biaya Sendiri"));
        itemBiayaList.add(new ObjGeneral("3", "Beasiswa/Perusahaan"));
        itemBiayaList.add(new ObjGeneral("4", "Kombinasi"));
        final List<String> typeNameList = new AbstractList<String>() {
            @Override
            public int size() {
                return itemBiayaList.size();
            }

            @Override
            public String get(int location) {
                return itemBiayaList.get(location).getName();
            }
        };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_custom, typeNameList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom);
        inputBiaya.setAdapter(dataAdapter);
        int mPos = 0;
        if (strBiaya != null && !strBiaya.contentEquals("")) {
            int countData = itemBiayaList.size();
            for (int i=0; i<countData; i++) {
                ObjGeneral objData = itemBiayaList.get(i);
                if (objData.getId().contentEquals(strBiaya))
                    mPos = i;
            }
        }
        inputBiaya.setSelection(mPos);
    }
    private void getInputData(){
        if (strGroup.contentEquals("0")){
            strNamaSekolah      = inputNamaSmu.getText().toString().trim();
            strTahunLulus       = inputLulusSmu.getText().toString().trim();
            strNilai            = inputNilaiSmu.getText().toString().trim();
            strJurusan          = inputJurusanSmu.getText().toString().trim();
            strAlamatSekolah    = inputAlamatSmu.getText().toString().trim();
        }else
        {
            strNamaSekolah      = inputNamaD3.getText().toString().trim();
            strTahunLulus       = inputLulusD3.getText().toString().trim();
            strNilai            = inputNilaiD3.getText().toString().trim();
            strJurusan          = inputJurusanD3.getText().toString().trim();
            strKonsentrasi      = inputKonsenD3.getText().toString().trim();
            strAlamatSekolah    = inputAlamatD3.getText().toString().trim();
        }
        strNamaCompany      = inputCompany.getText().toString().trim();
        strJabatan          = inputJabatan.getText().toString().trim();
        strAlamat           = inputAddress.getText().toString().trim();
        strKodePos          = inputKodePos.getText().toString().trim();
        strPhone            = inputPhone.getText().toString().trim();

        int idKembang   = radioPengembangan.getCheckedRadioButtonId();
        if (idKembang == R.id.radioKembangTunai) strKembang = "0";
        else strKembang = "1";

        int idDidik     = radioPendidikan.getCheckedRadioButtonId();
        if (idDidik == R.id.radioDidikTunai) strDidik = "0";
        else strDidik = "1";

        strKaliKembang  = inputKaliKembang.getText().toString().trim();
        strKaliDidik    = inputKaliDidik.getText().toString().trim();
    }
    public boolean validateData() {
        boolean valid   = true;
        getInputData();
        if (strGroup.contentEquals("0")){
            if (strNamaSekolah.isEmpty() || strNamaSekolah.length() == 0 ) {
                inputNamaSmu.setError("Nama sekolah tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputNamaSmu);
                inputNamaSmu.requestFocus();
                valid = false;
            }else {
                inputNamaSmu.setError(null);
                if (strTahunLulus.isEmpty() || strTahunLulus.length() == 0 ) {
                    inputLulusSmu.setError("Tahun lulus tidak boleh kosong");
                    YoYo.with(Techniques.Shake).duration(300).playOn(inputLulusSmu);
                    inputLulusSmu.requestFocus();
                    valid = false;
                }else {
                    inputLulusSmu.setError(null);
                    if (strNilai.isEmpty() || strNilai.length() == 0 ) {
                        inputNilaiSmu.setError("Nilai tidak boleh kosong");
                        YoYo.with(Techniques.Shake).duration(300).playOn(inputNilaiSmu);
                        inputNilaiSmu.requestFocus();
                        valid = false;
                    }else {
                        inputNilaiSmu.setError(null);
                        if (strJurusan.isEmpty() || strJurusan.length() == 0 ) {
                            inputJurusanSmu.setError("Jurusan tidak boleh kosong");
                            YoYo.with(Techniques.Shake).duration(300).playOn(inputJurusanSmu);
                            inputJurusanSmu.requestFocus();
                            valid = false;
                        }else {
                            inputJurusanSmu.setError(null);
                            if (strAlamatSekolah.isEmpty() || strAlamatSekolah.length() == 0 ) {
                                inputAlamatSmu.setError("Alamat sekolah tidak boleh kosong");
                                YoYo.with(Techniques.Shake).duration(300).playOn(inputAlamatSmu);
                                inputAlamatSmu.requestFocus();
                                valid = false;
                            }else {
                                inputAlamatSmu.setError(null);
                            }
                        }
                    }
                }
            }
        }else{
            if (strNamaSekolah.isEmpty() || strNamaSekolah.length() == 0 ) {
                inputNamaD3.setError("Nama kampus tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputNamaD3);
                inputNamaD3.requestFocus();
                valid = false;
            }else {
                inputNamaD3.setError(null);
                if (strTahunLulus.isEmpty() || strTahunLulus.length() == 0 ) {
                    inputLulusD3.setError("Tahun lulus tidak boleh kosong");
                    YoYo.with(Techniques.Shake).duration(300).playOn(inputLulusD3);
                    inputLulusD3.requestFocus();
                    valid = false;
                }else {
                    inputLulusD3.setError(null);
                    if (strNilai.isEmpty() || strNilai.length() == 0 ) {
                        inputNilaiD3.setError("Nilai tidak boleh kosong");
                        YoYo.with(Techniques.Shake).duration(300).playOn(inputNilaiD3);
                        inputNilaiD3.requestFocus();
                        valid = false;
                    }else {
                        inputNilaiD3.setError(null);
                        if (strJurusan.isEmpty() || strJurusan.length() == 0 ) {
                            inputJurusanD3.setError("Jurusan tidak boleh kosong");
                            YoYo.with(Techniques.Shake).duration(300).playOn(inputJurusanD3);
                            inputJurusanD3.requestFocus();
                            valid = false;
                        }else {
                            inputJurusanD3.setError(null);
                            if (strAlamatSekolah.isEmpty() || strAlamatSekolah.length() == 0 ) {
                                inputAlamatD3.setError("Alamat kampus tidak boleh kosong");
                                YoYo.with(Techniques.Shake).duration(300).playOn(inputAlamatD3);
                                inputAlamatD3.requestFocus();
                                valid = false;
                            }else {
                                inputAlamatD3.setError(null);
                            }
                        }
                    }
                }
            }
        }
        if (strBiaya.isEmpty() || strBiaya.length() == 0 ) {
            YoYo.with(Techniques.Shake).duration(300).playOn(inputBiaya);
            inputBiaya.requestFocus();
            valid = false;
        }
        if (strKembang.contentEquals("1")){
            if (strKaliKembang.isEmpty() || strKaliKembang.length() == 0 ) {
                inputKaliKembang.setError("Angsuran pengembangan tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputKaliKembang);
                inputKaliKembang.requestFocus();
                valid = false;
            }else
                inputKaliKembang.setError(null);
        }
        if (strDidik.contentEquals("1")){
            if (strKaliDidik.isEmpty() || strKaliDidik.length() == 0 ) {
                inputKaliDidik.setError("Angsuran pendidikan tidak boleh kosong");
                YoYo.with(Techniques.Shake).duration(300).playOn(inputKaliDidik);
                inputKaliDidik.requestFocus();
                valid = false;
            }else
                inputKaliDidik.setError(null);
        }

        return valid;
    }
    public String getJsonString() {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("NAMA_SEKOLAH", strNamaSekolah);
            jsonObj.put("TAHUN_LULUS", strTahunLulus);
            jsonObj.put("NILAI", strNilai);
            jsonObj.put("JURUSAN", strJurusan);
            jsonObj.put("KONSENTRASI", strKonsentrasi);
            jsonObj.put("ALAMAT_SEKOLAH", strAlamatSekolah);
            jsonObj.put("COMPANY", strNamaCompany);
            jsonObj.put("JABATAN", strJabatan);
            jsonObj.put("ALAMAT_COMPANY", strAlamat);
            jsonObj.put("KODE_POS", strKodePos);
            jsonObj.put("PHONE", strPhone);
            jsonObj.put("BIAYA", strBiaya);
            jsonObj.put("PENGEMBANGAN", strKembang);
            jsonObj.put("ANGSUR_KEMBANG", strKaliKembang);
            jsonObj.put("PENDIDIKAN", strDidik);
            jsonObj.put("ANGSUR_DIDIK", strKaliDidik);
            return jsonObj.toString();
        }
        catch(JSONException ex) {
            return "";
        }
    }





    private void onStartAnimation(){
        overridePendingTransition(R.anim.fade_in, R.anim.no_slide);
    }
    public void onEndAnimation(){
        overridePendingTransition(R.anim.no_slide, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onStartAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        onStartAnimation();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onEndAnimation();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onEndAnimation();
    }
    @Override
    public void onStop() {
        super.onStop();
        onEndAnimation();
    }
}
